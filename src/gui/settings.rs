//
// gui::settings
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui) struct Settings {
    pub changed: bool,
    pub rpanel_width: f32,
    pub grid_width: (i32, i32),
    pub snap_to_grid: bool,
    scripts_path: PathBuf,
    path: PathBuf,
}
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub(super) enum SettingsAction {
    PanelWidthChanged,
    GridSpacingChanged(i32, i32),
}
// ----------------------------------------------------------------------------
use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::path::{Path, PathBuf};
// ----------------------------------------------------------------------------
const SETTING_RPANEL_WIDTH: &str = "right panel width";
const SETTING_GRID_WIDTH_X: &str = "plane grid x-width";
const SETTING_GRID_WIDTH_Y: &str = "plane grid y-width";
const SETTING_SNAP_TO_GRID: &str = "snap blocks to grid";
const SETTING_SCRIPTS_FOLDER: &str = "project scripts folder";
// ----------------------------------------------------------------------------
impl Settings {
    // ------------------------------------------------------------------------
    pub fn load(&mut self, path: &Path) -> Result<(), String> {
        self.path = PathBuf::from(path);

        if path.exists() {
            debug!("reading settings from {}...", path.display());

            let file_reader = File::open(path)
                .map(BufReader::new)
                .map_err(|err| format!("settings: error reading settings file: {}", err))?;

            for (i, line) in file_reader.lines().enumerate() {
                let line =
                    line.map_err(|e| format!("settings: failed to read line {}: {}", i, e))?;
                let keyval = line.split('=').collect::<Vec<_>>();
                if keyval.len() != 2 {
                    warn!("settings: could not parse \"{}\". skipping line...", line);
                }

                match keyval[0].trim().to_lowercase().as_str() {
                    SETTING_RPANEL_WIDTH => {
                        let width = keyval[1].trim().parse::<u16>().map_err(|e| {
                            format!(
                                "settings: failed to parse numerical value in line {}: {}",
                                i, e
                            )
                        })?;
                        self.rpanel_width = 320.max(width).min(500) as f32;
                    }
                    SETTING_GRID_WIDTH_X => {
                        let width = keyval[1].trim().parse::<i32>().map_err(|e| {
                            format!(
                                "settings: failed to parse numerical value in line {}: {}",
                                i, e
                            )
                        })?;
                        self.grid_width.0 = 1.max(width).min(150);
                    }
                    SETTING_GRID_WIDTH_Y => {
                        let width = keyval[1].trim().parse::<i32>().map_err(|e| {
                            format!(
                                "settings: failed to parse numerical value in line {}: {}",
                                i, e
                            )
                        })?;
                        self.grid_width.1 = 1.max(width).min(150);
                    }
                    SETTING_SNAP_TO_GRID => {
                        let snap = keyval[1].trim().parse::<bool>().map_err(|e| {
                            format!("settings: failed to parse bool value in line {}: {}", i, e)
                        })?;
                        self.snap_to_grid = snap;
                    }
                    SETTING_SCRIPTS_FOLDER => {
                        self.scripts_path = PathBuf::from(keyval[1].trim());
                    }
                    unknown => warn!(
                        "settings: found unknown setting in line {}: {}. ignoring...",
                        i + 1,
                        unknown
                    ),
                }
            }
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
    pub fn save(&mut self) -> Result<(), String> {
        info!("saving settings to [{}]...", self.path.display());

        let mut file_writer = File::create(&self.path)
            .map(BufWriter::new)
            .map_err(|err| {
                format!(
                    "settings writer: couldn't create {}: {}",
                    self.path.display(),
                    err
                )
            })?;

        let settings = vec![
            format!("{} = {}\n", SETTING_RPANEL_WIDTH, self.rpanel_width as u16),
            format!("{} = {}\n", SETTING_GRID_WIDTH_X, self.grid_width.0),
            format!("{} = {}\n", SETTING_GRID_WIDTH_Y, self.grid_width.1),
            format!("{} = {}\n", SETTING_SNAP_TO_GRID, self.snap_to_grid),
            format!(
                "{} = {}\n",
                SETTING_SCRIPTS_FOLDER,
                self.scripts_path.display()
            ),
        ];

        for setting in settings {
            file_writer
                .write(setting.as_bytes())
                .map_err(|e| format!("settings writer: could not write setting: {}", e))?;
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
    pub fn script_path(&self, project_path: &Path) -> PathBuf {
        let mut path = project_path.to_owned();
        path.push(self.scripts_path.clone());
        path
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for Settings {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        Settings {
            changed: false,
            rpanel_width: 320.0,
            grid_width: (50, 25),
            snap_to_grid: false,
            scripts_path: PathBuf::from("../mod.scripts/local/"),
            path: PathBuf::from("settings.ini"),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
