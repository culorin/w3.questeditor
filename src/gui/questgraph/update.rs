//
// questgraph::actions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn handle_action(
    action: QuestGraphAction,
    state: &mut GraphState,
    def: &mut QuestStructure,
    dirtyflag: &mut bool,
) -> Result<Option<::gui::ActionSequence>, String> {
    use self::QuestGraphAction::*;

    let mut result = None;

    match action {
        Graph(action) => {
            result = handle_graph_interaction(action, state, dirtyflag);
        }
        Menu(action) => {
            result = handle_menu_action(action, def, state, dirtyflag)?;
        }
        OnDeselectBlock => {
            result = Some(ima_seq![
                QuestGraphAction::GuardModifiedBlock,
                QuestGraphAction::DeselectBlock
            ]);
        }
        OnSelectSegment(segmentid) => {
            result = Some(ima_seq![
                QuestGraphAction::GuardModifiedBlock,
                QuestGraphAction::SelectSegment(segmentid)
            ]);
        }
        OnSelectBlock(blockid) => {
            result = Some(ima_seq![
                QuestGraphAction::GuardModifiedBlock,
                QuestGraphAction::SelectBlock(blockid.clone()),
                QuestGraphAction::StartBlockDrag(blockid)
            ]);
        }
        SelectSegment(ref segmentid) => {
            cmds::update_segment_graph(
                &state.selection.segment.1,
                def,
                &state.blocks,
                &state.edges,
                &state.plane,
            )?;

            select_segment(
                segmentid,
                def,
                &mut state.selection,
                &mut state.plane,
                &mut state.blocks,
                &mut state.edges,
                &mut state.lists,
            )?;
            state.property_editor = None;
        }
        SelectBlock(ref blockid) => {
            cmds::select_block(
                blockid,
                &mut state.selection,
                &mut state.plane,
                &mut state.blocks,
                &mut state.edges,
            );
            result = Some(ima_seq![blocks::BlockEditorAction::InitFromBlock(
                blockid.clone()
            )]);
        }
        DeselectBlock => {
            cmds::deselect_block(
                &mut state.selection,
                &mut state.plane,
                &mut state.blocks,
                &mut state.edges,
            );
            state.property_editor = None;
        }
        StartBlockDrag(ref blockid) => {
            graph::action::start_block_drag(&mut state.plane, blockid, &state.blocks);
        }
        OpenBlockContextMenu(ref _blockid) => {
            state.menu.open_block_context_menu = true;
        }
        DeleteBlock(ref blockid) => {
            *dirtyflag = true;
            cmds::delete_block(
                &mut state.selection,
                blockid,
                def,
                &mut state.plane,
                &mut state.blocks,
                &mut state.edges,
            )?;
            state.property_editor = None;
            match blockid {
                BlockId::SegmentIn(_) | BlockId::SegmentOut(_) => {
                    cmds::delete_subsegment_socket(&state.selection.segment.1, def)?
                }
                _ => {}
            }
        }
        BlockAdded(blockid) => match blockid {
            BlockId::SegmentIn(_) | BlockId::SegmentOut(_) => {
                *dirtyflag = true;
                cmds::add_new_subsegment_socket(&state.selection.segment.1, def)?
            }
            _ => {}
        },
        AutoLayout => {
            *dirtyflag = true;
            auto_layout(&mut state.blocks, &mut state.edges);
        }
        UpdateGridSpacing(width_x, width_y) => {
            state.plane.set_grid_width((width_x as f32, width_y as f32));
        }
        QuestBlockEditor(action) => {
            blocks::handle_editor_action(
                action,
                &mut state.property_editor,
                def,
                &state.lists,
                &mut state.selection,
                &mut state.plane,
                &mut state.blocks,
                &mut state.edges,
                dirtyflag,
            )?;
        }
        GuardModifiedBlock => {
            if let Some(ref property_editor) = state.property_editor {
                if property_editor.changed() {
                    result = Some(ima_prio_seq![super::guard_modified_properties()]);
                }
            }
        }
        OnDeleteSegment(segmentid) => {
            result = Some(ima_seq![
                QuestGraphAction::GuardModifiedBlock,
                EditorAction::Confirm(
                    format!(
                        "Are you sure you want to delete segment \"{}\"?\
                         \n\nNote: all connections to and from all subsegment \
                         blocks referencing this segment will be removed!",
                        segmentid
                    ),
                    vec![EditorAction::QuestGraph(QuestGraphAction::DeleteSegment(
                        segmentid
                    )),],
                    vec![],
                )
            ]);
        }
        DeleteSegment(segmentid) => {
            *dirtyflag = true;
            cmds::delete_segment(
                &segmentid,
                def,
                &mut state.selection,
                &mut state.blocks,
                &mut state.edges,
            )?;
            // select root segment if the currently shown segment is deleted
            if state.selection.segment.1 == segmentid {
                // do NOT try to save current segment (hence direct call to
                // select_segment and using SelectSegment action)
                select_segment(
                    &SegmentId::default(),
                    def,
                    &mut state.selection,
                    &mut state.plane,
                    &mut state.blocks,
                    &mut state.edges,
                    &mut state.lists,
                )?;
                state.property_editor = None;
            } else {
                result = Some(ima_seq![QuestGraphAction::SelectSegment(
                    state.selection.segment.1.clone()
                )]);
            }
        }
        OnCreateSegment => {
            result = Some(ima_seq![
                QuestGraphAction::GuardModifiedBlock,
                PopupRequest(Box::new(SegmentNameRequest::default()))
            ]);
        }
        CreateSegment(segmentid) => {
            *dirtyflag = true;
            cmds::create_new_segment(&segmentid, def, &mut state.selection)?;
            result = Some(ima_seq![QuestGraphAction::SelectSegment(segmentid)])
        }
    }
    Ok(result)
}
// ----------------------------------------------------------------------------
use imgui_controls::graph;

use model::questgraph::QuestStructure;

use super::blocks;
use super::cmds;
// state
use super::{DataListProvider, GraphState, PlaneState, SelectionState};

// actions

use super::{Action as QuestGraphAction, EditorAction, GraphAction, MenuAction};
use gui::Action::PopupRequest;
// misc

use super::popup::SegmentNameRequest;
use super::{BlockId, Blocks, Edges, SegmentId};
// action groups
// ----------------------------------------------------------------------------
#[inline]
fn handle_graph_interaction(
    interaction: GraphAction,
    state: &mut super::GraphState,
    dirtyflag: &mut bool,
) -> Option<::gui::ActionSequence> {
    use self::graph::BlockInteraction;
    use self::graph::Interaction as GraphAction;
    // capture all graph actions that might trigger a blockchange and map to guard
    // against unsaved properties + dedicated action
    let (perform_default_action, followup_actions) = match interaction {
        GraphAction::Deselect => {
            // defer graph deselection (may get cancled)
            (false, Some(ima_seq![QuestGraphAction::OnDeselectBlock]))
        }

        GraphAction::DragStop => {
            if let Some(ref blockid) = state.selection.block {
                (
                    true,
                    Some(ima_seq![blocks::BlockEditorAction::SavePosition(
                        blockid.1.clone()
                    )]),
                )
            } else {
                (true, None)
            }
        }

        GraphAction::ContextMenu(pos) => {
            state.interaction_pos = (pos.x, pos.y);
            state.menu.open_plane_context_menu = true;
            (true, None)
        }
        GraphAction::Block(ref interaction) => match interaction {
            BlockInteraction::SelectSocket(blockid, b) => {
                // if blockproperties are dirty action has to be prepended with
                // modified data guard (as aaction *may* select other block)
                if state
                    .property_editor
                    .as_ref()
                    .map(blocks::PropertyEditorState::changed)
                    .unwrap_or(false)
                {
                    (
                        false,
                        Some(ima_seq![
                            QuestGraphAction::OnSelectBlock(blockid.clone()),
                            QuestGraphAction::Graph(GraphAction::Block(
                                BlockInteraction::SelectSocket(blockid.clone(), b.clone())
                            ))
                        ]),
                    )
                } else {
                    // no changed properties so an implicit selection of another
                    // block doesn't discard changes
                    (true, None)
                }
            }
            BlockInteraction::Select(ref id) => (
                false,
                Some(ima_seq![QuestGraphAction::OnSelectBlock(id.clone())]),
            ),
            BlockInteraction::ContextMenu(ref id) => (
                false,
                Some(ima_seq![
                    QuestGraphAction::OnSelectBlock(id.clone()),
                    QuestGraphAction::OpenBlockContextMenu(id.clone())
                ]),
            ),
            BlockInteraction::EdgeDisconnect if state.plane.is_edge_drag_active() => {
                *dirtyflag = true;
                (true, None)
            }
            BlockInteraction::EdgeConnectTo(_, _) => {
                *dirtyflag = true;
                (true, None)
            }
            _ => (true, None),
        },
        _ => (true, None),
    };

    if perform_default_action {
        // some default actions may implicitely select a block (e.g. edge dragging)
        if let Some(blockid) = graph::perform_default_action(
            interaction,
            &mut state.plane,
            &mut state.blocks,
            &mut state.edges,
        ) {
            // update selection state and properties in property editor
            return Some(ima_seq![QuestGraphAction::SelectBlock(blockid)]);
        }
    }

    followup_actions
}
// ----------------------------------------------------------------------------
#[inline]
fn handle_menu_action(
    action: MenuAction,
    def: &mut QuestStructure,
    state: &mut super::GraphState,
    dirtyflag: &mut bool,
) -> Result<Option<::gui::ActionSequence>, String> {
    match action {
        MenuAction::LayoutBlocks => Ok(Some(ima_seq![QuestGraphAction::AutoLayout])),
        MenuAction::SnapBlocksToGrid(do_snap) => {
            if do_snap {
                state.plane.set_grid_width(state.config.grid_spacing);
            } else {
                // reset snapping via default spacing
                state.plane.set_grid_width((1.0, 1.0));
            }
            Ok(None)
        }
        MenuAction::BlockCreation(blocktype) => {
            *dirtyflag = true;
            let new_id = blocks::cmds::add_new_block_from_template(
                blocktype,
                state.interaction_pos,
                &state.selection.segment.1,
                &mut state.blocks,
                def,
                &state.config.scripttemplates,
            )?;
            // update block list
            state.selection.sync_blocks(&state.blocks);

            Ok(Some(ima_seq![QuestGraphAction::BlockAdded(new_id)]))
        }
        MenuAction::BlockDeletion(blockid) => {
            Ok(Some(ima_seq![QuestGraphAction::DeleteBlock(blockid)]))
        }
        MenuAction::BlockSpecific(action) => blocks::handle_block_action(
            action,
            def,
            &mut state.selection,
            &mut state.plane,
            &mut state.blocks,
            &mut state.edges,
            dirtyflag,
        ),
        MenuAction::CreateSegment => Ok(Some(ima_seq![QuestGraphAction::OnCreateSegment])),
        MenuAction::DeleteSegment(segmentid) => {
            Ok(Some(ima_seq![QuestGraphAction::OnDeleteSegment(segmentid)]))
        }
    }
}
// ----------------------------------------------------------------------------
// interactions
// ----------------------------------------------------------------------------
fn select_segment(
    segmentid: &SegmentId,
    def: &QuestStructure,
    selection: &mut SelectionState,
    plane: &mut PlaneState,
    blocks: &mut Blocks,
    edges: &mut Edges,
    lists: &mut DataListProvider,
) -> Result<(), String> {
    let (graph_size, new_blocks, new_edges, editordata) =
        cmds::extract_segment_graph(segmentid, def)?;

    selection.select_segment(segmentid);
    selection.deselect_block();

    // update block list for new segment
    selection.sync_blocks(&new_blocks);

    *blocks = new_blocks;
    *edges = new_edges;

    plane
        .set_offset(editordata.pos)
        .set_zoom(editordata.zoom)
        .set_dim(graph_size.extend(super::GRAPH_EXTENSION));

    lists.refresh_segments(def, segmentid);

    Ok(())
}
// ----------------------------------------------------------------------------
fn auto_layout(blocks: &mut Blocks, edges: &mut Edges) {
    graph::auto_layout_blocks(blocks, edges);
}
// ----------------------------------------------------------------------------
