//
// blocks::scripttemplates
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(super) fn generate_list(
    questid: &str,
    project_script_folder: Option<PathBuf>,
) -> ScriptTemplates {
    let mut scripttemplates = IndexMap::new();

    scripttemplates.insert(
        ImString::new(""),
        vec![ScriptTemplate::new_with_caption(
            "logMsg",
            "logging",
            "radLog",
            vec![
                ScriptParameter::new_cname_with("logChannel", questid.to_uppercase()),
                ScriptParameter::new_string("msg"),
            ],
        )],
    );

    if let Some(script_folder) = project_script_folder {
        match scan_quest_functions(&script_folder) {
            Ok(ref quest_functions) => {
                add_custom_templates(&mut scripttemplates, questid, quest_functions)
            }
            Err(msg) => error!("extracting quest functions failed: {}", msg),
        }
    }

    add_facts(&mut scripttemplates);
    add_noticeboards(&mut scripttemplates);
    add_interaction(&mut scripttemplates);
    add_gameplay(&mut scripttemplates);
    add_environment(&mut scripttemplates);
    add_entities(&mut scripttemplates);
    add_actors(&mut scripttemplates);
    add_items(&mut scripttemplates);
    add_camera(&mut scripttemplates);
    add_hud(&mut scripttemplates);
    add_glossary(&mut scripttemplates);

    scripttemplates
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::collections::BTreeMap;
use std::path::{Path, PathBuf};

use imgui::ImString;
use indexmap::IndexMap;

use model::shared::ScriptParameter;

use super::{ScriptTemplate, ScriptTemplates};
// ----------------------------------------------------------------------------
fn add_custom_templates(
    templates: &mut ScriptTemplates,
    questid: &str,
    custom: &BTreeMap<String, Vec<ScriptParameter>>,
) {
    if !custom.is_empty() {
        let mut custom_templates = Vec::new();

        for (function, params) in custom.iter() {
            custom_templates.push(ScriptTemplate::new(function, function, params.to_vec()));
        }

        templates.insert(ImString::new(questid), custom_templates);
    }
}
// ----------------------------------------------------------------------------
fn add_interaction(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("interaction..."),
        vec![
            ScriptTemplate::new(
                "enable container",
                "EnableOrDisableContainers",
                vec![
                    ScriptParameter::new_cname("containersTag"),
                    ScriptParameter::new_bool_with("containerEnabled", true),
                ],
            ),
            ScriptTemplate::new(
                "disable container",
                "EnableOrDisableContainers",
                vec![
                    ScriptParameter::new_cname("containersTag"),
                    ScriptParameter::new_bool_with("containerEnabled", false),
                ],
            ),
            ScriptTemplate::new(
                "enable teleport",
                "ManageTeleport",
                vec![
                    ScriptParameter::new_cname("teleportTag"),
                    ScriptParameter::new_bool_with("enabling_activating", true),
                    ScriptParameter::new_bool_with("value", true),
                    ScriptParameter::new_bool_with("keepBlackscreen", true),
                    ScriptParameter::new_f32_with("activationTime", 1.0),
                ],
            ),
            ScriptTemplate::new(
                "disable teleport",
                "ManageTeleport",
                vec![
                    ScriptParameter::new_cname("teleportTag"),
                    ScriptParameter::new_bool("enabling_activating"),
                    ScriptParameter::new_bool_with("value", false),
                    ScriptParameter::new_bool_with("keepBlackscreen", true),
                    ScriptParameter::new_f32_with("activationTime", 1.0),
                ],
            ),
            ScriptTemplate::new(
                "open gate",
                "ManageGate",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_bool_with("open", true),
                    ScriptParameter::new_f32_with("speedModifier", 1.0),
                ],
            ),
            ScriptTemplate::new(
                "close gate",
                "ManageGate",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_bool_with("open", false),
                    ScriptParameter::new_f32_with("speedModifier", 1.0),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_entities(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("entities..."),
        vec![
            ScriptTemplate::new_with_caption(
                "add entitytag",
                "add tag",
                "AddTagToEntitiesQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("newTag"),
                    ScriptParameter::new_bool_with("remove", false),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "remove entitytag",
                "remove tag",
                "AddTagToEntitiesQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("newTag"),
                    ScriptParameter::new_bool_with("remove", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "destroy entity",
                "destroy",
                "DestroyEntity",
                vec![ScriptParameter::new_cname("entityTag")],
            ),
            ScriptTemplate::new_with_caption(
                "play entity effect",
                "play effect",
                "PlayEffectQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("effectName"),
                    ScriptParameter::new_bool_with("activate", true),
                    ScriptParameter::new_bool("persistentEffect"),
                    ScriptParameter::new_bool("deactivateAll"),
                    ScriptParameter::new_bool_with("preventEffectStacking", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "stop entity effect",
                "stop effect",
                "PlayEffectQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("effectName"),
                    ScriptParameter::new_bool_with("activate", false),
                    ScriptParameter::new_bool("persistentEffect"),
                    ScriptParameter::new_bool("deactivateAll"),
                    ScriptParameter::new_bool("preventEffectStacking"),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "stop all entity effects",
                "stop all effects",
                "PlayEffectQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("effectName"),
                    ScriptParameter::new_bool_with("activate", false),
                    ScriptParameter::new_bool("persistentEffect"),
                    ScriptParameter::new_bool_with("deactivateAll", true),
                    ScriptParameter::new_bool("preventEffectStacking"),
                ],
            ),
            ScriptTemplate::new(
                "enable component",
                "EntityComponentQuest",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_cname("componentName"),
                    ScriptParameter::new_bool_with("bEnable", true),
                ],
            ),
            ScriptTemplate::new(
                "disable component",
                "EntityComponentQuest",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_cname("componentName"),
                    ScriptParameter::new_bool_with("bEnable", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_actors(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("actors..."),
        vec![
            ScriptTemplate::new(
                "equip item",
                "EquipItemQuest",
                vec![
                    ScriptParameter::new_cname("targetTag"),
                    ScriptParameter::new_cname("itemName"),
                    ScriptParameter::new_cname("itemCategory"),
                    ScriptParameter::new_cname("itemTag"),
                    ScriptParameter::new_bool_with("unequip", true),
                    ScriptParameter::new_bool_with("toHand", true),
                ],
            ),
            ScriptTemplate::new(
                "change appearance",
                "AppearanceChange",
                vec![
                    ScriptParameter::new_cname("npcsTag"),
                    ScriptParameter::new_cname("appearanceName"),
                ],
            ),
            ScriptTemplate::new(
                "assign group attitude",
                "AssignNPCGroupAttitudeQuest",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_cname("attGroup"),
                ],
            ),
            ScriptTemplate::new(
                "despawn npc",
                "DespawnNPCsWithTag",
                vec![ScriptParameter::new_cname("tag")],
            ),
            ScriptTemplate::new(
                "set behavior variable",
                "SetBehaviorVariableQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("variableName"),
                    ScriptParameter::new_f32("variableValue"),
                ],
            ),
            ScriptTemplate::new(
                "broadcast danger",
                "BroadcastDanger",
                vec![
                    ScriptParameter::new_f32_with("lifetime", 10.0),
                    ScriptParameter::new_f32_with("distance", 10.0),
                    ScriptParameter::new_f32_with("interval", 10.0),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "block actor ability",
                "block ability",
                "BlockActorAbility",
                vec![
                    ScriptParameter::new_cname("actorTag"),
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("unBlock", false),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "unblock actor ability",
                "unblock ability",
                "BlockActorAbility",
                vec![
                    ScriptParameter::new_cname("actorTag"),
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("unBlock", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "add npc ability",
                "add ability",
                "ModifyNPCAbilityQuest",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("remove", false),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "remove npc ability",
                "remove ability",
                "ModifyNPCAbilityQuest",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("remove", true),
                ],
            ),
            ScriptTemplate::new(
                "play voiceset",
                "PlayVoicesetQuest",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_string("voiceSet"),
                    ScriptParameter::new_bool_with("oneRandomActor", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "enable npc interactivness",
                "enable interactivness",
                "DisableNPCInteractivness",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_bool_with("disableTalking", false),
                    ScriptParameter::new_bool_with("disableOnliners", false),
                    ScriptParameter::new_bool_with("disableLookats", false),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "disable npc interactivness",
                "disable interactivness",
                "DisableNPCInteractivness",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_bool_with("disableTalking", true),
                    ScriptParameter::new_bool_with("disableOnliners", true),
                    ScriptParameter::new_bool_with("disableLookats", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "enable npc attackable",
                "enable attackable by player",
                "SetNPCIsAttackableByPlayer",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_bool("persistent"),
                    ScriptParameter::new_bool_with("attackable", true),
                    ScriptParameter::new_f32_with("timeout", 10000.0),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "disable npc attackable",
                "disable attackable by player",
                "SetNPCIsAttackableByPlayer",
                vec![
                    ScriptParameter::new_cname("npcTag"),
                    ScriptParameter::new_bool("persistent"),
                    ScriptParameter::new_bool_with("attackable", false),
                    ScriptParameter::new_f32_with("timeout", 10000.0),
                ],
            ),
            ScriptTemplate::new(
                "enable shopkeeper",
                "EnableShopkeeper",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_bool_with("enable", true),
                ],
            ),
            ScriptTemplate::new(
                "disable shopkeeper",
                "EnableShopkeeper",
                vec![
                    ScriptParameter::new_cname("tag"),
                    ScriptParameter::new_bool_with("enable", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_environment(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("environment..."),
        vec![
            ScriptTemplate::new(
                "change weather",
                "ChangeWeatherQuest",
                vec![
                    ScriptParameter::new_cname("weatherName"),
                    ScriptParameter::new_f32_with("blendTime", 30.0),
                    ScriptParameter::new_bool("randomGen"),
                    ScriptParameter::new_bool("questPause"),
                ],
            ),
            // ScriptTemplate::new(
            //     "deactivate environment",
            //     "DectivateEnvironmentQuest",
            //     vec![ScriptParameter::new_f32_with("blendTime", 20.0)],
            // ),
            ScriptTemplate::new(
                "set timescale",
                "SetTimeScaleQuest",
                vec![ScriptParameter::new_f32_with("timeScale", 1.0)],
            ),
            ScriptTemplate::new(
                "enable illusion",
                "EnableIllusionQuest",
                vec![
                    ScriptParameter::new_cname("illusionTag"),
                    ScriptParameter::new_bool_with("enabled", true),
                ],
            ),
            ScriptTemplate::new(
                "disable illusion",
                "EnableIllusionQuest",
                vec![
                    ScriptParameter::new_cname("illusionTag"),
                    ScriptParameter::new_bool_with("enabled", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_camera(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("camera..."),
        vec![
            ScriptTemplate::new(
                "fade in",
                "FadeInQuest",
                vec![ScriptParameter::new_f32_with("fadeTime", 3.0)],
            ),
            ScriptTemplate::new_with_caption(
                "start cam shake",
                "start shake",
                "CameraShake",
                vec![ScriptParameter::new_f32_with("strength", 1.0)],
            ),
            ScriptTemplate::new_with_caption(
                "stop cam shake",
                "stop shake",
                "StopCameraShake",
                vec![ScriptParameter::new_cname("animName")],
            ),
            ScriptTemplate::new_with_caption(
                "play cam effect",
                "play effect",
                "EffectOnCamera",
                vec![
                    ScriptParameter::new_cname("effectName"),
                    ScriptParameter::new_bool_with("play", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "stop cam effect",
                "stop effect",
                "EffectOnCamera",
                vec![
                    ScriptParameter::new_cname("effectName"),
                    ScriptParameter::new_bool_with("play", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_facts(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("facts..."),
        vec![
            ScriptTemplate::new(
                "remove fact",
                "RemoveFactQuest",
                vec![ScriptParameter::new_cname("factId")],
            ),
            ScriptTemplate::new(
                "reset fact",
                "ResetFactQuest",
                vec![ScriptParameter::new_cname("factID")],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_hud(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("hud..."),
        vec![
            ScriptTemplate::new(
                "show timelapse",
                "ShowTimeLapse",
                vec![
                    ScriptParameter::new_f32_with("showTime", 5.0),
                    ScriptParameter::new_string("timeLapseMessageKey"),
                    ScriptParameter::new_string("timeLapseAdditionalMessageKey"),
                ],
            ),
            ScriptTemplate::new(
                "show bossfight indicator",
                "ShowBossFightIndicator",
                vec![
                    ScriptParameter::new_bool_with("enable", true),
                    ScriptParameter::new_cname("bossTag"),
                ],
            ),
            ScriptTemplate::new(
                "hide bossfight indicator",
                "ShowBossFightIndicator",
                vec![
                    ScriptParameter::new_bool_with("enable", false),
                    ScriptParameter::new_cname("bossTag"),
                ],
            ),
            // ScriptTemplate::new(
            //     "ShowCompanionIndicator",
            //     "ShowCompanionIndicator",
            //     vec![
            //         enable:bool,
            //         npcTag : name,
            //         optional iconPath : string,
            //         optional npcTag2 : name,
            //         optional iconPath2 : string
            //     ]),
            ScriptTemplate::new(
                "show update info",
                "ForceShowUpdateInfo",
                vec![
                    ScriptParameter::new_string("locKeyText"),
                    ScriptParameter::new_string("locKeyTitle"),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_glossary(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("glossary..."),
        vec![
            ScriptTemplate::new(
                "activate image overlay",
                "EnableGlossaryImageOverrideQuest",
                vec![
                    ScriptParameter::new_cname("uniqueEntryTag"),
                    ScriptParameter::new_string("imageFileName"),
                    ScriptParameter::new_bool_with("enable", true),
                ],
            ),
            ScriptTemplate::new(
                "deactivate image overlay",
                "EnableGlossaryImageOverrideQuest",
                vec![
                    ScriptParameter::new_cname("uniqueEntryTag"),
                    ScriptParameter::new_string("imageFileName"),
                    ScriptParameter::new_bool_with("enable", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_gameplay(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("gameplay..."),
        vec![
            ScriptTemplate::new(
                "enable fasttravel",
                "EnableFastTravelling",
                vec![ScriptParameter::new_bool_with("enable", true)],
            ),
            ScriptTemplate::new(
                "disable fasttravel",
                "EnableFastTravelling",
                vec![ScriptParameter::new_bool_with("enable", false)],
            ),
            ScriptTemplate::new(
                "force dismount",
                "ForceDismount",
                vec![ScriptParameter::new_cname("horseTag")],
            ),
            ScriptTemplate::new(
                "add player ability",
                "ModifyPlayerAbilityQuest",
                vec![
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("remove", false),
                ],
            ),
            ScriptTemplate::new(
                "remove player ability",
                "ModifyPlayerAbilityQuest",
                vec![
                    ScriptParameter::new_cname("abilityName"),
                    ScriptParameter::new_bool_with("remove", true),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_items(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("items..."),
        vec![
            ScriptTemplate::new(
                "add alchemy recipe",
                "AddAlchemyRecipeQ",
                vec![ScriptParameter::new_cname("recipeName")],
            ),
            ScriptTemplate::new(
                "add schematics",
                "AddCraftingSchematicsQ",
                vec![ScriptParameter::new_cname("schematicsName")],
            ),
            ScriptTemplate::new_with_caption(
                "disable item",
                "disable",
                "QuestItemDisable",
                vec![
                    ScriptParameter::new_cname("itemName"),
                    ScriptParameter::new_bool_with("addQuestTag", false),
                    ScriptParameter::new_cname("containerTag"),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "remove item",
                "remove",
                "RemoveItemQuest",
                vec![
                    ScriptParameter::new_cname("entityTag"),
                    ScriptParameter::new_cname("item_name"),
                    ScriptParameter::new_cname("item_category"),
                    ScriptParameter::new_cname("item_tag"),
                    ScriptParameter::new_i32_with("quantity", 1),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "transfer item",
                "transfer",
                "TransferItemQuest",
                vec![
                    ScriptParameter::new_cname("sourceTag"),
                    ScriptParameter::new_cname("targetTag"),
                    ScriptParameter::new_cname("itemName"),
                    ScriptParameter::new_cname("itemCategory"),
                    ScriptParameter::new_cname("itemTag"),
                    ScriptParameter::new_i32_with("quantity", 1),
                ],
            ),
            ScriptTemplate::new(
                "read book",
                "ReadBookByName",
                vec![
                    ScriptParameter::new_cname("bookName"),
                    ScriptParameter::new_bool_with("unread", false),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
fn add_noticeboards(templates: &mut ScriptTemplates) {
    templates.insert(
        ImString::new("noticeboards..."),
        vec![
            ScriptTemplate::new_with_caption(
                "enable noticeboard",
                "enable",
                "DisableNoticeboardQuest",
                vec![
                    ScriptParameter::new_cname("boardTag"),
                    ScriptParameter::new_bool_with("disabled", false),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "disable noticeboard",
                "disable",
                "DisableNoticeboardQuest",
                vec![
                    ScriptParameter::new_cname("boardTag"),
                    ScriptParameter::new_bool_with("disabled", true),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "open noticeboard",
                "open",
                "ForceOpenNoticeboardQuest",
                vec![
                    ScriptParameter::new_cname("boardTag"),
                    ScriptParameter::new_bool("dontConsiderAsVisiting"),
                    ScriptParameter::new_bool("dontAddDiscoveryFact"),
                    ScriptParameter::new_bool("dontSetPOIEntitiesKnown"),
                ],
            ),
            ScriptTemplate::new_with_caption(
                "remove noticeboard errands",
                "remove errands",
                "RemoveErrandsFromNoticeboard",
                vec![
                    ScriptParameter::new_cname("boardTag"),
                    ScriptParameter::new_string("errandName"),
                ],
            ),
        ],
    );
}
// ----------------------------------------------------------------------------
// internal helper trait
// ----------------------------------------------------------------------------
trait ParamGenerator {
    fn new_cname(name: &str) -> ScriptParameter;
    fn new_cname_with(name: &str, value: String) -> ScriptParameter;
    fn new_string(name: &str) -> ScriptParameter;
    fn new_i32(name: &str) -> ScriptParameter;
    fn new_i32_with(name: &str, value: i32) -> ScriptParameter;
    fn new_f32(name: &str) -> ScriptParameter;
    fn new_f32_with(name: &str, value: f32) -> ScriptParameter;
    fn new_bool(name: &str) -> ScriptParameter;
    fn new_bool_with(name: &str, value: bool) -> ScriptParameter;
}
// ----------------------------------------------------------------------------
impl ParamGenerator for ScriptParameter {
    // ------------------------------------------------------------------------
    fn new_cname(name: &str) -> ScriptParameter {
        ScriptParameter::CName(name.into(), "".into())
    }
    // ------------------------------------------------------------------------
    fn new_cname_with(name: &str, value: String) -> ScriptParameter {
        ScriptParameter::CName(name.into(), value)
    }
    // ------------------------------------------------------------------------
    fn new_string(name: &str) -> ScriptParameter {
        ScriptParameter::String(name.into(), "".into())
    }
    // ------------------------------------------------------------------------
    fn new_i32(name: &str) -> ScriptParameter {
        ScriptParameter::Int32(name.into(), -1)
    }
    // ------------------------------------------------------------------------
    fn new_i32_with(name: &str, value: i32) -> ScriptParameter {
        ScriptParameter::Int32(name.into(), value)
    }
    // ------------------------------------------------------------------------
    fn new_f32(name: &str) -> ScriptParameter {
        ScriptParameter::Float(name.into(), -1.0)
    }
    // ------------------------------------------------------------------------
    fn new_f32_with(name: &str, value: f32) -> ScriptParameter {
        ScriptParameter::Float(name.into(), value)
    }
    // ------------------------------------------------------------------------
    fn new_bool(name: &str) -> ScriptParameter {
        ScriptParameter::Bool(name.into(), false)
    }
    // ------------------------------------------------------------------------
    fn new_bool_with(name: &str, value: bool) -> ScriptParameter {
        ScriptParameter::Bool(name.into(), value)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// current project scanning and adding functions
// ----------------------------------------------------------------------------
/// regexp for extracing quest function signature
const REGEXP_QUEST_FUNCTION_SIG: &str =
    r"quest\s+function\s+([^\s\(]+)\s*\(\s*([^\)]*)\)\s*:?\s*([^{\s]*)\s*\{";
// ----------------------------------------------------------------------------
fn scan_quest_functions(path: &Path) -> Result<BTreeMap<String, Vec<ScriptParameter>>, String> {
    use walkdir::{DirEntry, WalkDir};

    let mut result = BTreeMap::new();

    info!("> scanning for quest functions in {}", path.display());

    let path = path.canonicalize().map_err(|e| e.to_string())?;

    let is_script_file = |entry: &DirEntry| -> bool {
        entry
            .file_name()
            .to_str()
            .map(|s| s.ends_with(".ws"))
            .unwrap_or(false)
    };

    // find all script files
    let def_filelist = WalkDir::new(path)
        .contents_first(true)
        .into_iter()
        .filter_entry(|e| is_script_file(e))
        .collect::<Result<Vec<_>, _>>()
        .map_err(|e| format!("read error: {}", e))?;

    for file in def_filelist {
        let filename = file.file_name().to_str().ok_or_else(|| {
            format!(
                "failed to convert filename to string: {}",
                file.path().display()
            )
        })?;

        info!("> found scriptfile: {}", filename);

        let content = std::fs::read(file.path())
            .map_err(|e| format!("error reading file {}: {}", filename, e))?;

        let content = String::from_utf8(content)
            .map_err(|e| format!("error reading scriptfile {}: {}", filename, e))?;

        let mut functions = parse_scriptfile(&content);
        info!(">> adding #{} quest function templates", functions.len());

        result.append(&mut functions);
    }

    Ok(result)
}
// ----------------------------------------------------------------------------
fn parse_scriptfile(content: &str) -> BTreeMap<String, Vec<ScriptParameter>> {
    use regex::Regex;

    let mut result = BTreeMap::new();

    let re = Regex::new(REGEXP_QUEST_FUNCTION_SIG).unwrap();

    for matches in re.captures_iter(content) {
        let function_name = matches[1].trim();
        let param_matches = matches[2]
            .trim()
            .split(',')
            .map(|p| p.trim())
            .filter(|p| !p.is_empty())
            .collect::<Vec<&str>>();
        let has_return_value = !matches[3].trim().is_empty();

        debug!(
            "found quest function {} with #{} params (has result: {})",
            function_name,
            param_matches.len(),
            has_return_value
        );

        let mut success = true;
        let mut params = Vec::new();

        let parse_param = |string: &str| -> Result<ScriptParameter, String> {
            let param = string
                .splitn(2, ':')
                .map(|p| p.trim())
                .collect::<Vec<&str>>();

            if param.len() == 2 {
                let param_name = param[0].replace("optional", "");
                let param_name = param_name.trim();

                match param[1].to_lowercase().as_str() {
                    "bool" => Ok(ScriptParameter::new_bool(param_name)),
                    "cname" | "name" => Ok(ScriptParameter::new_cname(param_name)),
                    "float" => Ok(ScriptParameter::new_f32(param_name)),
                    "int" => Ok(ScriptParameter::new_i32(param_name)),
                    "string" => Ok(ScriptParameter::new_string(param_name)),
                    _ => Err(format!("unsupported parameter type: {}", param[1])),
                }
            } else {
                Err(format!(
                    "expected <param_name>:<param_type> found: {}",
                    string
                ))
            }
        };

        if param_matches.len() <= 11 {
            for param_str in param_matches {
                match parse_param(param_str) {
                    Ok(param) => {
                        params.push(param);
                    }
                    Err(msg) => {
                        error!("failed to parse quest function param. {}", msg);
                        success = false;
                        break;
                    }
                }
            }
            if success {
                result.insert(function_name.to_string(), params);
            }
        } else {
            error!(
                "max number of supported quest functions parameters is 10. found: {} for {}",
                param_matches.len(),
                function_name
            );
        }
    }

    result
}
// ----------------------------------------------------------------------------
