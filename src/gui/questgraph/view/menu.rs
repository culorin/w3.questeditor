//
// questgraph::view::context menus
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui) fn show_main_menus<'ui>(
    ui: &Ui<'ui>,
    state: &GraphState,
    settings: &mut Settings,
    structure: &QuestStructure,
) -> Option<MenuAction> {
    let mut result = None;
    ui.menu(im_str!("|")).enabled(false).build(|| {});

    ui.menu(im_str!("Graph")).build(|| {
        if ui.menu_item(im_str!("Auto layout blocks")).build() {
            result = Some(MenuAction::LayoutBlocks);
        }
        ui.separator();
        if ui
            .menu_item(im_str!("Snap blocks to grid"))
            .selected(&mut settings.snap_to_grid)
            .build()
        {
            result = Some(MenuAction::SnapBlocksToGrid(settings.snap_to_grid));
        }
        ui.separator();

        if ui.menu_item(im_str!("Create new segment")).build() {
            result = Some(MenuAction::CreateSegment);
        }

        let segments = state.lists.segments();
        ui.menu(im_str!("Delete segment..."))
            .enabled(!segments.is_empty())
            .build(|| {
                for segment in segments {
                    if ui.menu_item(segment.caption()).build() {
                        result = Some(MenuAction::DeleteSegment(segment.id().clone()));
                    }
                }
            })
    });

    ui.menu(im_str!("Block")).build(|| {
        ui.menu(im_str!("Create new...")).build(|| {
            render_block_creation_items(
                ui,
                &state.selection,
                &state.config,
                structure,
                &mut result,
            );
        });

        ui.separator();
        if let Some(blockid) = state.selection.block.as_ref().map(|(_, id)| id) {

            if let Some(action) = render_block_context_menu_entries(
                ui,
                blockid,
                &state.selection.segment.1,
                &state.blocks,
                structure,
            ) {
                result = Some(action);
            }
        } else {
            // TODO as submenu with all blocks?
            // make menu point visible even if it's deactivated
            ui.menu_item(im_str!("Delete selected"))
                .enabled(false)
                .build();
        }
    });
    result
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn show_context_menus<'ui>(
    ui: &Ui<'ui>,
    menu: &MenuState,
    selection: &SelectionState,
    blocks: &Blocks,
    quest_structure: &QuestStructure,
    config: &Config,
) -> Option<MenuAction> {
    let mut result = None;

    // -- popup activation (one-time trigger for imgui)
    if menu.open_plane_context_menu {
        ui.open_popup(im_str!("plane##context"));
    }
    if menu.open_block_context_menu {
        ui.open_popup(im_str!("block##context"));
    }
    // -- render popups (every frame if popup was opened, see above)
    ui.popup(im_str!("plane##context"), || {
        let io_block_count = quest_structure
            .segment(&selection.segment.1)
            .map(segments::QuestSegment::in_out_block_count)
            .unwrap_or((0, 0));

        ui.text(im_str!("Create new block..."));
        ui.separator();

        result = render_plane_context_menu(
            ui,
            selection.segment.0 == 0,
            &config.scripttemplates,
            io_block_count,
        )
        .map(MenuAction::BlockCreation);
    });

    if let Some((_, id)) = selection.block.as_ref() {
        if let BlockId::QuestStart(_) = id {
            // no options for quest-start block (it's always required and has
            // no options)
        } else {
            ui.popup(im_str!("block##context"), || {
                result = render_block_context_menu(
                    ui,
                    id,
                    blocks,
                    &selection.segment.1,
                    quest_structure,
                );
            });
        }
    }

    result
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::Ui;

use gui::lists::AsCaption;

use super::blocks;
use super::blocks::BlockTemplate;
use super::cmds;
use super::MenuAction;
use super::{GraphState, MenuState, ScriptTemplates, SelectionState, Settings};

use super::MAX_IN_OUT_BLOCKS;
use super::{BlockId, Blocks, Config, LogicOperation, QuestStructure, SegmentId};

use model::questgraph::segments;
// ----------------------------------------------------------------------------
fn render_plane_context_menu<'ui>(
    ui: &Ui<'ui>,
    is_root_segment: bool,
    scripttemplates: &ScriptTemplates,
    io_block_count: (usize, usize),
) -> Option<BlockTemplate> {
    use self::BlockTemplate::*;

    let mut result = None;

    if !is_root_segment {
        ui.menu(im_str!("segment in/out...")).build(|| {
            if ui
                .menu_item(im_str!("in"))
                .enabled(io_block_count.0 < MAX_IN_OUT_BLOCKS)
                .build()
            {
                result = Some(SegmentIn);
            }
            if ui
                .menu_item(im_str!("out"))
                .enabled(io_block_count.1 < MAX_IN_OUT_BLOCKS)
                .build()
            {
                result = Some(SegmentOut);
            }
        });
    }

    ui.menu(im_str!("wait until...")).build(|| {
        use self::blocks::AreaConditionType::*;
        use self::blocks::ConditionType::*;
        use self::blocks::InteractionConditionType::*;
        use self::blocks::LoadScreenConditionType::*;
        use self::blocks::TimeConditionType::*;
        use self::LogicOperation::*;

        if ui.menu_item(im_str!("fact")).build() {
            result = Some(WaitUntil(Fact));
        }
        if ui
            .menu_item(im_str!("facts (multiple out-sockets)"))
            .build()
        {
            result = Some(WaitUntil(MultiOutFact));
        }
        ui.separator();
        if ui.menu_item(im_str!("entered area")).build() {
            result = Some(WaitUntil(Area(Entered)));
        }
        if ui.menu_item(im_str!("left area")).build() {
            result = Some(WaitUntil(Area(Left)));
        }
        if ui.menu_item(im_str!("inside area")).build() {
            result = Some(WaitUntil(Area(Inside)));
        }
        if ui.menu_item(im_str!("outside area")).build() {
            result = Some(WaitUntil(Area(Outside)));
        }
        ui.separator();
        if ui.menu_item(im_str!("time range")).build() {
            result = Some(WaitUntil(Time(Range)));
        }
        if ui.menu_item(im_str!("before some time")).build() {
            result = Some(WaitUntil(Time(Before)));
        }
        if ui.menu_item(im_str!("after some time")).build() {
            result = Some(WaitUntil(Time(After)));
        }
        if ui.menu_item(im_str!("some (real)time elapsed")).build() {
            result = Some(WaitUntil(Time(Elapsed)));
        }
        ui.separator();
        if ui.menu_item(im_str!("examined")).build() {
            result = Some(WaitUntil(Interaction(Examined)));
        }
        if ui.menu_item(im_str!("talked")).build() {
            result = Some(WaitUntil(Interaction(Talked)));
        }
        if ui.menu_item(im_str!("used")).build() {
            result = Some(WaitUntil(Interaction(Used)));
        }
        if ui.menu_item(im_str!("looted")).build() {
            result = Some(WaitUntil(Interaction(Looted)));
        }
        if ui.menu_item(im_str!("custom interaction")).build() {
            result = Some(WaitUntil(Interaction(Custom)));
        }
        ui.separator();
        if ui.menu_item(im_str!("loadscreen shown")).build() {
            result = Some(WaitUntil(LoadScreen(Shown)));
        }
        if ui.menu_item(im_str!("loadscreen hidden")).build() {
            result = Some(WaitUntil(LoadScreen(Hidden)));
        }
        ui.separator();
        if ui.menu_item(im_str!("all conditions")).build() {
            result = Some(WaitUntil(Logic(AND)));
        }
        if ui.menu_item(im_str!("any condition")).build() {
            result = Some(WaitUntil(Logic(OR)));
        }
        // --------------------------------------------------------------------
        // the following logic condition combinators show unexpected behavior
        // (at least in combination with fact checking). to prevent hard to
        // debug problems the creation in editor is being deactivated
        // (although the encoder still DOES support them and the editor will
        // correctly show them in the graph!)
        // --------------------------------------------------------------------
        // if ui.menu_item(im_str!("exactly one condition")).build() {
        //     result = Some(WaitUntil(Logic(XOR)));
        // }
        // if ui.menu_item(im_str!("not all conditions")).build() {
        //     result = Some(WaitUntil(Logic(NAND)));
        // }
        // if ui.menu_item(im_str!("none of the conditions")).build() {
        //     result = Some(WaitUntil(Logic(NOR)));
        // }
        // if ui.menu_item(im_str!("nxor conditions")).build() {
        //     result = Some(WaitUntil(Logic(NXOR)));
        // }
    });

    ui.menu(im_str!("layers...")).build(|| {
        if ui.menu_item(im_str!("show")).build() {
            result = Some(ShowLayers);
        }
        if ui.menu_item(im_str!("hide")).build() {
            result = Some(HideLayers);
        }
        if ui.menu_item(im_str!("show and hide")).build() {
            result = Some(ShowAndHideLayers);
        }
    });

    ui.menu(im_str!("journal...")).build(|| {
        if ui.menu_item(im_str!("entry")).build() {
            result = Some(JournalEntry);
        }
        if ui.menu_item(im_str!("quest phase")).build() {
            result = Some(JournalPhaseObjectives);
        }
        if ui.menu_item(im_str!("quest objective")).build() {
            result = Some(JournalObjective);
        }
        if ui.menu_item(im_str!("quest mappin")).build() {
            result = Some(JournalMappin);
        }
        if ui.menu_item(im_str!("quest outcome")).build() {
            result = Some(JournalQuestOutcome);
        }
    });

    if ui.menu_item(im_str!("sub segment")).build() {
        result = Some(SubSegment);
    }
    if ui.menu_item(im_str!("scene")).build() {
        result = Some(Scene);
    }
    if ui.menu_item(im_str!("interaction")).build() {
        result = Some(Interaction);
    }
    ui.menu(im_str!("script...")).build(|| {
        if ui.menu_item(im_str!("custom")).build() {
            result = Some(Script("scriptcall".into()));
        }
        ui.separator();
        for (category, templates) in scripttemplates {
            let cat: &str = category.as_ref();
            if cat.is_empty() {
                for template in templates {
                    if ui.menu_item(template.name()).build() {
                        result = Some(Script(template.id().into()));
                    }
                }
            } else {
                ui.menu(category).build(|| {
                    for template in templates {
                        if ui.menu_item(template.name()).build() {
                            result = Some(Script(template.id().into()));
                        }
                    }
                });
            }
        }
    });
    if ui.menu_item(im_str!("add fact")).build() {
        result = Some(AddFact);
    }
    if ui.menu_item(im_str!("teleport")).build() {
        result = Some(Teleport);
    }
    if ui.menu_item(im_str!("change world")).build() {
        result = Some(ChangeWorld);
    }
    if ui.menu_item(im_str!("reward")).build() {
        result = Some(Reward);
    }
    if ui.menu_item(im_str!("randomize")).build() {
        result = Some(Randomize);
    }

    ui.menu(im_str!("communities...")).build(|| {
        if ui.menu_item(im_str!("spawn")).build() {
            result = Some(Spawn);
        }
        if ui.menu_item(im_str!("despawn")).build() {
            result = Some(Despawn);
        }
        if ui.menu_item(im_str!("spawn and despawn")).build() {
            result = Some(SpawnAndDespawn);
        }
    });
    ui.menu(im_str!("time adjustments...")).build(|| {
        if ui.menu_item(im_str!("pause time")).build() {
            result = Some(PauseTime);
        }
        if ui.menu_item(im_str!("unpause time")).build() {
            result = Some(UnpauseTime);
        }
        if ui.menu_item(im_str!("set time")).build() {
            result = Some(SetTime);
        }
        if ui.menu_item(im_str!("shift time")).build() {
            result = Some(ShiftTime);
        }
    });

    result
}
// ----------------------------------------------------------------------------
#[inline]
fn render_block_creation_items<'ui>(
    ui: &Ui<'ui>,
    selection: &SelectionState,
    config: &Config,
    structure: &QuestStructure,
    result: &mut Option<MenuAction>,
) {
    let io_block_count = structure
        .segment(&selection.segment.1)
        .map(segments::QuestSegment::in_out_block_count)
        .unwrap_or((0, 0));

    if let Some(action) = render_plane_context_menu(
        ui,
        selection.segment.0 == 0,
        &config.scripttemplates,
        io_block_count,
    ) {
        *result = Some(MenuAction::BlockCreation(action));
    }
}
// ----------------------------------------------------------------------------
#[inline]
fn render_block_context_menu<'ui>(
    ui: &Ui<'ui>,
    blockid: &BlockId,
    blocks: &Blocks,
    segmentid: &SegmentId,
    structure: &QuestStructure,
) -> Option<MenuAction> {
    let mut result = None;

    if let Some(graphblock) = blocks.get(blockid) {
        ui.text(graphblock.id().type_caption());
        ui.separator();

        // block type specific context menu entries
        if let Ok(questblock) = cmds::find_block(segmentid, blockid, structure) {
            result = blocks::view::menu::draw_block_context_menu(
                ui,
                questblock.as_questblock(),
                graphblock,
            );
        }

        // generic operations for all blocks
        if ui.menu_item(im_str!("delete block")).build() {
            result = Some(MenuAction::BlockDeletion(blockid.to_owned()));
        }
    }
    result
}
// ----------------------------------------------------------------------------
fn render_block_context_menu_entries<'ui>(
    ui: &Ui<'ui>,
    blockid: &BlockId,
    segmentid: &SegmentId,
    blocks: &Blocks,
    structure: &QuestStructure,
) -> Option<MenuAction> {
    let mut result = None;

    let graphblock = blocks.get(blockid);
    let questblock = cmds::find_block(segmentid, blockid, structure);

    // block type specific context menu entries
    if let (Ok(questblock), Some(graphblock)) = (questblock, graphblock) {
        if let Some(action) =
            blocks::view::menu::draw_block_context_menu(ui, questblock.as_questblock(), graphblock)
        {
            result = Some(action);
        }

        if ui.menu_item(im_str!("delete selected")).build() {
            result = Some(MenuAction::BlockDeletion(blockid.to_owned()));
        }
    }
    result
}
// ----------------------------------------------------------------------------
