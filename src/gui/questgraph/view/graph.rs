//
// questgraph::view::graph
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[allow(clippy::ptr_arg)]
pub(in gui::questgraph) fn render_plane<'ui>(
    ui: &Ui<'ui>,
    area: &UiArea,
    plane: &PlaneState,
    edges: &Edges,
    blocks: &Blocks,
) -> Option<GraphAction> {
    let mut result = None;

    let graph_area = (area.size.0 - 15.0, area.size.1 - 15.0);

    ui.window(im_str!("graph"))
        .title_bar(false)
        .menu_bar(false)
        .movable(false)
        .resizable(false)
        .no_bring_to_front_on_focus(true)
        .position(area.pos, imgui::ImGuiCond::Always)
        .size(area.size, imgui::ImGuiCond::Always)
        .build(|| {
            result = graph::show(ui, graph_area, plane, blocks, edges);
        });

    result
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui;
use imgui::Ui;

use imgui_controls::graph;

// actions
use super::GraphAction;

// state
use super::PlaneState;

use super::UiArea;
use super::{Blocks, Edges};
// ----------------------------------------------------------------------------
