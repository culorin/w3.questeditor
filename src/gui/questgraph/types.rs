//
// some wrapper types for graph
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Clone, Default, Debug)]
pub struct InSocketId(primitives::InSocketId, ImString);

#[derive(Clone, Debug)]
pub struct OutSocketId(primitives::OutSocketId, ImString);
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd};
use imgui::{ImStr, ImString};

use model::questgraph::primitives;
// ----------------------------------------------------------------------------
// InSocketId
// ----------------------------------------------------------------------------
impl AsRef<ImStr> for InSocketId {
    fn as_ref(&self) -> &ImStr {
        self.1.as_ref()
    }
}
// ----------------------------------------------------------------------------
impl Eq for InSocketId {}
// ----------------------------------------------------------------------------
impl PartialEq for InSocketId {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}
// ----------------------------------------------------------------------------
impl PartialEq<primitives::InSocketId> for InSocketId {
    fn eq(&self, other: &primitives::InSocketId) -> bool {
        self.0.eq(other)
    }
}
// ----------------------------------------------------------------------------
impl PartialEq<str> for InSocketId {
    fn eq(&self, other: &str) -> bool {
        self.0.eq(other)
    }
}
// ----------------------------------------------------------------------------
impl Ord for InSocketId {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}
// ----------------------------------------------------------------------------
impl PartialOrd for InSocketId {
    fn partial_cmp(&self, other: &Self) -> Option<::std::cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a str> for InSocketId {
    fn from(socket: &str) -> InSocketId {
        InSocketId(
            primitives::InSocketId(socket.to_owned()),
            ImString::new(socket.to_owned()),
        )
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a primitives::InSocketId> for InSocketId {
    fn from(socket: &primitives::InSocketId) -> InSocketId {
        InSocketId(socket.clone(), ImString::new(socket.0.to_owned()))
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a InSocketId> for primitives::InSocketId {
    fn from(socket: &InSocketId) -> primitives::InSocketId {
        socket.0.clone()
    }
}
// ----------------------------------------------------------------------------
// OutSocketId
// ----------------------------------------------------------------------------
impl AsRef<ImStr> for OutSocketId {
    fn as_ref(&self) -> &ImStr {
        self.1.as_ref()
    }
}
// ----------------------------------------------------------------------------
impl Eq for OutSocketId {}
// ----------------------------------------------------------------------------
impl PartialEq for OutSocketId {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}
// ----------------------------------------------------------------------------
impl PartialEq<primitives::OutSocketId> for OutSocketId {
    fn eq(&self, other: &primitives::OutSocketId) -> bool {
        self.0.eq(other)
    }
}
// ----------------------------------------------------------------------------
impl PartialEq<str> for OutSocketId {
    fn eq(&self, other: &str) -> bool {
        self.0.eq(other)
    }
}
// ----------------------------------------------------------------------------
impl Ord for OutSocketId {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}
// ----------------------------------------------------------------------------
impl PartialOrd for OutSocketId {
    fn partial_cmp(&self, other: &Self) -> Option<::std::cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a str> for OutSocketId {
    fn from(socket: &str) -> OutSocketId {
        OutSocketId(
            primitives::OutSocketId(socket.to_owned()),
            ImString::new(socket.to_owned()),
        )
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a primitives::OutSocketId> for OutSocketId {
    fn from(socket: &primitives::OutSocketId) -> OutSocketId {
        OutSocketId(socket.clone(), ImString::new(socket.0.to_owned()))
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a OutSocketId> for primitives::OutSocketId {
    fn from(socket: &OutSocketId) -> primitives::OutSocketId {
        socket.0.clone()
    }
}
// ----------------------------------------------------------------------------
