//
// questgraph::cmds - more complex actions with (possible) side effects
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(super) fn init_block_editordata(structure: &mut QuestStructure) -> Result<(), String> {
    use model::questgraph::SegmentBlock::*;

    let mut segment_io = HashMap::new();
    for segment in structure.segments() {
        segment_io.insert(segment.id().clone(), segment.in_out_ids());
    }

    let init_editordata = |block: &mut SegmentBlock| {
        match block {
            WaitUntil(block) => {
                // attach editor hint for all waituntil blocks
                blocks::cmds::inject_waituntil_hint(block);
            }
            SubSegment(block) => {
                // make sure only correct sockets are in editordata
                blocks::cmds::inject_subsegment_editordata(block, &segment_io);
            }
            Layers(block) => {
                // attach editor hint for all layers blocks
                blocks::cmds::inject_layers_hint(block);
            }
            Spawnsets(block) => {
                // attach editor hint for all spawnset variants
                blocks::cmds::inject_spawnsets_hint(block);
            }
            _ => {}
        }
    };

    structure
        .root_mut()?
        .segment_blocks_mut()
        .for_each(&init_editordata);

    structure
        .segments_mut()
        .flat_map(QuestSegment::segment_blocks_mut)
        .for_each(&init_editordata);

    Ok(())
}
// ----------------------------------------------------------------------------
/// stores current block positions and connections into currently edited quest
/// segment
pub(super) fn update_segment_graph(
    segmentid: &SegmentId,
    structure: &mut QuestStructure,
    blocks: &Blocks,
    edges: &[Edge],
    plane: &super::PlaneState,
) -> Result<(), String> {
    let editor_settings = update_segment_blocks(segmentid, structure, blocks, edges)?;

    // store current editor settings in segment
    editor_settings.zoom = plane.zoom();
    editor_settings.pos = plane.offset().into();

    Ok(())
}
// ----------------------------------------------------------------------------
pub(super) fn extract_segment_graph(
    segmentid: &SegmentId,
    structure: &QuestStructure,
) -> Result<(Rectangle, Blocks, Edges, EditorSegmentData), String> {
    // use settings stored in segment or default ones
    let editor_settings = if segmentid.is_quest_root() {
        structure.root()?.editordata()
    } else {
        structure
            .segment(segmentid)
            .ok_or("segment not found")?
            .editordata()
    }
    .cloned()
    .unwrap_or(EditorSegmentData {
        pos: (0.0, 0.0),
        zoom: DEFAULT_GRAPH_ZOOM,
    });

    let (mut graphblocks, with_editordata) = extract_segment_blocks(segmentid, structure)?;
    let mut graphedges = extract_segment_blockedges(segmentid, structure)?;

    // check edges for valid end blockids (edge is extracted from a block so start
    // is always valid)
    for edge in &graphedges {
        if !graphblocks.contains_key(&edge.to().0) {
            return Err(format!(
                "segment {} contains a block with a link pointing to an invalid block. invalid link: [{}] -> [{}]",
                segmentid,
                edge.from().0,
                edge.to().0,
            ));
        }
    }

    // since some blocks can have multiple custom defined in-sockets (e.g. scenes) and
    // in-sockets are only referenced by connections, they have to be extracted
    // in a separate pass (over all edges) and blocks need to be updated
    add_sockets_to_blocks(&graphedges, &mut graphblocks)?;

    let dim = if with_editordata {
        graph::calculate_graph_dimension(&graphblocks)
    } else {
        // auto layout only if (at least) one block without position setting was found
        graph::auto_layout_blocks(&mut graphblocks, &mut graphedges)
    };

    graph::refresh_edge_positions(&graphblocks, &mut graphedges);

    Ok((dim, graphblocks, graphedges, editor_settings))
}
// ----------------------------------------------------------------------------
pub(super) fn extract_segment_blocks(
    segmentid: &SegmentId,
    structure: &QuestStructure,
) -> Result<(Blocks, bool), String> {
    if segmentid.is_quest_root() {
        collect_blocks(structure.root()?.quest_blocks())
    } else {
        collect_blocks(
            structure
                .segment(segmentid)
                .ok_or("segment not found")?
                .quest_blocks(),
        )
    }
}
// ----------------------------------------------------------------------------
pub(super) fn extract_segment_blockedges(
    segmentid: &SegmentId,
    structure: &QuestStructure,
) -> Result<Vec<Edge>, String> {
    let graphedges = if segmentid.is_quest_root() {
        collect_edges(structure.root()?.quest_blocks())
    } else {
        collect_edges(
            structure
                .segment(segmentid)
                .ok_or("segment not found")?
                .quest_blocks(),
        )
    };
    Ok(graphedges)
}
// ----------------------------------------------------------------------------
pub(super) fn create_new_segment(
    segmentid: &SegmentId,
    structure: &mut QuestStructure,
    selection: &mut SelectionState,
) -> Result<(), String> {
    use model::questgraph::segments::{SegmentIn, SegmentOut};

    let segmentid: &str = segmentid;

    let mut segment = QuestSegment::new(segmentid);

    segment.add_input(SegmentIn::new("In"))?;
    segment.add_output(SegmentOut::new("Out"))?;

    // -- set some default editor settings for better visibility
    let data = segment.editordata_or_default();
    data.zoom = 1.0;
    data.pos = (0.0, -100.0);

    structure.add_segment(segmentid, segment)?;

    // update selection list with available segments
    selection.sync_segments(structure);

    Ok(())
}
// ----------------------------------------------------------------------------
pub(super) fn delete_segment(
    segmentid: &SegmentId,
    structure: &mut QuestStructure,
    selection: &mut SelectionState,
    graphblocks: &mut Blocks,
    graphedges: &mut Edges,
) -> Result<(), String> {
    structure.delete_segment(segmentid)?;

    let remove_questblock_sockets = |block: &mut SegmentBlock| -> Option<BlockId> {
        match block {
            SegmentBlock::SubSegment(block) if block.segmentlink() == Some(segmentid) => {
                blocks::cmds::remove_subsegment_sockets(block);
                Some(block.uid().blockid().clone())
            }
            _ => None,
        }
    };

    let mut remove_graphblock_sockets = |updated_blocks| {
        for blockid in &updated_blocks {
            if let Some(b) = graphblocks.get_mut(blockid) {
                b.set_in_sockets(vec![]);
                b.set_out_sockets(vec![]);
            }
        }
    };

    let empty_insockets = &vec![];

    let root = structure.root_mut()?;
    let updated_blocks = root
        .segment_blocks_mut()
        .filter_map(&remove_questblock_sockets)
        .collect::<Vec<_>>();

    for blockid in &updated_blocks {
        root.prune_links(blockid, empty_insockets);
    }

    // also remove sockets from the currently shown graphblocks
    if selection.segment.1.is_quest_root() {
        remove_graphblock_sockets(updated_blocks);
    }

    for segment in structure.segments_mut() {
        let updated_blocks = segment
            .segment_blocks_mut()
            .filter_map(&remove_questblock_sockets)
            .collect::<Vec<_>>();

        for blockid in &updated_blocks {
            segment.prune_links(blockid, empty_insockets);
        }

        // also remove sockets from the currently shown graphblocks
        if segment.id() == &selection.segment.1 {
            remove_graphblock_sockets(updated_blocks);
        }
    }

    // current graph still contains edges -> refresh
    if &selection.segment.1 != segmentid {
        refresh_graphedges(&selection.segment.1, structure, graphblocks, graphedges)?;
    }

    // update selection list with available segments
    selection.sync_segments(structure);

    Ok(())
}
// ----------------------------------------------------------------------------
pub(super) fn find_block<'def>(
    segmentid: &SegmentId,
    blockid: &BlockId,
    structure: &'def QuestStructure,
) -> Result<&'def dyn blocks::EditableQuestBlock, String> {
    if segmentid.is_quest_root() {
        let seg = structure.root()?;

        seg.segment_block(blockid)
            .map(|b| b as &dyn blocks::EditableQuestBlock)
            .or_else(|| {
                seg.end_block(blockid)
                    .map(|b| b as &dyn blocks::EditableQuestBlock)
            })
            .or_else(|| {
                seg.start_block(blockid)
                    .map(|b| b as &dyn blocks::EditableQuestBlock)
            })
    } else {
        let seg = structure
            .segment(segmentid)
            .ok_or("unknown segment id provided for search")?;

        seg.segment_block(blockid)
            .map(|b| b as &dyn blocks::EditableQuestBlock)
            .or_else(|| {
                seg.out_block(blockid)
                    .map(|b| b as &dyn blocks::EditableQuestBlock)
            })
            .or_else(|| {
                seg.in_block(blockid)
                    .map(|b| b as &dyn blocks::EditableQuestBlock)
            })
    }
    .ok_or_else(|| "block not found in segment".into())
}
// ----------------------------------------------------------------------------
pub(super) fn find_block_mut<'def>(
    segmentid: &SegmentId,
    blockid: &BlockId,
    structure: &'def mut QuestStructure,
) -> Result<&'def mut dyn blocks::EditableQuestBlock, String> {
    if segmentid.is_quest_root() {
        let seg = structure.root_mut()?;

        // multiple mutable borrow issue workaound...
        if seg.segment_block_mut(blockid).is_some() {
            Ok(seg.segment_block_mut(blockid).unwrap())
        } else if seg.end_block_mut(blockid).is_some() {
            Ok(seg.end_block_mut(blockid).unwrap())
        } else if seg.start_block_mut(blockid).is_some() {
            Ok(seg.start_block_mut(blockid).unwrap())
        } else {
            Err("block not found in segment".into())
        }
    } else {
        let seg = structure
            .segment_mut(segmentid)
            .ok_or("unknown segment id provided for search")?;

        // multiple mutable borrow issue workaound...
        if seg.segment_block_mut(blockid).is_some() {
            Ok(seg.segment_block_mut(blockid).unwrap())
        } else if seg.out_block_mut(blockid).is_some() {
            Ok(seg.out_block_mut(blockid).unwrap())
        } else if seg.in_block_mut(blockid).is_some() {
            Ok(seg.in_block_mut(blockid).unwrap())
        } else {
            Err("block not found in segment".into())
        }
    }
}
// ----------------------------------------------------------------------------
pub(super) fn find_segmentblock_mut<'def>(
    segmentid: &SegmentId,
    blockid: &BlockId,
    structure: &'def mut QuestStructure,
) -> Result<&'def mut SegmentBlock, String> {
    if segmentid.is_quest_root() {
        let seg = structure.root_mut()?;

        if let Some(block) = seg.segment_block_mut(blockid) {
            Ok(block)
        } else {
            Err("block not found in segment".into())
        }
    } else {
        let seg = structure
            .segment_mut(segmentid)
            .ok_or("unknown segment id provided for search")?;

        // multiple mutable borrow issue workaound...
        if let Some(block) = seg.segment_block_mut(blockid) {
            Ok(block)
        } else {
            Err("block not found in segment".into())
        }
    }
}
// ----------------------------------------------------------------------------
/// given a rename of one in/out block in a specific segment, the respective
/// socket of *all* subsegment blocks in the quest structure which reference
/// this segment will be renamed. additionally links to this socket from *all*
/// other blocks will be updated, too.
pub(super) fn sync_subsegment_in_out_changes(
    structure: &mut QuestStructure,
    updated_segmentid: &SegmentId,
    old_in_out_blockid: &BlockId,
    new_in_out_blockid: &BlockId,
) -> Result<(), String> {
    match old_in_out_blockid {
        BlockId::SegmentIn(_) => sync_subsegment_in_changes(
            structure,
            updated_segmentid,
            old_in_out_blockid,
            new_in_out_blockid,
        ),
        BlockId::SegmentOut(_) => sync_subsegment_out_changes(
            structure,
            updated_segmentid,
            old_in_out_blockid,
            new_in_out_blockid,
        ),
        _ => Err(String::from(
            "tried to sync io segment changes on a non subsegment block",
        )),
    }
}
// ----------------------------------------------------------------------------
/// updates the blockname part of the blockid and all linktargets of all other
/// blocks in the segment
pub(super) fn rename_block<'def>(
    selection: &mut SelectionState,
    blockid: &BlockId,
    new_name: &str,
    structure: &'def mut QuestStructure,
    planestate: &mut PlaneState,
    graphblocks: &mut Blocks,
    graphedges: &mut Edges,
) -> Result<BlockId, String> {
    let segmentid = &selection.segment.1;

    // renaming is a complex operation because graph edges need to be updated after renaming
    // BUT user action for creating/modifying/removing graphedges is not synced immediately
    // (only on segment changes) -> so at this point some edges might not be synced with
    // questgraph!
    trace!("renaming block {:?} -> {}", blockid, new_name);

    // 1. sync graph -> queststructure
    update_segment_graph(segmentid, structure, graphblocks, graphedges, planestate)?;

    // 2. rename questblock
    if segmentid.is_quest_root() {
        structure.root_mut()?.rename_block(blockid, new_name)?;
    } else {
        structure
            .segment_mut(segmentid)
            .ok_or("unknown segment id provided for search")?
            .rename_block(blockid, new_name)?;
    }
    let new_id = blockid.cloned_with_name(new_name);

    // remove old block (*before* refreshing!)
    graphblocks.remove(blockid);

    refresh_graph_after_questblock_update(
        selection,
        &new_id,
        structure,
        planestate,
        graphblocks,
        graphedges,
    )?;

    Ok(new_id)
}
// ----------------------------------------------------------------------------
/// deletes a block in currently selected segment and all of the links to and
/// from it. selection list will be updated
pub(in gui::questgraph) fn delete_block<'def>(
    selection: &mut SelectionState,
    blockid: &BlockId,
    structure: &'def mut QuestStructure,
    planestate: &mut PlaneState,
    graphblocks: &mut Blocks,
    graphedges: &mut Edges,
) -> Result<(), String> {
    let segmentid = &selection.segment.1;

    // deleting is a complex operation because graph edges need to be updated after delete
    // BUT user action for creating/modifying/removing graphedges is not synced immediately
    // (only on segment changes) -> so at this point some edges might not be synced with
    // questgraph!
    trace!("deleting block {:?}", blockid);

    // 1. sync graph -> queststructure
    update_segment_graph(segmentid, structure, graphblocks, graphedges, planestate)?;

    // 2. delete questblock
    if segmentid.is_quest_root() {
        structure.root_mut()?.delete_block(blockid)?;
    } else {
        structure
            .segment_mut(segmentid)
            .ok_or("unknown segment id provided for search")?
            .delete_block(blockid)?;
    }

    // 3. remove block from graphblocks
    graphblocks.remove(blockid);

    // 4. refresh *all* graph edges
    // since blocknames have changed all edges must be regenerated from
    // quest structure (the respective target blockid must be updated)
    refresh_graphedges(segmentid, structure, graphblocks, graphedges)?;

    // 5. refresh block selection list
    selection.deselect_block();
    selection.sync_blocks(graphblocks);

    Ok(())
}
// ----------------------------------------------------------------------------
pub(super) fn select_block(
    blockid: &BlockId,
    selection: &mut SelectionState,
    planestate: &mut PlaneState,
    graphblocks: &mut Blocks,
    graphedges: &mut Edges,
) {
    graph::action::select_block(blockid, planestate, graphblocks, graphedges);
    selection.select_block(blockid);
}
// ----------------------------------------------------------------------------
pub(super) fn deselect_block(
    selection: &mut SelectionState,
    planestate: &mut PlaneState,
    graphblocks: &mut Blocks,
    graphedges: &mut Edges,
) {
    graph::action::deselect_block(planestate, graphblocks, graphedges);
    selection.deselect_block();
}
// ----------------------------------------------------------------------------
/// recreates graphblock from questblock and refreshes graph (block + all edges)
/// reselects this block
pub(super) fn refresh_graph_after_questblock_update<'def>(
    selection: &mut SelectionState,
    blockid: &BlockId,
    structure: &'def mut QuestStructure,
    planestate: &mut PlaneState,
    graphblocks: &mut Blocks,
    graphedges: &mut Edges,
) -> Result<(), String> {
    let segmentid = &selection.segment.1;

    // 1. refresh block in graph (by recreating a graphblock) since structural
    // changes must be updated in graphblocks (delete + recreate from questblock
    // is most simple solution as multiple things need to be updated: name,
    // tooltip, etc.)
    let block = find_block(segmentid, blockid, structure)?;
    let graphblock = blocks::cmds::create_graphblock_from_questblock(block.as_questblock())?;
    graphblocks.remove(blockid);
    graphblocks.insert(graphblock.id().clone(), graphblock);

    // 2. refresh *all* graph edges
    // since blocknames have changed all edges must be regenerated from
    // quest structure (the respective target blockid must be updated)
    refresh_graphedges(segmentid, structure, graphblocks, graphedges)?;

    // 3. refresh block selection as id/name may have changed
    // 3.1 list needs updated names
    selection.sync_blocks(graphblocks);

    // 3.2 list + graph need to mark selected block
    select_block(blockid, selection, planestate, graphblocks, graphedges);
    Ok(())
}
// ----------------------------------------------------------------------------
pub(super) fn sync_block_sockets(
    questblock: &mut dyn blocks::EditableQuestBlock,
    graphblocks: &Blocks,
) {
    let id = questblock.uid().blockid();
    if let Some(graphblock) = graphblocks.get(id) {
        match id {
            BlockId::Scene(_)
            | BlockId::Script(_)
            | BlockId::Interaction(_)
            | BlockId::WaitUntil(_)
            | BlockId::Randomize(_)
            | BlockId::SubSegment(_) => {
                let sockets = questblock.editordata_or_default();

                sockets.in_sockets = graphblock.in_sockets().iter().map(|s| s.into()).collect();
                sockets.out_sockets = graphblock.out_sockets().iter().map(|s| s.into()).collect();
            }
            _ => {}
        }
    }
}
// ----------------------------------------------------------------------------
pub(super) fn add_new_subsegment_socket(
    changed_segment: &SegmentId,
    structure: &mut QuestStructure,
) -> Result<(), String> {
    if let Some((in_sockets, out_sockets)) = structure
        .segment(changed_segment)
        .map(QuestSegment::in_out_ids)
    {
        let update_sockets = |block: &mut SegmentBlock| match block {
            SegmentBlock::SubSegment(block) if block.segmentlink() == Some(changed_segment) => {
                blocks::cmds::update_subsegment_sockets(block, &in_sockets, &out_sockets)
            }
            _ => {}
        };

        structure
            .root_mut()?
            .segment_blocks_mut()
            .for_each(&update_sockets);

        structure
            .segments_mut()
            .flat_map(QuestSegment::segment_blocks_mut)
            .for_each(&update_sockets);
    }
    Ok(())
}
// ----------------------------------------------------------------------------
pub(super) fn delete_subsegment_socket(
    changed_segment: &SegmentId,
    structure: &mut QuestStructure,
) -> Result<(), String> {
    if let Some((in_sockets, out_sockets)) = structure
        .segment(changed_segment)
        .map(QuestSegment::in_out_ids)
    {
        let update_sockets = |block: &mut SegmentBlock| -> Option<BlockId> {
            match block {
                SegmentBlock::SubSegment(block) if block.segmentlink() == Some(changed_segment) => {
                    blocks::cmds::update_subsegment_sockets(block, &in_sockets, &out_sockets);
                    Some(block.uid().blockid().clone())
                }
                _ => None,
            }
        };

        let root = structure.root_mut()?;

        let updated_blocks = root
            .segment_blocks_mut()
            .filter_map(&update_sockets)
            .collect::<Vec<_>>();

        for blockid in &updated_blocks {
            root.prune_links(blockid, &in_sockets);
        }

        for segment in structure.segments_mut() {
            let updated_blocks = segment
                .segment_blocks_mut()
                .filter_map(&update_sockets)
                .collect::<Vec<_>>();

            for blockid in &updated_blocks {
                segment.prune_links(blockid, &in_sockets);
            }
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
pub(super) fn prune_links_to_block(
    structure: &mut QuestStructure,
    segmentid: &SegmentId,
    blockid: &BlockId,
    valid_sockets: &[primitives::InSocketId],
) -> Result<(), String> {
    if segmentid.is_quest_root() {
        structure.root_mut()?.prune_links(blockid, valid_sockets);
    } else {
        structure
            .segment_mut(segmentid)
            .ok_or("unknown segment id provided for link pruning")?
            .prune_links(blockid, valid_sockets);
    }
    Ok(())
}
// ----------------------------------------------------------------------------
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::convert::TryFrom;

use imgui_controls::graph;
use imgui_widgets::Rectangle;

use model::questgraph::primitives;
use model::questgraph::primitives::EditorSegmentData;
use model::questgraph::segments::QuestSegment;
use model::questgraph::{QuestBlock, QuestStructure, SegmentBlock};

use super::{BlockId, SegmentId};
use super::{Blocks, Edge, Edges};
use super::{PlaneState, SelectionState};

use super::blocks;
use super::types::{InSocketId, OutSocketId};

use super::DEFAULT_ZOOM as DEFAULT_GRAPH_ZOOM;
// ----------------------------------------------------------------------------
// initialization of graph data
// ----------------------------------------------------------------------------
fn collect_edges<'a, I>(questblocks: I) -> Vec<Edge>
where
    I: Iterator<Item = &'a dyn QuestBlock>,
{
    let mut result = Vec::new();

    for block in questblocks {
        let id = block.uid().blockid();

        for (out_socket, links) in block.links() {
            for link in links {
                if let Ok(target_block) = BlockId::try_from(link.target_block()) {
                    trace!(
                        "found edge [{:?}:{:?}] -> [{:?}:{:?}]",
                        &id,
                        out_socket,
                        &target_block,
                        link.target_socket()
                    );

                    result.push(Edge::new(
                        id.clone(),
                        out_socket.into(),
                        target_block.clone(),
                        link.target_socket().into(),
                    ))
                }
            }
        }
    }
    result
}
// ----------------------------------------------------------------------------
fn refresh_graphedges(
    segmentid: &SegmentId,
    structure: &QuestStructure,
    blocks: &mut Blocks,
    edges: &mut Edges,
) -> Result<(), String> {
    *edges = extract_segment_blockedges(segmentid, structure)?;

    // some sockets are implicitely defined by existing edges and need to be
    // attached after edges are extracted from structure
    add_sockets_to_blocks(edges, blocks)?;

    graph::refresh_edge_positions(blocks, edges);

    Ok(())
}
// ----------------------------------------------------------------------------
fn collect_blocks<'a, I>(questblocks: I) -> Result<(Blocks, bool), String>
where
    I: Iterator<Item = &'a dyn QuestBlock>,
{
    let mut graphblocks = BTreeMap::new();
    let mut all_editordata = true;

    for block in questblocks {
        all_editordata = all_editordata && block.editordata().is_some();

        let graphblock = blocks::cmds::create_graphblock_from_questblock(block)?;
        graphblocks.insert(block.uid().blockid().clone(), graphblock);
    }
    Ok((graphblocks, all_editordata))
}
// ----------------------------------------------------------------------------
fn add_sockets_to_blocks(edges: &[Edge], blocks: &mut Blocks) -> Result<(), String> {
    use self::blocks::EditableGraphBlock;

    // extract all in-sockets, group by block, sort them, attach to blocks (or
    // use default in-sockets if no edge was found)

    let mut out_sockets: HashMap<&BlockId, Vec<_>> = HashMap::with_capacity(blocks.len());
    let mut in_sockets: HashMap<&BlockId, Vec<_>> = HashMap::with_capacity(blocks.len());

    for edge in edges {
        in_sockets
            .entry(&edge.to().0)
            .or_insert_with(Vec::new)
            .push(edge.to().1.clone());
        out_sockets
            .entry(&edge.from().0)
            .or_insert_with(Vec::new)
            .push(edge.from().1.clone());
    }

    for (id, block) in blocks.iter_mut() {
        let mut in_sockets: Vec<InSocketId> = in_sockets.remove(&id).unwrap_or_default();
        let mut out_sockets: Vec<OutSocketId> = out_sockets.remove(&id).unwrap_or_default();

        // some blocks (e.g. scenes, interactions, scripts, subsegments) may already have some
        // (empty) sockets attached from editordata (so they survive saving). those sockets
        // will NOT be found in the edges and must be added separately
        // -> add to the list (will be deduped in set_sockets anyway)
        in_sockets.append(&mut block.in_sockets().clone());
        out_sockets.append(&mut block.out_sockets().clone());

        block.set_sockets(in_sockets, out_sockets)?;
        // if there are still no sockets add default sockets
        block.add_missing_sockets();
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn update_segment_blocks<'def>(
    segmentid: &SegmentId,
    structure: &'def mut QuestStructure,
    graphblocks: &Blocks,
    graphedges: &[Edge],
) -> Result<&'def mut EditorSegmentData, String> {
    // create lookup table for edges
    let mut edges: HashMap<&BlockId, Vec<_>> = HashMap::with_capacity(graphblocks.len());
    for e in graphedges {
        edges.entry(&e.from().0).or_insert_with(Vec::new).push(e);
    }

    // all blocks are already in structure (on creation a block is stored in structure)
    // so it's save to iterate over structure blocks
    if segmentid.is_quest_root() {
        let seg = structure.root_mut()?;

        for questblock in seg.start_blocks_mut() {
            sync_block_pos(questblock, graphblocks);
            sync_block_links(questblock, &edges)?;
        }

        for questblock in seg.segment_blocks_mut() {
            sync_block_pos(questblock, graphblocks);
            sync_block_sockets(questblock, graphblocks);
            sync_block_links(questblock, &edges)?;
        }

        for questblock in seg.end_blocks_mut() {
            sync_block_pos(questblock, graphblocks);
        }

        Ok(seg.editordata_or_default())
    } else {
        let seg = structure
            .segment_mut(segmentid)
            .ok_or("unknown segment id provided for search")?;

        for questblock in seg.in_blocks_mut() {
            sync_block_pos(questblock, graphblocks);
            sync_block_links(questblock, &edges)?;
        }

        for questblock in seg.segment_blocks_mut() {
            sync_block_pos(questblock, graphblocks);
            sync_block_sockets(questblock, graphblocks);
            sync_block_links(questblock, &edges)?;
        }

        for questblock in seg.out_blocks_mut() {
            sync_block_pos(questblock, graphblocks);
        }

        Ok(seg.editordata_or_default())
    }
}
// ----------------------------------------------------------------------------
#[inline]
fn sync_block_pos(questblock: &mut dyn blocks::EditableQuestBlock, graphblocks: &Blocks) {
    let id = questblock.uid().blockid();
    if let Some(graphblock) = graphblocks.get(id) {
        questblock.editordata_or_default().pos = graphblock.pos();
    }
}
// ----------------------------------------------------------------------------
#[inline]
fn sync_block_links(
    questblock: &mut dyn blocks::EditableQuestBlock,
    graphedges: &HashMap<&BlockId, Vec<&Edge>>,
) -> Result<(), String> {
    questblock.reset_links();

    if let Some(edges) = graphedges.get(questblock.uid().blockid()) {
        for edge in edges {
            questblock.add_link_to(
                Some(primitives::OutSocketId::from(&edge.from().1)),
                primitives::LinkTarget::try_from(&edge.to().0)?,
                Some(primitives::InSocketId::from(&edge.to().1)),
            );
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn sync_subsegment_in_changes(
    structure: &mut QuestStructure,
    updated_segmentid: &SegmentId,
    old_in_out_blockid: &BlockId,
    new_in_out_blockid: &BlockId,
) -> Result<(), String> {
    use model::questgraph::primitives::InSocketId;

    let old_socket = InSocketId::from(old_in_out_blockid.name().as_str());
    let new_socket = InSocketId::from(new_in_out_blockid.name().as_str());

    let rename_in_socket = |block: &mut SegmentBlock| -> Option<BlockId> {
        match block {
            SegmentBlock::SubSegment(block) if block.segmentlink() == Some(updated_segmentid) => {
                blocks::cmds::rename_in_socket(block, &old_socket, &new_socket);
                Some(block.uid().blockid().clone())
            }
            _ => None,
        }
    };

    let root = structure.root_mut()?;

    let updated_blocks = root
        .segment_blocks_mut()
        .filter_map(&rename_in_socket)
        .collect::<Vec<_>>();

    for blockid in &updated_blocks {
        root.change_link_targets(blockid, &old_socket, blockid, &new_socket);
    }

    for segment in structure.segments_mut() {
        let updated_blocks = segment
            .segment_blocks_mut()
            .filter_map(&rename_in_socket)
            .collect::<Vec<_>>();

        for blockid in &updated_blocks {
            segment.change_link_targets(blockid, &old_socket, blockid, &new_socket);
        }
    }

    Ok(())
}
// ----------------------------------------------------------------------------
fn sync_subsegment_out_changes(
    structure: &mut QuestStructure,
    updated_segmentid: &SegmentId,
    old_in_out_blockid: &BlockId,
    new_in_out_blockid: &BlockId,
) -> Result<(), String> {
    use model::questgraph::primitives::OutSocketId;

    let old_socket = OutSocketId::from(old_in_out_blockid.name().as_str());
    let new_socket = OutSocketId::from(new_in_out_blockid.name().as_str());

    let rename_out_socket = |block: &mut SegmentBlock| match block {
        SegmentBlock::SubSegment(block) if block.segmentlink() == Some(updated_segmentid) => {
            blocks::cmds::rename_out_socket(block, &old_socket, &new_socket);
        }
        _ => {}
    };

    structure
        .root_mut()?
        .segment_blocks_mut()
        .for_each(&rename_out_socket);

    structure
        .segments_mut()
        .flat_map(QuestSegment::segment_blocks_mut)
        .for_each(&rename_out_socket);

    Ok(())
}
// ----------------------------------------------------------------------------
