//
// interactive editing
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
// state
// ----------------------------------------------------------------------------
pub(super) struct GraphState {
    edges: Edges,
    blocks: Blocks,

    property_editor: Option<blocks::PropertyEditorState>,

    plane: PlaneState,
    selection: SelectionState,
    interaction_pos: (f32, f32),

    lists: DataListProvider,

    menu: MenuState,
    config: Config,
}
// ----------------------------------------------------------------------------
pub(super) use self::help::HelpTopic;
// ----------------------------------------------------------------------------
// actions
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub(super) enum Action {
    // -- interruptable actions
    OnSelectSegment(SegmentId),
    OnSelectBlock(BlockId),
    OnDeselectBlock,
    SelectSegment(SegmentId),
    SelectBlock(BlockId),
    DeselectBlock,
    StartBlockDrag(BlockId),
    OnDeleteSegment(SegmentId),
    OnCreateSegment,
    // --
    DeleteBlock(BlockId),
    CreateSegment(SegmentId),
    BlockAdded(BlockId),
    GuardModifiedBlock,
    DeleteSegment(SegmentId),
    Graph(GraphAction),
    Menu(MenuAction),
    OpenBlockContextMenu(BlockId),
    AutoLayout,
    UpdateGridSpacing(i32, i32),
    QuestBlockEditor(blocks::BlockEditorAction),
}
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub(super) enum MenuAction {
    LayoutBlocks,
    SnapBlocksToGrid(bool),
    CreateSegment,
    DeleteSegment(SegmentId),
    BlockCreation(blocks::BlockTemplate),
    BlockDeletion(BlockId),
    BlockSpecific(blocks::BlockAction),
}
// ----------------------------------------------------------------------------
// view
// ----------------------------------------------------------------------------
pub(in gui) mod view;
// ----------------------------------------------------------------------------
// action processing
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn running_actions_tick(state: &mut GraphState) -> Option<::gui::Action> {
    // this needs to be resetted here since menu popup open requires a one-time
    // trigger flag and cannot be resetted directly after menu opening since state
    // is immutable on view drawing
    state.menu.open_plane_context_menu = false;
    state.menu.open_block_context_menu = false;

    // i.e. dragging positions
    graph::update_running_actions(&mut state.plane, &mut state.blocks, &mut state.edges)
        .map(Into::into)
}
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn handle_action(
    action: Action,
    state: &mut GraphState,
    def: &mut QuestStructure,
    dirtyflag: &mut bool,
) -> Result<Option<::gui::ActionSequence>, String> {
    update::handle_action(action, state, def, dirtyflag)
}
// ----------------------------------------------------------------------------
pub(super) fn sync_data(state: &GraphState, def: &mut QuestStructure) -> Result<(), String> {
    cmds::update_segment_graph(
        &state.selection.segment.1,
        def,
        &state.blocks,
        &state.edges,
        &state.plane,
    )
}
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn guard_modified_data(state: &Option<GraphState>) -> Option<EditorAction> {
    if let Some(ref state) = state {
        if state
            .property_editor
            .as_ref()
            .map(blocks::PropertyEditorState::changed)
            .unwrap_or(false)
        {
            return Some(guard_modified_properties());
        }
    }
    None
}
// ----------------------------------------------------------------------------
// misc
// ----------------------------------------------------------------------------
pub(super) fn init(
    data: &mut QuestData,
    project_dir: Option<&Path>,
    settings: &Settings,
) -> Result<(), String> {
    let definition = data.definition.as_mut().ok_or("missing questdefinition")?;

    let structure = definition.structure_mut();

    // prepare some editordata that may be missing
    cmds::init_block_editordata(structure)?;

    // always extract blocks from root segment on load
    let (graph_size, blocks, edges, editordata) =
        cmds::extract_segment_graph(&SegmentId::default(), structure)?;

    let selection = SelectionState::new(structure, &blocks);

    let mut state = GraphState {
        blocks,
        edges,
        property_editor: None,
        plane: PlaneState::new(graph_size.extend(GRAPH_EXTENSION)),
        selection,
        lists: DataListProvider::new(definition),
        interaction_pos: (250.0, 250.0),
        menu: MenuState::default(),
        config: Config::new(
            data.definition.as_ref().map_or("QUESTMOD", |def| {
                def.settings().map_or("QUESTMOD", |conf| conf.dlcid())
            }),
            project_dir.map(|dir| settings.script_path(dir)),
            (settings.grid_width.0 as f32, settings.grid_width.1 as f32),
        ),
    };
    state
        .plane
        .set_offset(editordata.pos)
        .set_zoom(editordata.zoom)
        .set_grid_width({
            if settings.snap_to_grid {
                state.config.grid_spacing
            } else {
                (1.0, 1.0)
            }
        });

    data.graph = Some(state);
    Ok(())
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::path::{Path, PathBuf};
use std::rc::Rc;

use indexmap::IndexMap;

use imgui::ImString;
use imgui_controls::graph;

use super::settings::Settings;
use super::{Action as EditorAction, QuestData};

use model::questgraph::primitives::{BlockId, SegmentId};
use model::questgraph::QuestStructure;
use model::QuestDefinition;

use gui::lists::UiOption;
use gui::registry;

use self::blocks::ScriptTemplate;
// ----------------------------------------------------------------------------
mod cmds;
mod types;
mod update;

mod blocks;
mod scripttemplates;

mod help;
mod popup;
// ----------------------------------------------------------------------------
// some type aliases for concrete questgraph types
// ----------------------------------------------------------------------------
use self::types::{InSocketId, OutSocketId};

type Edge = graph::Edge<BlockId, OutSocketId, InSocketId>;
type Edges = graph::Edges<BlockId, OutSocketId, InSocketId>;
type Block = graph::Block<BlockId, OutSocketId, InSocketId>;
type Blocks = graph::Blocks<BlockId, OutSocketId, InSocketId>;
type GraphAction = graph::Interaction<BlockId, OutSocketId, InSocketId>;
// type BlockInteraction = graph::BlockInteraction<BlockId>;
type PlaneState = graph::PlaneState<BlockId>;
// ----------------------------------------------------------------------------
type ScriptTemplates = IndexMap<ImString, Vec<ScriptTemplate>>;
// ----------------------------------------------------------------------------
const GRAPH_EXTENSION: f32 = 5000.0;
const DEFAULT_ZOOM: f32 = 0.5;
// ----------------------------------------------------------------------------
#[derive(Default)]
struct MenuState {
    open_plane_context_menu: bool,
    open_block_context_menu: bool,
}
// ----------------------------------------------------------------------------
struct Config {
    scripttemplates: ScriptTemplates,
    grid_spacing: (f32, f32),
}

// ----------------------------------------------------------------------------
impl Config {
    // ------------------------------------------------------------------------
    fn new(
        questid: &str,
        project_scriptfolder: Option<PathBuf>,
        grid_spacing: (f32, f32),
    ) -> Config {
        Config {
            scripttemplates: self::scripttemplates::generate_list(questid, project_scriptfolder),
            grid_spacing,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// ui rendering helpers
// ----------------------------------------------------------------------------
struct SelectableEntry<T> {
    id: T,
    caption: ImString,
}
// ----------------------------------------------------------------------------
struct SelectionState {
    segment_list: Vec<SelectableEntry<SegmentId>>,
    block_list: Vec<SelectableEntry<BlockId>>,

    segment: (i32, SegmentId),
    block: Option<(i32, BlockId)>,
}
// ----------------------------------------------------------------------------
impl<T> SelectableEntry<T> {
    // ------------------------------------------------------------------------
    fn new(id: T, caption: &str) -> SelectableEntry<T> {
        SelectableEntry {
            id,
            caption: ImString::new(caption.replace('_', " ")),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl SelectionState {
    // ------------------------------------------------------------------------
    fn new(def: &QuestStructure, blocks: &Blocks) -> SelectionState {
        let mut state = SelectionState {
            segment_list: Vec::default(),
            block_list: Vec::default(),
            segment: (0, SegmentId::default()),
            block: None,
        };

        state.sync_segments(def);
        state.sync_blocks(blocks);

        state
    }
    // ------------------------------------------------------------------------
    fn sync_segments(&mut self, def: &QuestStructure) {
        // first segment must always be the root segment
        let mut segments = vec![SelectableEntry::new("quest".into(), "- root segment -")];
        let mut subsegments: Vec<_> = def
            .segments()
            .map(|segment| SelectableEntry::new(segment.id().to_owned(), segment.id()))
            .collect();
        subsegments.sort_by(|a, b| a.id.cmp(&b.id));

        subsegments
            .drain(..)
            .for_each(|segment| segments.push(segment));

        self.segment_list = segments;
    }
    // ------------------------------------------------------------------------
    fn sync_blocks(&mut self, blocks: &Blocks) {
        self.block_list = blocks
            .iter()
            .map(|(id, _)| SelectableEntry::new(id.to_owned(), id.name()))
            .collect();
    }
    // ------------------------------------------------------------------------
    fn select_segment(&mut self, id: &SegmentId) {
        self.segment = self
            .segment_list
            .iter()
            .enumerate()
            .find(|&(_, slot)| &slot.id == id)
            .map(|(i, _)| (i as i32, id.clone()))
            .unwrap_or((0, SegmentId::default()));
    }
    // ------------------------------------------------------------------------
    fn select_block(&mut self, id: &BlockId) {
        self.block = self
            .block_list
            .iter()
            .enumerate()
            .find(|&(_, slot)| &slot.id == id)
            .map(|(i, _)| (i as i32, id.clone()));
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn deselect_block(&mut self) {
        self.block = None;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// helper
// ----------------------------------------------------------------------------
fn guard_modified_properties() -> EditorAction {
    use self::blocks::BlockEditorAction::*;

    EditorAction::Confirm(
        String::from("Blockproperties are modified. Update block?"),
        vec![EditorAction::QuestGraph(SaveProperties.into())],
        vec![EditorAction::QuestGraph(DiscardChanges.into())],
    )
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
impl From<GraphAction> for ::gui::Action {
    fn from(action: GraphAction) -> ::gui::Action {
        Action::Graph(action).into()
    }
}
// ----------------------------------------------------------------------------
impl From<MenuAction> for ::gui::Action {
    fn from(action: MenuAction) -> ::gui::Action {
        Action::Menu(action).into()
    }
}
// ----------------------------------------------------------------------------
// ui lists provider
// ----------------------------------------------------------------------------
type SegmentRef = UiOption<SegmentId>;
type SegmentList = Vec<SegmentRef>;
type CommunityPhaseList = IndexMap<registry::Phase, Vec<registry::Community>>;
// ----------------------------------------------------------------------------
struct DataListProvider {
    assets: registry::AssetRegistry,
    segments: SegmentList,
    filtered_segments: Rc<SegmentList>,
    spawnset_phases: Rc<CommunityPhaseList>,
}
// ----------------------------------------------------------------------------
impl DataListProvider {
    // ------------------------------------------------------------------------
    fn new(quest: &QuestDefinition) -> DataListProvider {
        let registry = registry::AssetRegistry::new(quest);
        let community_phases = Self::extract_phases(&registry.communities());

        DataListProvider {
            assets: registry,
            segments: Self::extract_segments(quest.structure(), None),
            filtered_segments: Rc::new(Self::extract_segments(quest.structure(), None)),
            spawnset_phases: Rc::new(community_phases),
        }
    }
    // ------------------------------------------------------------------------
    fn extract_phases(communities: &Rc<registry::CommunityList>) -> CommunityPhaseList {
        let mut options = IndexMap::new();

        for (community, phases) in communities.iter() {
            for phase in phases {
                let entry = options.entry(phase.clone()).or_insert(Vec::new());
                entry.push((*community).clone());
            }
        }
        options.sort_keys();

        options
    }
    // ------------------------------------------------------------------------
    fn extract_segments(
        structure: &QuestStructure,
        filter: Option<&SegmentId>,
    ) -> SegmentList {
        let mut segments: Vec<_> = structure
            .segments()
            .filter(|segment| Some(segment.id()) != filter)
            .map(|segment| SegmentRef::new(&(*segment.id().as_str())))
            .collect();

        segments.sort();
        segments
    }
    // ------------------------------------------------------------------------
    fn refresh_segments(&mut self, structure: &QuestStructure, ignore: &SegmentId) {
        self.segments = Self::extract_segments(structure, None);
        self.filtered_segments = Rc::new(Self::extract_segments(structure, Some(ignore)));
    }
    // ------------------------------------------------------------------------
    fn segments(&self) -> &SegmentList {
        &self.segments
    }
    // ------------------------------------------------------------------------
    fn filtered_segments(&self) -> Rc<SegmentList> {
        self.filtered_segments.clone()
    }
    // ------------------------------------------------------------------------
    fn scenepoints(&self) -> Rc<registry::TagList> {
        self.assets.scenepoints()
    }
    // ------------------------------------------------------------------------
    fn waypoints(&self) -> Rc<registry::TagList> {
        self.assets.waypoints()
    }
    // ------------------------------------------------------------------------
    fn areas(&self) -> Rc<registry::TagList> {
        self.assets.areas()
    }
    // ------------------------------------------------------------------------
    fn interactions(&self) -> Rc<registry::InteractionList> {
        self.assets.interactions()
    }
    // ------------------------------------------------------------------------
    fn journal_entries(&self) -> Rc<registry::Journals> {
        self.assets.journals()
    }
    // ------------------------------------------------------------------------
    fn journal_quests(&self) -> Rc<registry::JournalQuestList> {
        self.assets.journals().quests()
    }
    // ------------------------------------------------------------------------
    fn layers(&self) -> Rc<registry::LayerList> {
        self.assets.layers()
    }
    // ------------------------------------------------------------------------
    fn communities(&self) -> Rc<registry::CommunityList> {
        self.assets.communities()
    }
    // ------------------------------------------------------------------------
    fn community_phases(&self) -> Rc<CommunityPhaseList> {
        self.spawnset_phases.clone()
    }
    // ------------------------------------------------------------------------
    fn rewards(&self) -> Rc<registry::RewardList> {
        self.assets.rewards()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ::gui::lists::AsStr for SegmentId {
    fn as_str(&self) -> &str {
        &*self
    }
}
// ----------------------------------------------------------------------------
