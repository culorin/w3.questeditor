//
// blocks::questblocks::templates
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
/// creates new questblock, adds it into the segment and returns a block
/// validationresult on successful add
pub(in gui::questgraph::blocks) fn create_new_from_template(
    blocktemplate: BlockTemplate,
    segmentid: &SegmentId,
    id: &BlockId,
    pos: (f32, f32),
    structure: &mut QuestStructure,
    scripttemplates: &ScriptTemplates,
) -> Result<BlockValidationState, String> {
    let validation;

    match blocktemplate {
        // these two are only allowed for subsegments
        BlockTemplate::SegmentIn | BlockTemplate::SegmentOut if segmentid.is_quest_root() => {
            unreachable!("tried to create segment in/out in root segment")
        }

        BlockTemplate::SegmentIn | BlockTemplate::SegmentOut => {
            let segment = structure
                .segment_mut(segmentid)
                .expect("currently selected segment not found in queststructure");

            match blocktemplate {
                BlockTemplate::SegmentIn => {
                    let mut block = segments::SegmentIn::new(id.name());
                    validation = block.validate("error");

                    block.editordata_or_default().pos = pos;
                    segment.add_input(block)?;
                }
                BlockTemplate::SegmentOut => {
                    let mut block = segments::SegmentOut::new(id.name());
                    validation = block.validate("error");

                    block.editordata_or_default().pos = pos;
                    segment.add_output(block)?;
                }
                _ => unreachable!(),
            }
        }
        // all remaining can be used in root and subsegments (and are bundled as SegmentBlock)
        _ => {
            let mut block = create_from_template(id.name(), blocktemplate, scripttemplates);
            validation = block.validate("error");

            block.editordata_or_default().pos = pos;

            if segmentid.is_quest_root() {
                structure.root_mut()?.add_block(block)?;
            } else {
                let segment = structure
                    .segment_mut(segmentid)
                    .expect("currently selected segment not found in queststructure");
                segment.add_block(block)?;
            }
        }
    }
    Ok(validation)
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use model::questgraph::conditions;
use model::questgraph::journals;
use model::questgraph::misc;
use model::questgraph::scripting;
use model::questgraph::segments;
use model::questgraph::{QuestBlock, QuestStructure, SegmentBlock, SegmentId};
use model::ValidatableElement;

use model::primitives::{HiResTime, Time};

use model::shared::{CompareFunction, FactsDbCondition, ScriptCall};

use super::{
    AreaConditionType, BlockId, BlockTemplate, ConditionType, InteractionConditionType,
    LoadScreenConditionType, TimeConditionType,
};
use super::{BlockValidationState, EditorBlockHint, ScriptTemplates};
// ----------------------------------------------------------------------------
fn create_from_template(
    name: &str,
    template: BlockTemplate,
    scripttemplates: &ScriptTemplates,
) -> SegmentBlock {
    use self::conditions::{Condition, InteractionType, LogicCondition};
    use self::AreaConditionType::*;
    use self::BlockTemplate::*;
    use self::InteractionConditionType::*;
    use self::LoadScreenConditionType::*;
    use self::TimeConditionType::*;

    match template {
        SegmentIn | SegmentOut => unreachable!(),
        SubSegment => segments::SubSegment::new(name).into(),

        ShowLayers | HideLayers | ShowAndHideLayers => {
            let hint = match template {
                ShowLayers => EditorBlockHint::ShowLayers,
                HideLayers => EditorBlockHint::HideLayers,
                _ => EditorBlockHint::ShowAndHideLayers,
            };
            let mut block = misc::Layers::new(name);
            block.editordata_or_default().hint = Some(hint.to_string());
            block.into()
        }

        Scene => misc::Scene::new(name).into(),
        Interaction => misc::Interaction::new(name).into(),

        WaitUntil(condition) => {
            let mut block = conditions::WaitUntil::new(name);

            match condition {
                ConditionType::Fact => {
                    block.set_condition(Condition::FactsDb(FactsDbCondition::new(
                        String::from(""),
                        CompareFunction::GTE,
                        1,
                    )));
                }
                ConditionType::MultiOutFact => {
                    block.editordata_or_default().hint =
                        Some(EditorBlockHint::NamedConditions.to_string());
                    block.set_condition(Condition::FactsDb(FactsDbCondition::new(
                        String::from(""),
                        CompareFunction::GTE,
                        1,
                    )));
                }
                ConditionType::Area(Entered) => {
                    block.set_condition(Condition::EnteredLeftArea(
                        conditions::EnteredLeftAreaCondition::new(None, true, None),
                    ));
                }
                ConditionType::Area(Left) => {
                    block.set_condition(Condition::EnteredLeftArea(
                        conditions::EnteredLeftAreaCondition::new(None, false, None),
                    ));
                }
                ConditionType::Area(Inside) => {
                    block.set_condition(Condition::InsideOutsideArea(
                        conditions::InsideOutsideAreaCondition::new(None, true, None),
                    ));
                }
                ConditionType::Area(Outside) => {
                    block.set_condition(Condition::InsideOutsideArea(
                        conditions::InsideOutsideAreaCondition::new(None, false, None),
                    ));
                }
                ConditionType::Time(Before) => {
                    block.set_condition(Condition::Time(conditions::TimeCondition::Before(Time(
                        12, 0,
                    ))));
                }
                ConditionType::Time(After) => {
                    block.set_condition(Condition::Time(conditions::TimeCondition::After(Time(
                        12, 0,
                    ))));
                }
                ConditionType::Time(Range) => {
                    block.set_condition(Condition::Time(conditions::TimeCondition::TimePeriod(
                        Time(12, 0),
                        Time(12, 30),
                    )));
                }
                ConditionType::Time(Elapsed) => {
                    block.set_condition(Condition::Time(conditions::TimeCondition::Elapsed(
                        HiResTime(0, 0, 15),
                    )));
                }
                ConditionType::Interaction(Custom) => {
                    block.set_condition(Condition::Interaction(
                        conditions::InteractionCondition::new(
                            InteractionType::Custom(String::from("")),
                            None,
                        ),
                    ));
                }
                ConditionType::Interaction(Examined) => {
                    block.set_condition(Condition::Interaction(
                        conditions::InteractionCondition::new(InteractionType::Examine, None),
                    ));
                }
                ConditionType::Interaction(Talked) => {
                    block.set_condition(Condition::Interaction(
                        conditions::InteractionCondition::new(InteractionType::Talk, None),
                    ));
                }
                ConditionType::Interaction(Looted) => {
                    block.set_condition(Condition::Interaction(
                        conditions::InteractionCondition::new(InteractionType::Loot, None),
                    ));
                }
                ConditionType::Interaction(Used) => {
                    block.set_condition(Condition::Interaction(
                        conditions::InteractionCondition::new(InteractionType::Use, None),
                    ));
                }
                ConditionType::LoadScreen(Shown) => {
                    block.set_condition(Condition::LoadingScreen(
                        conditions::LoadingScreenCondition::new(true),
                    ));
                }
                ConditionType::LoadScreen(Hidden) => {
                    block.set_condition(Condition::LoadingScreen(
                        conditions::LoadingScreenCondition::new(false),
                    ));
                }
                ConditionType::Logic(op) => {
                    block.set_condition(Condition::Logic(LogicCondition::new(op)));
                }
            }

            block.into()
        }
        Script(id) => {
            let mut block = scripting::Script::new(name);

            if let Some(template) = scripttemplates
                .values()
                .filter_map(|cat| cat.iter().find(|t| t.id == id))
                .find(|t| t.id == id)
            {
                let mut call = ScriptCall::new(&template.function);
                for param in &template.params {
                    call.add_param(param.clone());
                }

                block.set_function_call(call);
            }

            block.into()
        }

        Spawn | Despawn | SpawnAndDespawn => {
            let (flag, hint) = match template {
                Spawn => (Some(true), EditorBlockHint::SpawnCommunities),
                Despawn => (Some(false), EditorBlockHint::DespawnCommunities),
                _ => (None, EditorBlockHint::Spawnsets),
            };
            let mut block = misc::Spawnsets::new(name, flag);
            block.editordata_or_default().hint = Some(hint.to_string());
            block.into()
        }

        JournalEntry => journals::JournalEntry::new(name).into(),
        JournalObjective => journals::JournalObjective::new(name).into(),
        JournalMappin => journals::JournalMappin::new(name).into(),
        JournalPhaseObjectives => journals::JournalPhaseObjectives::new(name).into(),
        JournalQuestOutcome => journals::JournalQuestOutcome::new(name).into(),

        PauseTime => misc::TimeManagement::new(name, misc::TimeOperation::Pause).into(),
        UnpauseTime => misc::TimeManagement::new(name, misc::TimeOperation::Unpause).into(),
        ShiftTime => misc::TimeManagement::new(name, misc::TimeOperation::Shift(Time(1, 0))).into(),
        SetTime => misc::TimeManagement::new(name, misc::TimeOperation::Set(Time(12, 0))).into(),
        AddFact => misc::AddFact::new(name).into(),

        Teleport => misc::Teleport::new(name).into(),
        ChangeWorld => misc::ChangeWorld::new(name).into(),
        Reward => misc::Reward::new(name).into(),
        Randomize => misc::Randomize::new(name).into(),
    }
}
// ----------------------------------------------------------------------------
