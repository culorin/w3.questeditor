//
// blocks::questblocks::misc
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

use imgui_controls::input;
use imgui_controls::input::validator;

use gui::questgraph::blocks::properties::view;

use model::questgraph::misc;
use model::questgraph::QuestBlock;

use model::primitives::references::{
    CheckedTag, LayerTagReference, RewardReference as ModelRewardReference, SceneReference,
};
use model::primitives::Time;

use super::properties;
use super::properties::{Property, RewardReference, TagReference};

use super::ListProvider;
use super::{EditableBlock, EditorBlockHint};
// ----------------------------------------------------------------------------
impl EditableBlock for misc::Layers {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        use self::EditorBlockHint::*;

        let (shown_layers, hidden_layers) = match self
            .editordata()
            .map(|data| data.hint.as_ref())
            .unwrap_or(None)
        {
            Some(hint) if ShowLayers == *hint => (Some(self.show()), None),
            Some(hint) if HideLayers == *hint => (None, Some(self.hide())),
            _ => (Some(self.show()), Some(self.hide())),
        };

        vec![
            Property::new_control(
                "##layers",
                Box::new(properties::ChangeLayerControl::new(
                    &listprovider.layers(),
                    self.world(),
                    shown_layers,
                    hidden_layers,
                )),
            ),
            new_property_field_bool!(
                "##sync",
                "sync",
                *self.sync().unwrap_or(&true),
                view::LABEL_WIDTH
            ),
            new_property_field_bool!(
                "##purge",
                "purge",
                *self.purge().unwrap_or(&false),
                view::LABEL_WIDTH
            ),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Field(id, field) if id.as_str() == "##sync" => {
                    self.set_sync(field.value().as_bool()?)
                }
                Field(id, field) if id.as_str() == "##purge" => {
                    self.set_purge(field.value().as_bool()?)
                }
                Control(id, ctl) if id.as_str() == "##layers" => {
                    let layers = ctl_to_property!(ctl, properties::ChangeLayerControl, "##layers");

                    self.set_world(layers.world().map(|id| id.into()).unwrap_or(""));
                    self.set_layers_to_show(
                        layers
                            .layers_to_show()
                            .iter()
                            .map(|id| String::from(*id))
                            .collect(),
                    );
                    self.set_layers_to_hide(
                        layers
                            .layers_to_hide()
                            .iter()
                            .map(|id| String::from(*id))
                            .collect(),
                    );
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for misc::Spawnsets {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        use self::EditorBlockHint::*;

        let (spawned, despawned) = match self
            .editordata()
            .map(|data| data.hint.as_ref())
            .unwrap_or(None)
        {
            Some(hint) if SpawnCommunities == *hint => (true, false),
            Some(hint) if DespawnCommunities == *hint => (false, true),
            _ => (true, true),
        };
        let mut controls = Vec::new();
        if spawned {
            controls.push(Property::new_control(
                "##spawn",
                Box::new(properties::CommunitySet::new(
                    "spawn communities",
                    properties::CommunityOptions::ByPhase(
                        listprovider.communities(),
                        listprovider.community_phases(),
                    ),
                    self.phase(),
                    self.spawn().iter().map(misc::Spawnset::id),
                )),
            ));
        }
        if despawned {
            controls.push(Property::new_control(
                "##despawn",
                Box::new(properties::CommunitySet::new(
                    "despawn communities",
                    properties::CommunityOptions::All(listprovider.communities()),
                    None,
                    self.despawn().iter().map(misc::Despawnset::id),
                )),
            ));
        }
        controls
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Control(id, ctl) if id.as_str() == "##spawn" => {
                    let community_set = ctl_to_property!(ctl, properties::CommunitySet, "##spawn");
                    // always set a phase if it has spawnset list
                    self.set_phase(
                        community_set
                            .phase()
                            .map(|phase| phase.into())
                            .unwrap_or_else(|| String::from("")),
                    );
                    self.set_spawnlist(community_set.spawnsets());
                }
                Control(id, ctl) if id.as_str() == "##despawn" => {
                    let community_set =
                        ctl_to_property!(ctl, properties::CommunitySet, "##despawn");

                    self.set_despawnlist(community_set.spawnsets());
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for misc::Interaction {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        let empty_placement = Some(LayerTagReference::WorldAndTag(
            "".into(),
            CheckedTag::default(),
        ));
        vec![
            Property::new_control(
                "##actor",
                Box::new(properties::TagList::new(
                    view::LABEL_WIDTH,
                    "actor(s)",
                    self.actor(),
                    true,
                )),
            ),
            Property::new_control(
                "##scene",
                Box::new(properties::SceneReference::new(
                    view::LABEL_WIDTH,
                    self.scene(),
                )),
            ),
            Property::new_control(
                "##placement",
                Box::new(properties::LabeledReference::<TagReference>::new(
                    view::LABEL_WIDTH,
                    "placement",
                    listprovider.scenepoints(),
                    false,
                    self.placement().or(empty_placement.as_ref()),
                )),
            ),
            new_property_field_bool!(
                "##interruptible",
                "interruptible",
                *self.interruptable(),
                view::LABEL_WIDTH_EXTENDED
            ),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Field(id, field) if id.as_str() == "##interruptible" => {
                    self.set_interruptable(field.value().as_bool()?)
                }
                Control(id, ctl) if id.as_str() == "##scene" => {
                    let scene = ctl_to_property!(ctl, properties::SceneReference, "##scene");

                    self.set_scene(SceneReference::try_from(scene)?);
                }
                Control(id, ctl) if id.as_str() == "##actor" => {
                    let actors = ctl_to_property!(ctl, properties::TagList, "##actor");

                    self.set_actors(&actors.tags());
                }
                Control(id, ctl) if id.as_str() == "##placement" => {
                    let placement = ctl_to_property!(
                        ctl,
                        properties::LabeledReference<TagReference>,
                        "##placement"
                    );

                    self.set_opt_placement(LayerTagReference::try_from(placement.reference()).ok());
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for misc::Scene {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        let empty_placement = Some(LayerTagReference::WorldAndTag(
            "".into(),
            CheckedTag::default(),
        ));
        vec![
            Property::new_control(
                "##scene",
                Box::new(properties::SceneReference::new(
                    view::LABEL_WIDTH,
                    self.scene(),
                )),
            ),
            Property::new_control(
                "##placement",
                Box::new(properties::LabeledReference::<TagReference>::new(
                    view::LABEL_WIDTH,
                    "placement",
                    listprovider.scenepoints(),
                    true,
                    self.placement().or(empty_placement.as_ref()),
                )),
            ),
            new_property_field_bool!(
                "##interruptible",
                "interruptible",
                *self.interruptable(),
                view::LABEL_WIDTH_EXTENDED
            ),
            new_property_field_bool!(
                "##fadein",
                "fade in",
                *self.fadein(),
                view::LABEL_WIDTH_EXTENDED
            ),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Field(id, field) if id.as_str() == "##interruptible" => {
                    self.set_interruptable(field.value().as_bool()?)
                }
                Field(id, field) if id.as_str() == "##fadein" => {
                    self.set_fadein(field.value().as_bool()?)
                }
                Control(id, ctl) if id.as_str() == "##scene" => {
                    let scene = ctl_to_property!(ctl, properties::SceneReference, "##scene");

                    self.set_scene(SceneReference::try_from(scene)?)
                }
                Control(id, ctl) if id.as_str() == "##placement" => {
                    let placement = ctl_to_property!(
                        ctl,
                        properties::LabeledReference<TagReference>,
                        "##placement"
                    );

                    self.set_opt_placement(LayerTagReference::try_from(placement.reference()).ok());
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for misc::TimeManagement {
    // ------------------------------------------------------------------------
    fn properties(&self, _listprovider: &ListProvider) -> Vec<Property> {
        use self::misc::TimeOperation::*;

        match self.operation() {
            Pause | Unpause => vec![],

            Shift(time) => vec![new_property_field!(
                "##time",
                "shift by",
                format!("{}", time),
                input::TextField::new,
                vec![validator::is_nonempty(), validator::is_hhmm()],
                view::FIELD_WIDTH_TIME_HHMM
            )],
            Set(time) => vec![new_property_field!(
                "##time",
                "set to",
                format!("{}", time),
                input::TextField::new,
                vec![validator::is_nonempty(), validator::is_hhmm()],
                view::FIELD_WIDTH_TIME_HHMM
            )],
        }
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::misc::TimeOperation::*;

        for property in properties {
            match property {
                Property::Field(id, field) if id.as_str() == "##time" => match self.operation_mut()
                {
                    Shift(ref mut time) | Set(ref mut time) => {
                        *time = Time::try_from(field.value().as_str()?)?
                    }

                    Pause | Unpause => {
                        return Err(String::from(
                            "property update: pause/unpause blocks don't have properties",
                        ))
                    }
                },
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for misc::AddFact {
    // ------------------------------------------------------------------------
    fn properties(&self, _listprovider: &ListProvider) -> Vec<Property> {
        vec![
            new_property_field!(
                "##factid",
                "fact",
                self.fact().to_string(),
                input::TextField::new,
                vec![
                    validator::is_nonempty(),
                    validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
                    validator::min_length(2),
                    validator::max_length(250),
                ]
            ),
            new_property_field!(
                "##factvalue",
                "value",
                self.value().cloned().unwrap_or(-1),
                input::IntField::new,
                vec![]
            ),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        let mut factid = "";
        let mut value = -1;

        for property in properties {
            match property {
                Field(id, field) if id.as_str() == "##factid" => {
                    factid = field.value().as_str()?;
                }
                Field(id, field) if id.as_str() == "##factvalue" => {
                    value = field.value().as_i32()?;
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        self.set_fact(factid, value);
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for misc::Reward {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        vec![Property::new_control(
            "##reward",
            Box::new(properties::LabeledReference::<RewardReference>::new(
                view::LABEL_WIDTH,
                "reward",
                listprovider.rewards(),
                true,
                self.reward(),
            )),
        )]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Control(id, ctl) if id.as_str() == "##reward" => {
                    let reward = ctl_to_property!(
                        ctl,
                        properties::LabeledReference<RewardReference>,
                        "##reward"
                    );

                    self.set_opt_reward(ModelRewardReference::try_from(reward.reference()).ok());
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for misc::Teleport {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        vec![
            new_property_field!(
                "##actor",
                "actor",
                self.actor().to_string(),
                input::TextField::new,
                vec![validator::is_nonempty(), validator::min_length(2)]
            ),
            Property::new_control(
                "##destination",
                Box::new(properties::LabeledReference::<TagReference>::new(
                    view::LABEL_WIDTH,
                    "destination",
                    listprovider.waypoints(),
                    true,
                    self.destination(),
                )),
            ),
        ]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Field(id, field) if id.as_str() == "##actor" => {
                    self.set_actor(field.value().as_str()?)
                }
                Control(id, ctl) if id.as_str() == "##destination" => {
                    let destination = ctl_to_property!(
                        ctl,
                        properties::LabeledReference<TagReference>,
                        "##destination"
                    );

                    self.set_opt_destination(
                        LayerTagReference::try_from(destination.reference()).ok(),
                    );
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for misc::ChangeWorld {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        vec![Property::new_control(
            "##destination",
            Box::new(properties::LabeledReference::<TagReference>::new(
                view::LABEL_WIDTH,
                "destination",
                listprovider.waypoints(),
                true,
                self.destination(),
            )),
        )]
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        for property in properties {
            match property {
                Property::Control(id, ctl) if id.as_str() == "##destination" => {
                    let destination = ctl_to_property!(
                        ctl,
                        properties::LabeledReference<TagReference>,
                        "##destination"
                    );

                    self.set_opt_destination(
                        LayerTagReference::try_from(destination.reference()).ok(),
                    );
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EditableBlock for misc::Randomize {
    // ------------------------------------------------------------------------
    fn properties(&self, _listprovider: &ListProvider) -> Vec<Property> {
        Vec::default()
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, _properties: &[Property]) -> Result<(), String> {
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
