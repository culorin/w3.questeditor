//
// blocks::questblocks functions on questblock objects
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui::questgraph) trait EditableBlock: QuestBlock {
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property>;
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String>;
    fn as_questblock(&self) -> &dyn QuestBlock;
}
// ----------------------------------------------------------------------------
pub(super) use self::templates::create_new_from_template;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use model::questgraph::{QuestBlock, SegmentBlock};

use super::{
    AreaConditionType, BlockId, BlockTemplate, ConditionType, InteractionConditionType,
    LoadScreenConditionType, TimeConditionType,
};
use super::{BlockValidationState, ScriptTemplates};

use super::properties::Property;

use super::properties;

use super::{EditorBlockHint, ListProvider};
// ----------------------------------------------------------------------------
mod conditions;
mod journals;
mod misc;
mod scripting;
mod segments;
mod templates;
// ----------------------------------------------------------------------------
impl EditableBlock for SegmentBlock {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        use self::SegmentBlock::*;

        match *self {
            SubSegment(ref block) => block.properties(listprovider),
            WaitUntil(ref block) => block.properties(listprovider),
            Script(ref block) => block.properties(listprovider),
            JournalEntry(ref block) => block.properties(listprovider),
            JournalObjective(ref block) => block.properties(listprovider),
            JournalMappin(ref block) => block.properties(listprovider),
            JournalPhaseObjectives(ref block) => block.properties(listprovider),
            JournalQuestOutcome(ref block) => block.properties(listprovider),
            Layers(ref block) => block.properties(listprovider),
            Spawnsets(ref block) => block.properties(listprovider),
            Interaction(ref block) => block.properties(listprovider),
            Scene(ref block) => block.properties(listprovider),
            TimeManagement(ref block) => block.properties(listprovider),
            AddFact(ref block) => block.properties(listprovider),
            Reward(ref block) => block.properties(listprovider),
            Teleport(ref block) => block.properties(listprovider),
            ChangeWorld(ref block) => block.properties(listprovider),
            Randomize(ref block) => block.properties(listprovider),
        }
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::SegmentBlock::*;

        match *self {
            SubSegment(ref mut block) => block.set_properties(properties),
            WaitUntil(ref mut block) => block.set_properties(properties),
            Script(ref mut block) => block.set_properties(properties),
            JournalEntry(ref mut block) => block.set_properties(properties),
            JournalObjective(ref mut block) => block.set_properties(properties),
            JournalMappin(ref mut block) => block.set_properties(properties),
            JournalPhaseObjectives(ref mut block) => block.set_properties(properties),
            JournalQuestOutcome(ref mut block) => block.set_properties(properties),
            Layers(ref mut block) => block.set_properties(properties),
            Spawnsets(ref mut block) => block.set_properties(properties),
            Interaction(ref mut block) => block.set_properties(properties),
            Scene(ref mut block) => block.set_properties(properties),
            TimeManagement(ref mut block) => block.set_properties(properties),
            AddFact(ref mut block) => block.set_properties(properties),
            Reward(ref mut block) => block.set_properties(properties),
            Teleport(ref mut block) => block.set_properties(properties),
            ChangeWorld(ref mut block) => block.set_properties(properties),
            Randomize(ref mut block) => block.set_properties(properties),
        }
    }
    // ------------------------------------------------------------------------
    fn as_questblock(&self) -> &dyn QuestBlock {
        use self::SegmentBlock::*;

        match *self {
            SubSegment(ref block) => block,
            WaitUntil(ref block) => block,
            Script(ref block) => block,
            JournalEntry(ref block) => block,
            JournalObjective(ref block) => block,
            JournalMappin(ref block) => block,
            JournalPhaseObjectives(ref block) => block,
            JournalQuestOutcome(ref block) => block,
            Layers(ref block) => block,
            Spawnsets(ref block) => block,
            Interaction(ref block) => block,
            Scene(ref block) => block,
            TimeManagement(ref block) => block,
            AddFact(ref block) => block,
            Reward(ref block) => block,
            Teleport(ref block) => block,
            ChangeWorld(ref block) => block,
            Randomize(ref block) => block,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// helper
// ----------------------------------------------------------------------------
fn unknown_property(uid: &::model::questgraph::BlockUid, id: &str) -> Result<(), String> {
    Err(format!(
        "property update: unsuppored property {} on block {}",
        id,
        uid.blockid()
    ))
}
// ----------------------------------------------------------------------------
