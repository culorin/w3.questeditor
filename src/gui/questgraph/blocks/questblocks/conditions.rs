//
// blocks::questblocks::conditions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::convert::{TryFrom, TryInto};

use model::questgraph::conditions;
use model::questgraph::QuestBlock;

use super::properties;
use super::properties::Property;

use super::ListProvider;
use super::{EditableBlock, EditorBlockHint};
// ----------------------------------------------------------------------------
impl EditableBlock for conditions::WaitUntil {
    // ------------------------------------------------------------------------
    fn properties(&self, listprovider: &ListProvider) -> Vec<Property> {
        use self::conditions::Condition::*;
        // multiple use cases
        //  1. single condition
        //    a) logic condition with embedded conditions
        //      -> LogicConditionSet
        //    b) simple condition (not logic)
        //      -> specialized condition widget
        //  2. multiple simple/fact conditions
        //      -> SocketConditionSet
        //  _ => unsupported
        let conditions = self.conditions();

        let has_named_conditions = match self.editordata() {
            Some(data) => data
                .hint
                .as_ref()
                .map(|hint| EditorBlockHint::NamedConditions == *hint)
                .unwrap_or(false),
            None => false,
        };

        if conditions.len() == 1 && !has_named_conditions {
            let control = match conditions.first() {
                Some(Logic(ref cond)) => Property::new_control(
                    "##logiccond",
                    Box::new(properties::LogicConditionSet::new(
                        listprovider.areas(),
                        listprovider.interactions(),
                        cond,
                    )),
                ),
                Some(FactsDb(ref cond)) => Property::new_control(
                    "##factcond",
                    Box::new(properties::FactCondition::new(Some(cond))),
                ),
                Some(Time(ref cond)) => Property::new_control(
                    "##timecond",
                    Box::new(properties::TimeCondition::from(cond)),
                ),
                Some(Interaction(ref cond)) => Property::new_control(
                    "##interactcond",
                    Box::new(properties::InteractionCondition::from((
                        listprovider.interactions(),
                        cond,
                    ))),
                ),
                Some(EnteredLeftArea(ref cond)) => Property::new_control(
                    "##areacond",
                    Box::new(properties::AreaCondition::from((
                        listprovider.areas(),
                        cond,
                    ))),
                ),
                Some(InsideOutsideArea(ref cond)) => Property::new_control(
                    "##areacond",
                    Box::new(properties::AreaCondition::from((
                        listprovider.areas(),
                        cond,
                    ))),
                ),
                Some(LoadingScreen(ref cond)) => Property::new_control(
                    "##loadscreencond",
                    Box::new(properties::LoadScreenCondition::from(cond)),
                ),
                None => unreachable!(),
            };
            vec![control]
        } else {
            vec![Property::new_control(
                "##namedcondset",
                Box::new(properties::NamedConditionSet::new(self.conditions())),
            )]
        }
    }
    // ------------------------------------------------------------------------
    fn set_properties(&mut self, properties: &[Property]) -> Result<(), String> {
        use self::Property::*;

        for property in properties {
            match property {
                Control(id, ctl) if id.as_str() == "##factcond" => {
                    let cond = ctl_to_property!(ctl, properties::FactCondition, "##factcond");

                    self.set_condition(cond.into());
                }
                Control(id, ctl) if id.as_str() == "##timecond" => {
                    let cond = ctl_to_property!(ctl, properties::TimeCondition, "##timecond");

                    self.set_condition(cond.try_into()?);
                }
                Control(id, ctl) if id.as_str() == "##interactcond" => {
                    let cond =
                        ctl_to_property!(ctl, properties::InteractionCondition, "##interactcond");

                    self.set_condition(cond.into());
                }
                Control(id, ctl) if id.as_str() == "##areacond" => {
                    let cond = ctl_to_property!(ctl, properties::AreaCondition, "##areacond");

                    self.set_condition(cond.into());
                }
                Control(id, ctl) if id.as_str() == "##loadscreencond" => {
                    let cond =
                        ctl_to_property!(ctl, properties::LoadScreenCondition, "##loadscreencond");

                    self.set_condition(cond.into());
                }
                Control(id, ctl) if id.as_str() == "##logiccond" => {
                    let cond_set =
                        ctl_to_property!(ctl, properties::LogicConditionSet, "##logiccond");

                    let mut logic_cond = conditions::LogicCondition::new(cond_set.logic_op());

                    for cond in cond_set.conditions() {
                        logic_cond.add_condition(conditions::Condition::try_from(cond)?);
                    }

                    self.set_condition(conditions::Condition::Logic(logic_cond));
                }
                Control(id, ctl) if id.as_str() == "##namedcondset" => {
                    let cond_set =
                        ctl_to_property!(ctl, properties::NamedConditionSet, "##namedcondset");

                    self.clear_conditions();

                    for cond in cond_set.conditions() {
                        self.add_condition(Some(cond.socket()), conditions::Condition::from(cond));
                    }
                }
                unknown => super::unknown_property(self.uid(), unknown.id())?,
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn as_questblock(&self) -> &dyn QuestBlock {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
