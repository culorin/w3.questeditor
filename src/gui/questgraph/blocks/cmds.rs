//
// blocks::cmds - more complex actions with (possible) side effects
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
/// creates new questblock and graphblock.
pub(in gui::questgraph) fn add_new_block_from_template(
    blocktemplate: BlockTemplate,
    pos: (f32, f32),
    segmentid: &SegmentId,
    blocks: &mut Blocks,
    def: &mut QuestStructure,
    scripttemplates: &ScriptTemplates,
) -> Result<BlockId, String> {
    let new_unique_block_id = create_new_block_id(&blocktemplate, blocks);

    let validation_state = questblocks::create_new_from_template(
        blocktemplate,
        segmentid,
        &new_unique_block_id,
        pos,
        def,
        scripttemplates,
    )?;

    let mut graphblock = graphblocks::create_new(
        &new_unique_block_id,
        Some(pos),
        validation_state,
        // new blocks have only block specific default sockets
        None,
    )?;
    graphblock.add_missing_sockets();

    blocks.insert(new_unique_block_id.clone(), graphblock);

    Ok(new_unique_block_id)
}
// ----------------------------------------------------------------------------
/// creates a new graphblock from questblock. includes sockets from editordata
/// but NO default sockets (even if editordata doesn't have any socket info).
/// current validation result will be used as tooltip.
pub(in gui::questgraph) fn create_graphblock_from_questblock(
    questblock: &dyn QuestBlock,
) -> Result<GraphBlock, String> {
    graphblock_from_questblock(questblock)
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn inject_waituntil_hint(block: &mut WaitUntil) {
    // inject hint for named conditions/multi out-socket version if it's missing
    let is_named = block
        .conditions()
        .iter()
        .enumerate()
        .any(|(i, cond)| i > 1 || cond.name() != "Out");

    if is_named {
        block.editordata_or_default().hint = Some(EditorBlockHint::NamedConditions.to_string());
    }
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn inject_layers_hint(block: &mut Layers) {
    let (has_show, has_hide) = (!block.show().is_empty(), !block.hide().is_empty());

    // inject hint for type of layers block (if it's missing)
    let data = block.editordata_or_default();

    if data.hint.is_none() {
        let hint = match (has_show, has_hide) {
            (true, false) => EditorBlockHint::ShowLayers,
            (false, true) => EditorBlockHint::HideLayers,
            _ => EditorBlockHint::ShowAndHideLayers,
        };
        data.hint = Some(hint.to_string());
    }
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn inject_spawnsets_hint(block: &mut Spawnsets) {
    let (has_spawns, has_despawns) = (!block.spawn().is_empty(), !block.despawn().is_empty());

    // inject hint for type of spawnsets block (if it's missing)
    let data = block.editordata_or_default();

    if data.hint.is_none() {
        let hint = match (has_spawns, has_despawns) {
            (true, false) => EditorBlockHint::SpawnCommunities,
            (false, true) => EditorBlockHint::DespawnCommunities,
            _ => EditorBlockHint::Spawnsets,
        };
        data.hint = Some(hint.to_string());
    }
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn inject_subsegment_editordata(
    block: &mut SubSegment,
    segment_io: &HashMap<SegmentId, (Vec<InSocketId>, Vec<OutSocketId>)>,
) {
    match block.segmentlink().and_then(|id| segment_io.get(id)) {
        Some((in_sockets, out_sockets)) => {
            let editordata = block.editordata_or_default();

            editordata.in_sockets = in_sockets.clone();
            editordata.out_sockets = out_sockets.clone();
        }
        None => {
            let editordata = block.editordata_or_default();

            editordata.in_sockets.clear();
            editordata.out_sockets.clear();
        }
    }
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn update_subsegment_sockets(
    block: &mut SubSegment,
    in_sockets: &[InSocketId],
    out_sockets: &[OutSocketId],
) {
    let editordata = block.editordata_or_default();

    editordata.in_sockets = in_sockets.to_vec();
    editordata.out_sockets = out_sockets.to_vec();

    // remove all invalid out links
    let invalid_sockets = block
        .links()
        .filter(|(socket, _)| !out_sockets.contains(socket))
        .map(|(socket, _)| socket.clone())
        .collect::<Vec<_>>();

    for socket in &invalid_sockets {
        block.delete_links_for_socket(socket);
    }
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn remove_subsegment_sockets(block: &mut SubSegment) {
    let editordata = block.editordata_or_default();

    editordata.in_sockets = Vec::default();
    editordata.out_sockets = Vec::default();

    block.set_opt_segmentlink(None);
    block.reset_links();
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn rename_out_socket(
    block: &mut SubSegment,
    old: &OutSocketId,
    new: &OutSocketId,
) {
    let editordata = block.editordata_or_default();

    editordata.out_sockets = editordata
        .out_sockets
        .drain(..)
        .map(|socket| if socket == *old { new.clone() } else { socket })
        .collect();

    block.rename_out_socket(old, new);
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn rename_in_socket(
    block: &mut SubSegment,
    old: &InSocketId,
    new: &InSocketId,
) {
    let editordata = block.editordata_or_default();

    editordata.in_sockets = editordata
        .in_sockets
        .drain(..)
        .map(|socket| if socket == *old { new.clone() } else { socket })
        .collect();

    // references to in-sockets must be updated on the quest graph!
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::collections::HashMap;

use model::questgraph::conditions::WaitUntil;
use model::questgraph::misc::{Layers, Spawnsets};
use model::questgraph::primitives::{InSocketId, OutSocketId};
use model::questgraph::segments::SubSegment;
use model::questgraph::{QuestStructure, SegmentId};

use super::graphblocks;
use super::questblocks;

use super::{BlockTemplate, EditorBlockHint, ScriptTemplates};

use super::{BlockId, Blocks, EditableGraphBlock, GraphBlock, QuestBlock};
// ----------------------------------------------------------------------------
fn graphblock_from_questblock(questblock: &dyn QuestBlock) -> Result<GraphBlock, String> {
    let id = questblock.uid().blockid();

    let in_sockets = questblock
        .editordata()
        .map(|d| d.in_sockets.iter().map(|s| s.into()).collect::<Vec<_>>())
        .unwrap_or_else(Vec::new);

    let out_sockets = questblock
        .editordata()
        .map(|d| d.out_sockets.iter().map(|s| s.into()).collect::<Vec<_>>())
        .unwrap_or_else(Vec::new);

    graphblocks::create_new(
        id,
        questblock.editordata().map(|d| d.pos),
        questblock.validate("error"),
        Some((in_sockets, out_sockets)),
    )
    // do NOT add missing sockets here! those must to be added ONLY if NO
    // socket defining questlinks are found in structure!
}
// ----------------------------------------------------------------------------
fn create_new_block_id(blocktype: &BlockTemplate, blocks: &Blocks) -> BlockId {
    use super::AreaConditionType::*;
    use super::BlockTemplate::*;
    use super::ConditionType;
    use super::InteractionConditionType::*;
    use super::LoadScreenConditionType::*;
    use super::LogicOperation::*;
    use super::TimeConditionType::*;

    let mut id = match blocktype {
        SegmentIn => BlockId::SegmentIn("new_segment_in".into()),
        SegmentOut => BlockId::SegmentOut("new_segment_out".into()),
        SubSegment => BlockId::SubSegment("new_subsegment".into()),
        ShowLayers => BlockId::Layers("new_show_layers".into()),
        HideLayers => BlockId::Layers("new_hide_layers".into()),
        ShowAndHideLayers => BlockId::Layers("new_layers".into()),
        Scene => BlockId::Scene("new_scene".into()),
        Interaction => BlockId::Interaction("new_interaction".into()),
        WaitUntil(condition) => {
            let name = match condition {
                ConditionType::Fact | ConditionType::MultiOutFact => "new_wait_until_fact",
                ConditionType::Area(Entered) => "new_wait_until_entered_area",
                ConditionType::Area(Left) => "new_wait_until_left_area",
                ConditionType::Area(Inside) => "new_wait_until_inside_area",
                ConditionType::Area(Outside) => "new_wait_until_outside_area",
                ConditionType::Time(Range) => "new_wait_until_some_time",
                ConditionType::Time(After) => "new_wait_until_after_time",
                ConditionType::Time(Before) => "new_wait_until_before_time",
                ConditionType::Time(Elapsed) => "new_wait_until_realtime_elapsed",
                ConditionType::Interaction(Custom) => "new_wait_until_interaction",
                ConditionType::Interaction(Examined) => "new_wait_until_examined",
                ConditionType::Interaction(Talked) => "new_wait_until_talked",
                ConditionType::Interaction(Used) => "new_wait_until_used",
                ConditionType::Interaction(Looted) => "new_wait_until_looted",
                ConditionType::Logic(AND) => "new_wait_until_all",
                ConditionType::Logic(OR) => "new_wait_until_any",
                ConditionType::Logic(XOR) => "new_wait_until_exactly_one",
                ConditionType::Logic(NAND) => "new_wait_until_not_all",
                ConditionType::Logic(NOR) => "new_wait_until_none",
                ConditionType::Logic(NXOR) => "new_wait_until_nxor",
                ConditionType::LoadScreen(Shown) => "new_wait_until_loading_start",
                ConditionType::LoadScreen(Hidden) => "new_wait_until_loading_end",
            };
            BlockId::WaitUntil(name.into())
        }
        Script(identifier) => BlockId::Script(format!("new_{}", identifier).into()),
        Spawn => BlockId::Spawn("new_community_spawn".into()),
        Despawn => BlockId::Despawn("new_community_despawn".into()),
        SpawnAndDespawn => BlockId::Spawnsets("new_community".into()),
        JournalEntry => BlockId::JournalEntry("new_journal_entry".into()),
        JournalMappin => BlockId::JournalMappin("new_quest_mappin".into()),
        JournalObjective => BlockId::JournalObjective("new_quest_objective".into()),
        JournalPhaseObjectives => BlockId::JournalPhaseObjectives("new_quest_phase".into()),
        JournalQuestOutcome => BlockId::JournalQuestOutcome("new_quest_outcome".into()),
        PauseTime => BlockId::PauseTime("new_pause_time".into()),
        UnpauseTime => BlockId::UnpauseTime("new_unpause_time".into()),
        ShiftTime => BlockId::ShiftTime("new_shift_time".into()),
        SetTime => BlockId::SetTime("new_set_time".into()),
        AddFact => BlockId::AddFact("new_add_fact".into()),
        Teleport => BlockId::Teleport("new_teleport".into()),
        ChangeWorld => BlockId::ChangeWorld("new_change_world".into()),
        Reward => BlockId::Reward("new_reward".into()),
        Randomize => BlockId::Randomize("new_randomize".into()),
    };

    // find unique unused name in this segment
    let mut counter = 1;
    let name_prefix = id.name().as_str().to_owned();

    while blocks.contains_key(&id) {
        id = id.cloned_with_name(format!("{}_{}", name_prefix, counter));
        counter += 1;
    }

    id
}
// ----------------------------------------------------------------------------
