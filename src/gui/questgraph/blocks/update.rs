//
// blocks::update
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[allow(clippy::too_many_arguments)]
#[inline]
pub(super) fn handle_editor_action(
    action: BlockEditorAction,
    state: &mut Option<PropertyEditorState>,
    quest_structure: &mut QuestStructure,
    listprovider: &ListProvider,
    selection: &mut SelectionState,
    planestate: &mut PlaneState,
    blocks: &mut Blocks,
    edges: &mut Edges,
    dirtyflag: &mut bool,
) -> Result<(), String> {
    handle_property_editor_action(
        action,
        state,
        quest_structure,
        listprovider,
        selection,
        planestate,
        blocks,
        edges,
        dirtyflag,
    )?;
    Ok(())
}
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn handle_block_action(
    action: BlockAction,
    quest_structure: &mut QuestStructure,
    selection: &mut SelectionState,
    planestate: &mut PlaneState,
    blocks: &mut Blocks,
    edges: &mut Edges,
    dirtyflag: &mut bool,
) -> Result<Option<::gui::ActionSequence>, String> {
    handle_block_specific_action(
        action,
        quest_structure,
        selection,
        planestate,
        blocks,
        edges,
        dirtyflag,
    )
}
// ----------------------------------------------------------------------------
use imgui_controls::graph;
use imgui_controls::input;
use model::questgraph::segments::QuestSegment;
use model::questgraph::{QuestStructure, SegmentBlock};

// state
use super::properties;
use super::{PlaneState, PropertyEditorState, SelectionState};

// actions
use super::{BlockAction, BlockEditorAction};
use gui::Action::PopupRequest;

// misc
use super::ListProvider;
use super::{BlockId, InSocketId, OutSocketId, SegmentId};
use super::{Blocks, Edges, EditableGraphBlock, GraphBlock};
use super::{FieldValue, SocketNameRequest};

use gui::questgraph::cmds as questgraph_cmds;
// ----------------------------------------------------------------------------
// action groups
// ----------------------------------------------------------------------------
#[allow(clippy::too_many_arguments)]
fn handle_property_editor_action(
    action: BlockEditorAction,
    state: &mut Option<PropertyEditorState>,
    quest_structure: &mut QuestStructure,
    listprovider: &ListProvider,
    selection: &mut SelectionState,
    plane: &mut PlaneState,
    blocks: &mut Blocks,
    edges: &mut Edges,
    dirtyflag: &mut bool,
) -> Result<(), String> {
    use self::BlockEditorAction::*;

    if let Some(state) = state.as_mut() {
        match action {
            Property(id, action) => {
                properties::handle_action(&id, action, state)?;
            }
            UpdateField(id, new_value) => match id.as_str() {
                "blockname" => update_blockname_field(state, &new_value, blocks)?,
                unknown => warn!(
                    "update action for unknown editor field [{}]: {}",
                    unknown, new_value
                ),
            },
            SavePosition(ref blockid) => {
                if save_block_position(
                    quest_structure,
                    &selection.segment.1,
                    blockid,
                    blocks.get(blockid),
                )? {
                    *dirtyflag = true;
                }
            }
            SaveProperties => {
                *dirtyflag = true;
                save_block_properties(quest_structure, selection, state, plane, blocks, edges)?;
                state.reset_changed();
            }
            DiscardChanges => {
                // reselect block to get original values
                *state = setup_property_editor(
                    &selection.segment.1,
                    state.blockid.clone(),
                    quest_structure,
                    listprovider,
                )?;
            }
            InitFromBlock(id) => {
                // make sure editordata contains current set of sockets
                sync_block_sockets(&selection.segment.1, quest_structure, &id, blocks);
                *state =
                    setup_property_editor(&selection.segment.1, id, quest_structure, listprovider)?;
            }
        }
    } else {
        match action {
            InitFromBlock(id) => {
                // make sure editordata contains current set of sockets
                sync_block_sockets(&selection.segment.1, quest_structure, &id, blocks);
                *state = Some(setup_property_editor(
                    &selection.segment.1,
                    id,
                    quest_structure,
                    listprovider,
                )?);
            }
            _ => unreachable!("received editor action without initialized editor state!"),
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn update_blockname_field(
    state: &mut PropertyEditorState,
    new_value: &FieldValue,
    graphblocks: &Blocks,
) -> Result<(), String> {
    if state.blockname_mut().validate().valid() {
        // additional check if name is unique in block
        let new_name = new_value.as_str()?.replace(' ', "_");
        let new_id = state.blockid.cloned_with_name(new_name.clone());

        if new_name != state.blockid.name().as_str() && graphblocks.contains_key(&new_id) {
            state
                .blockname_mut()
                .set_error_msg(String::from("blockname must be unique in current segment"));
        } else {
            state
                .blockname_mut()
                .set_value(input::FieldValue::Str(&new_name))?;
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn setup_property_editor(
    segmentid: &SegmentId,
    blockid: BlockId,
    structure: &QuestStructure,
    listprovider: &ListProvider,
) -> Result<PropertyEditorState, String> {
    let block = questgraph_cmds::find_block(segmentid, &blockid, structure)?;

    Ok(PropertyEditorState::new(
        blockid,
        block.properties(listprovider),
    ))
}
// ----------------------------------------------------------------------------
fn save_block_position(
    quest_structure: &mut QuestStructure,
    segmentid: &SegmentId,
    blockid: &BlockId,
    graphblock: Option<&GraphBlock>,
) -> Result<bool, String> {
    if let Some(graphblock) = graphblock {
        let mut editordata = questgraph_cmds::find_block_mut(segmentid, blockid, quest_structure)?
            .editordata_or_default();
        let changed = editordata.pos != graphblock.pos();
        if changed {
            editordata.pos = graphblock.pos();
        }
        Ok(changed)
    } else {
        Ok(false)
    }
}
// ----------------------------------------------------------------------------
fn save_block_properties(
    quest_structure: &mut QuestStructure,
    selection: &mut SelectionState,
    editor: &mut PropertyEditorState,
    planestate: &mut PlaneState,
    graphblocks: &mut Blocks,
    graphedges: &mut Edges,
) -> Result<(), String> {
    if editor.blockname().valid() {
        let segmentid = &selection.segment.1;
        let blockname = &editor.blockname().value().as_str()?.replace(' ', "_");
        let blockname_changed = blockname != editor.blockid.name().as_str();
        let blockid = &editor.blockid;

        let validation = match blockid {
            // special case for subsegment blocks: sockets must be regenerated and verified!
            BlockId::SubSegment(_) => save_subsegment_properties(
                quest_structure,
                selection,
                blockid,
                editor.properties(),
                planestate,
                graphblocks,
                graphedges,
            ),
            _ => {
                let questblock =
                    questgraph_cmds::find_block_mut(segmentid, blockid, quest_structure)?;

                questblock.set_properties(editor.properties())?;
                questblock.validate("error")
            }
        };

        if blockname_changed {
            let new_id = questgraph_cmds::rename_block(
                selection,
                blockid,
                blockname,
                quest_structure,
                planestate,
                graphblocks,
                graphedges,
            )?;

            // some renaming has implications on blocks in different segments!
            match blockid {
                BlockId::SegmentIn(_) | BlockId::SegmentOut(_) => {
                    questgraph_cmds::sync_subsegment_in_out_changes(
                        quest_structure,
                        &selection.segment.1,
                        blockid,
                        &new_id,
                    )?;
                }
                _ => {}
            }

            editor.blockid.set_name(blockname.into());
        } else if let Some(graphblock) = graphblocks.get_mut(&editor.blockid) {
            // block will be recreated and thus validated if blockname changes
            // but if it doesn't this has to be triggered here (so graphlock will be
            // updated immediately

            graphblock.set_validation_state(validation);
            // since it was edited it is also selected (setting validation overwrites highlights!)
            graphblock.set_highlight(true);
        }
        Ok(())
    } else {
        Err(
            "blockname must be valid and unique in the current segment to save properties. \
             Please correct and retry."
                .into(),
        )
    }
}
// ----------------------------------------------------------------------------
// block specific actions
// ----------------------------------------------------------------------------
fn handle_block_specific_action(
    action: BlockAction,
    quest_structure: &mut QuestStructure,
    selection: &mut SelectionState,
    _plane: &mut PlaneState,
    blocks: &mut Blocks,
    edges: &mut Edges,
    dirtyflag: &mut bool,
) -> Result<Option<::gui::ActionSequence>, String> {
    use self::BlockAction::*;

    match action {
        ScriptResultBranch(id, branch) => {
            *dirtyflag = true;
            switch_scriptresult_branching(&id, branch, blocks, edges);
        }
        OnAddInSocket(id) => {
            return Ok(Some(ima_seq![PopupRequest(Box::new(
                SocketNameRequest::new(id, true)
            ))]));
        }
        OnAddOutSocket(id) => {
            return Ok(Some(ima_seq![PopupRequest(Box::new(
                SocketNameRequest::new(id, false)
            ))]));
        }
        AddInSocket(id, socket) => {
            *dirtyflag = true;
            add_in_socket(&id, &socket, blocks, edges)?;
        }
        DeleteInSocket(id, socket) => {
            *dirtyflag = true;
            delete_in_socket(&id, &socket, blocks, edges)?;
        }
        AddOutSocket(id, socket) => {
            *dirtyflag = true;
            match id {
                BlockId::WaitUntil(_) => {
                    // special case as property editor needs to be updated based
                    // on available set of out-sockets
                    add_out_socket(&id, &socket, blocks, edges)?;
                    add_named_condition(
                        &selection.segment.1,
                        quest_structure,
                        &id,
                        &socket,
                        blocks,
                    )?;
                    return Ok(Some(ima_seq![BlockEditorAction::InitFromBlock(id)]));
                }
                _ => add_out_socket(&id, &socket, blocks, edges)?,
            }
        }
        DeleteOutSocket(id, socket) => {
            *dirtyflag = true;
            match id {
                BlockId::WaitUntil(_) => {
                    // special case as property editor needs to be updated based
                    // on available set of out-sockets
                    delete_out_socket(&id, &socket, blocks, edges)?;
                    delete_named_condition(
                        &selection.segment.1,
                        quest_structure,
                        &id,
                        &socket,
                        blocks,
                    )?;
                    return Ok(Some(ima_seq![BlockEditorAction::InitFromBlock(id)]));
                }
                _ => delete_out_socket(&id, &socket, blocks, edges)?,
            }
        }
    }
    Ok(None)
}
// ----------------------------------------------------------------------------
// script
// ----------------------------------------------------------------------------
#[inline]
fn switch_scriptresult_branching(
    id: &BlockId,
    branch: bool,
    blocks: &mut Blocks,
    edges: &mut Edges,
) {
    use model::questgraph::primitives;

    if let Some(block) = blocks.get_mut(id) {
        // keep it simple: remove all outgoing edges from this block
        *edges = edges
            .drain(..)
            .filter(|e| e.from().0 != *id)
            .collect::<Vec<_>>();

        // since all egdes for this block were removed there is no need to refresh
        // edge positions (since new sockets don't have any edges)

        if branch {
            block.set_out_sockets(vec!["False".into(), "True".into()]);
        } else {
            block.set_out_sockets(vec![(&primitives::OutSocketId::default()).into()]);
        }
    }
}
// ----------------------------------------------------------------------------
// custom sockets
// ----------------------------------------------------------------------------
#[inline]
fn delete_in_socket(
    id: &BlockId,
    socket: &InSocketId,
    blocks: &mut Blocks,
    edges: &mut Edges,
) -> Result<(), String> {
    if let Some(block) = blocks.get_mut(id) {
        trace!("> removing in-socket [{}] from block [{}]", socket, id);
        // remove all edges targeting this block on this socket
        *edges = edges
            .drain(..)
            .filter(|e| !(e.to().0 == *id && e.to().1 == *socket))
            .collect::<Vec<_>>();

        // remove socket
        block.set_in_sockets(
            block
                .in_sockets()
                .iter()
                .filter(|s| *s != socket)
                .cloned()
                .collect::<Vec<_>>(),
        );
        // if last of the sockets was removed add default socket
        block.add_missing_sockets();

        // removing sockets changes position of remaining sockets, thus edge
        // positions must be refreshed
        // no socket names changed -> it's not required to update/adjust/reextract
        // edges and/or blocks in quest structure
        graph::refresh_edge_positions(blocks, edges);

        Ok(())
    } else {
        Err(format!("block {} not found!", id))
    }
}
// ----------------------------------------------------------------------------
#[inline]
fn delete_out_socket(
    id: &BlockId,
    socket: &OutSocketId,
    blocks: &mut Blocks,
    edges: &mut Edges,
) -> Result<(), String> {
    if let Some(block) = blocks.get_mut(id) {
        trace!("> removing out-socket [{}] from block [{}]", socket, id);
        // remove all outgoing edges from this block on this socket
        *edges = edges
            .drain(..)
            .filter(|e| !(e.from().0 == *id && e.from().1 == *socket))
            .collect::<Vec<_>>();

        // remove socket
        block.set_out_sockets(
            block
                .out_sockets()
                .iter()
                .filter(|s| *s != socket)
                .cloned()
                .collect::<Vec<_>>(),
        );
        // if last of the sockets was removed add default socket
        block.add_missing_sockets();

        // removing sockets changes position of remaining sockets, thus edge
        // positions must be refreshed
        // no socket names changed -> it's not required to update/adjust/reextract
        // edges and/or blocks in quest structure
        graph::refresh_edge_positions(blocks, edges);

        Ok(())
    } else {
        Err(format!("block {} not found!", id))
    }
}
// ----------------------------------------------------------------------------
#[inline]
fn add_in_socket(
    id: &BlockId,
    socket: &InSocketId,
    blocks: &mut Blocks,
    edges: &mut Edges,
) -> Result<(), String> {
    if let Some(block) = blocks.get_mut(id) {
        trace!("> adding new in-socket [{}] to block [{}]", socket, id);

        let mut new_sockets = block.in_sockets().to_vec();

        new_sockets.push(socket.into());

        block.set_in_sockets(new_sockets);
        // adding sockets changes position of other sockets, thus edge
        // positions must be refreshed
        // no socket names changed -> it's not required to update/adjust/reextract
        // edges and/or blocks in quest structure
        graph::refresh_edge_positions(blocks, edges);

        Ok(())
    } else {
        Err(format!("block {} not found!", id))
    }
}
// ----------------------------------------------------------------------------
#[inline]
fn add_out_socket(
    id: &BlockId,
    socket: &OutSocketId,
    blocks: &mut Blocks,
    edges: &mut Edges,
) -> Result<(), String> {
    if let Some(block) = blocks.get_mut(id) {
        trace!("> adding new out-socket [{}] to block [{}]", socket, id);

        let mut new_sockets = block.out_sockets().to_vec();

        new_sockets.push(socket.into());

        block.set_out_sockets(new_sockets);

        // removing sockets changes position of remaining sockets, thus edge
        // positions must be refreshed
        // no socket names changed -> it's not required to update/adjust/reextract
        // edges and/or blocks in quest structure
        graph::refresh_edge_positions(blocks, edges);

        Ok(())
    } else {
        Err(format!("block {} not found!", id))
    }
}
// ----------------------------------------------------------------------------
fn sync_block_sockets(
    segmentid: &SegmentId,
    quest_structure: &mut QuestStructure,
    id: &BlockId,
    blocks: &Blocks,
) {
    use std::ops::DerefMut;

    if let Ok(ref mut questblock) = questgraph_cmds::find_block_mut(segmentid, id, quest_structure)
    {
        questgraph_cmds::sync_block_sockets(questblock.deref_mut(), blocks)
    }
}
// ----------------------------------------------------------------------------
// WaitUntil
// ----------------------------------------------------------------------------
fn add_named_condition(
    segmentid: &SegmentId,
    structure: &mut QuestStructure,
    id: &BlockId,
    socket: &OutSocketId,
    graphblocks: &mut Blocks,
) -> Result<(), String> {
    use model::questgraph::conditions::Condition;
    use model::shared::FactsDbCondition;
    use model::ValidatableElement;

    match questgraph_cmds::find_segmentblock_mut(segmentid, id, structure)? {
        SegmentBlock::WaitUntil(questblock) => {
            questblock.add_condition(
                Some(&socket.0),
                Condition::FactsDb(FactsDbCondition::default()),
            );
            if let Some(graphblock) = graphblocks.get_mut(id) {
                graphblock.set_validation_state(questblock.validate("error"));
            }
            Ok(())
        }
        _ => Err(format!("block {} not a wait-until block!", id)),
    }
}
// ----------------------------------------------------------------------------
fn delete_named_condition(
    segmentid: &SegmentId,
    structure: &mut QuestStructure,
    id: &BlockId,
    socket: &OutSocketId,

    graphblocks: &mut Blocks,
) -> Result<(), String> {
    use model::questgraph::conditions::Condition;
    use model::shared::FactsDbCondition;
    use model::ValidatableElement;

    match questgraph_cmds::find_segmentblock_mut(segmentid, id, structure)? {
        SegmentBlock::WaitUntil(questblock) => {
            let cond = questblock.conditions_mut();
            let idx = cond
                .iter()
                .enumerate()
                .find(|(_, c)| c.name() == socket.0)
                .map(|(i, _)| i);

            match idx {
                Some(idx) => {
                    cond.remove(idx);
                    // make sure there is at least one named condition left!
                    if cond.is_empty() {
                        questblock.add_condition(
                            Some(&(OutSocketId::default()).0),
                            Condition::FactsDb(FactsDbCondition::default()),
                        );
                    }
                    if let Some(graphblock) = graphblocks.get_mut(id) {
                        graphblock.set_validation_state(questblock.validate("error"));
                    }
                    Ok(())
                }
                None => Err(format!("socket {} not found on block {}!", socket, id)),
            }
        }
        _ => Err(format!("block {} not a wait-until block!", id)),
    }
}
// ----------------------------------------------------------------------------
// subsegment
// ----------------------------------------------------------------------------
#[inline]
fn get_segment_link(questblock: &SegmentBlock) -> Option<SegmentId> {
    if let SegmentBlock::SubSegment(block) = questblock {
        block.segmentlink().cloned()
    } else {
        None
    }
}
// ----------------------------------------------------------------------------
fn update_sockets_from_segment_in_out(
    referenced_segment: &Option<SegmentId>,
    segmentid: &SegmentId,
    blockid: &BlockId,
    quest_structure: &mut QuestStructure,
) -> Result<(), String> {
    let segment_io = match referenced_segment {
        Some(ref id) => quest_structure.segment(id).map(QuestSegment::in_out_ids),
        None => None,
    };

    let (in_sockets, out_sockets) = segment_io
        .map(|io| (io.0, io.1))
        .unwrap_or_else(|| (vec![], vec![]));

    if let SegmentBlock::SubSegment(questblock) =
        questgraph_cmds::find_segmentblock_mut(segmentid, blockid, quest_structure)?
    {
        super::cmds::update_subsegment_sockets(questblock, &in_sockets, &out_sockets);
    }
    questgraph_cmds::prune_links_to_block(quest_structure, segmentid, blockid, &in_sockets)?;

    Ok(())
}
// ----------------------------------------------------------------------------
fn save_subsegment_properties(
    quest_structure: &mut QuestStructure,
    selection: &mut SelectionState,
    blockid: &BlockId,
    properties: &[properties::Property],
    planestate: &mut PlaneState,
    graphblocks: &mut Blocks,
    graphedges: &mut Edges,
) -> Result<(), String> {
    use super::questblocks::EditableBlock;
    use model::ValidatableElement;

    let segmentid = &selection.segment.1;

    let questblock = questgraph_cmds::find_segmentblock_mut(segmentid, blockid, quest_structure)?;
    let prev_subsegment = get_segment_link(questblock);

    questblock.set_properties(properties)?;
    let validation = questblock.validate("error");

    let current_subsegment = get_segment_link(questblock);

    if current_subsegment != prev_subsegment {
        trace!("> subsegment changed... updating sockets...");
        update_sockets_from_segment_in_out(
            &current_subsegment,
            segmentid,
            blockid,
            quest_structure,
        )?;

        // this recreates all edges from questgraph, too
        questgraph_cmds::refresh_graph_after_questblock_update(
            selection,
            blockid,
            quest_structure,
            planestate,
            graphblocks,
            graphedges,
        )?;
    }

    validation
}
// ----------------------------------------------------------------------------
