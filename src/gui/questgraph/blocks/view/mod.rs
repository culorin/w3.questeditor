//
// block::view block drawing related functions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui::questgraph) mod menu;
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn draw_property_editor<'ui>(
    ui: &Ui<'ui>,
    state: &mut PropertyEditorState,
    result: &mut Option<QuestGraphAction>,
) {
    editor::draw(ui, state, result)
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph) fn properties_for_blocktype(
    block_type: &BlockId,
    is_valid: bool,
) -> BlockDrawProperties {
    use self::BlockId::*;

    let (rounding, color, mut border_color) = match *block_type {
        SubSegment(_) => (0.0, COL_SUBSEGMENT, COL_SUBSEGMENT_BORDER),
        QuestStart(_) => (25.0, COL_SEGMENT_IO, COL_BLACK_BORDER),
        QuestEnd(_) => (25.0, COL_SEGMENT_IO, COL_BLACK_BORDER),
        SegmentIn(_) => (25.0, COL_SEGMENT_IO, COL_BLACK_BORDER),
        SegmentOut(_) => (25.0, COL_SEGMENT_IO, COL_BLACK_BORDER),
        Scene(_) => (10.0, COL_SCENE, COL_BLOCK_BORDER),
        Interaction(_) => (0.0, COL_INTERACTION, COL_BLOCK_BORDER),
        WaitUntil(_) => (10.0, COL_WAITUNTIL, COL_WAITUNTIL_BORDER),
        Script(_) => (0.0, COL_SCRIPT, COL_BLACK_BORDER),
        Layers(_) => (0.0, COL_BLOCK, COL_BLOCK_BORDER),
        Spawnsets(_) => (0.0, COL_COMMUNITY, COL_BLOCK_BORDER),
        Spawn(_) => (25.0, COL_COMMUNITY, COL_BLOCK_BORDER),
        Despawn(_) => (25.0, COL_COMMUNITY, COL_COMMUNITY_DARK_BORDER),
        JournalEntry(_) => (25.0, COL_JOURNAL, COL_BLACK_BORDER),
        JournalObjective(_) => (0.0, COL_OBJECTIVE, COL_OBJECTIVE_BORDER),
        JournalMappin(_) => (10.0, COL_MAPPIN, COL_MAPPIN_BORDER),
        JournalPhaseObjectives(_) => (0.0, COL_PHASEOBJECTIVES, COL_PHASEOBJECTIVES_BORDER),
        JournalQuestOutcome(_) => (10.0, COL_QUESTOUTCOME, COL_QUESTOUTCOME_BORDER),
        PauseTime(_) => (0.0, COL_BLOCK, COL_BLACK_BORDER),
        UnpauseTime(_) => (0.0, COL_BLOCK, COL_BLACK_BORDER),
        ShiftTime(_) => (0.0, COL_BLOCK, COL_BLACK_BORDER),
        SetTime(_) => (0.0, COL_BLOCK, COL_BLACK_BORDER),
        AddFact(_) => (25.0, COL_ADDFACT, COL_BLOCK_BORDER),
        Teleport(_) => (25.0, COL_TELEPORT, COL_TELEPORT_BORDER),
        ChangeWorld(_) => (25.0, COL_CHANGEWORLD, COL_CHANGEWORLD_BORDER),
        Reward(_) => (25.0, COL_REWARD, COL_REWARD_BORDER),
        Randomize(_) => (0.0, COL_RANDOMIZE, COL_RANDOMIZE_BORDER),
        Invalid => (0.0, COL_BLOCK, COL_BLOCK_BORDER),
    };

    if !is_valid {
        border_color = (1.0, 0.0, 0.0, 1.0);
    }

    BlockDrawProperties::new(rounding, color, border_color)
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::Ui;
use imgui_controls::graph::BlockDrawProperties;

use super::{BlockId, GraphBlock, QuestBlock};

// actions
use super::{BlockAction, BlockEditorAction, MenuAction, QuestGraphAction};

// state
use super::PropertyEditorState;
use super::SocketNameRequest;

// misc
use super::EditorBlockHint;
use super::MAX_SOCKETS;
// ----------------------------------------------------------------------------
// colors in imgui are RGBA with range 0 - 1.0
type Color = (f32, f32, f32, f32);
// ----------------------------------------------------------------------------
const COL_BLOCK: Color = (0.5, 0.5, 0.5, 1.0);
const COL_BLOCK_BORDER: Color = (1.0, 1.0, 1.0, 0.7);

const COL_BLACK_BORDER: Color = (29.0 / 255.0, 29.0 / 255.0, 29.0 / 255.0, 0.7);
const COL_SEGMENT_IO: Color = (69.0 / 255.0, 69.0 / 255.0, 69.0 / 255.0, 1.0);
const COL_JOURNAL: Color = (150.0 / 255.0, 165.0 / 255.0, 65.0 / 255.0, 1.0);
const COL_MAPPIN: Color = (0.44, 0.53, 0.35, 1.0);
const COL_MAPPIN_BORDER: Color = (0.21, 0.31, 0.11, 0.9);
const COL_OBJECTIVE: Color = (0.36, 0.5, 0.21, 1.0);
const COL_OBJECTIVE_BORDER: Color = (0.17, 0.24, 0.1, 0.7);
const COL_PHASEOBJECTIVES: Color = (0.3, 0.4, 0.2, 1.0);
const COL_PHASEOBJECTIVES_BORDER: Color = (0.21, 0.31, 0.11, 0.9);
const COL_QUESTOUTCOME: Color = (0.23, 0.33, 0.12, 1.0);
const COL_QUESTOUTCOME_BORDER: Color = (0.14, 0.17, 0.10, 0.7);
const COL_INTERACTION: Color = (58.0 / 255.0, 138.0 / 255.0, 171.0 / 255.0, 1.0);
const COL_SCENE: Color = (108.0 / 255.0, 162.0 / 255.0, 188.0 / 255.0, 1.0);
const COL_SCRIPT: Color = (135.0 / 255.0, 102.0 / 255.0, 75.0 / 255.0, 1.0);
const COL_ADDFACT: Color = COL_SCRIPT;
const COL_WAITUNTIL: Color = (0.5, 0.2, 0.2, 1.0);
const COL_WAITUNTIL_BORDER: Color = (0.4, 0.1, 0.1, 1.0);
const COL_COMMUNITY: Color = (62.0 / 255.0, 84.0 / 255.0, 138.0 / 255.0, 1.0);
const COL_COMMUNITY_DARK_BORDER: Color = (22.0 / 255.0, 30.0 / 255.0, 50.0 / 255.0, 0.5);
const COL_TELEPORT: Color = (0.5, 0.5, 0.5, 1.0);
const COL_TELEPORT_BORDER: Color = (1.0, 1.0, 1.0, 0.7);
const COL_CHANGEWORLD: Color = (0.5, 0.5, 0.5, 1.0);
const COL_CHANGEWORLD_BORDER: Color = (30.0 / 255.0, 30.0 / 255.0, 30.0 / 255.0, 0.7);
const COL_REWARD: Color = (140.0 / 255.0, 175.0 / 255.0, 160.0 / 255.0, 1.0);
const COL_REWARD_BORDER: Color = (30.0 / 255.0, 30.0 / 255.0, 30.0 / 255.0, 0.7);
const COL_SUBSEGMENT: Color = (90.0 / 255.0, 90.0 / 255.0, 90.0 / 255.0, 1.0);
const COL_SUBSEGMENT_BORDER: Color = (30.0 / 255.0, 30.0 / 255.0, 30.0 / 255.0, 0.7);
const COL_RANDOMIZE: Color = (90.0 / 255.0, 80.0 / 255.0, 110.0 / 255.0, 1.0);
const COL_RANDOMIZE_BORDER: Color = (30.0 / 255.0, 30.0 / 255.0, 30.0 / 255.0, 0.7);
// ----------------------------------------------------------------------------
mod editor;
mod popup;
// ----------------------------------------------------------------------------
