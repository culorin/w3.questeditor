//
// blocks::view::editor
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn draw<'ui>(
    ui: &Ui<'ui>,
    state: &mut PropertyEditorState,
    result: &mut Option<QuestGraphAction>,
) {
    ui.spacing();
    ui.text(im_str!("blocktype"));
    ui.same_line(view::LABEL_WIDTH);
    ui.text(&state.blocktype);
    ui.spacing();

    if let Some(new_value) = state.blockname_mut().draw(ui) {
        *result = Some(BlockEditorAction::UpdateField("blockname".into(), new_value.into()).into());
    }

    ui.separator();
    for (i, property) in state.properties.iter_mut().enumerate() {
        ui.with_id(i as i32, || {
            render_property!(ui, property, *result);
        });
    }
    if !state.properties().is_empty() {
        ui.separator();
    }

    let is_changed = state.changed();

    let width = ui.get_contentregion_width();
    let button_size = (130.0, 0.0);
    let padding = 2.0 * 4.0;

    ui.spacing();
    ui.with_style_var(::imgui::StyleVar::FramePadding((0.0, 2.0).into()), || {
        if ui.enabled_button(im_str!("discard changes"), button_size, is_changed) {
            *result = Some(BlockEditorAction::DiscardChanges.into());
        }

        ui.same_line(width - button_size.0 + padding);
        if ui.enabled_button(
            im_str!("save changes"),
            button_size,
            is_changed && state.blockname().valid(),
        ) {
            *result = Some(BlockEditorAction::SaveProperties.into());
        }
    });
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::Ui;

use super::super::properties::view;

// actions
use super::{BlockEditorAction, QuestGraphAction};

// state
use super::PropertyEditorState;
// ----------------------------------------------------------------------------
