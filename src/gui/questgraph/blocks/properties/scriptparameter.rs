//
// blocks::properties::scriptparameter
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
/// selectable script call parameter types
#[derive(Debug, PartialEq)]
pub(in gui) enum VarType {
    String,
    CName,
    Float,
    Bool,
    Int32,
}
// ----------------------------------------------------------------------------
/// container for type, name and value fields for one script call parameter
pub(in gui::questgraph::blocks) struct ScriptParameter {
    pub(super) v_type: VarType,
    pub(super) v_name: input::TextField,
    pub(super) v_value: Box<dyn input::Field>,
}
// ----------------------------------------------------------------------------
/// container for a script call parameter property (multiple call parameters)
pub(in gui::questgraph::blocks) struct ScriptParameterSet {
    pub(super) error: Result<(), ImString>,
    pub(super) params: Vec<ScriptParameter>,
    // required for adding/removing params which cannot be extracted otherwise
    pub(super) changed: bool,
}
// ----------------------------------------------------------------------------
pub(super) const MAX_SCRIPTPARAMS: usize = 10;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;

use imgui::ImString;
use imgui_controls::input;
use imgui_controls::input::validator;
use imgui_controls::input::Field;

// actions
use super::PropertyAction;

// misc
use model::shared::ScriptParameter as ModelScriptParam;

use super::PropertyControl;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl ScriptParameterSet {
    // ------------------------------------------------------------------------
    pub fn new(params: &[ModelScriptParam]) -> ScriptParameterSet {
        ScriptParameterSet {
            changed: false,
            error: Ok(()),
            params: params.iter().map(ScriptParameter::from).collect(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn params(&self) -> &Vec<ScriptParameter> {
        &self.params
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn create_varname_field(name: &str) -> input::TextField {
    input::TextField::new("##name", Some(name)).set_validators(vec![
        validator::is_nonempty(),
        validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
        validator::min_length(1),
        validator::max_length(100),
    ])
}
// ----------------------------------------------------------------------------
impl ScriptParameter {
    // ------------------------------------------------------------------------
    pub fn valid(&self) -> bool {
        self.v_name.valid() && self.v_value.valid()
    }
    // ------------------------------------------------------------------------
    pub fn changed(&self) -> bool {
        self.v_name.changed() || self.v_value.changed()
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) {
        self.v_name.validate();
        self.v_value.validate();
    }
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.v_name.reset_changed();
        self.v_value.reset_changed();
    }
    // ------------------------------------------------------------------------
    fn new_string(name: &str, value: &str) -> ScriptParameter {
        ScriptParameter {
            v_type: VarType::String,
            v_name: create_varname_field(name),
            v_value: Box::new(input::TextField::new("##value", Some(value))),
        }
    }
    // ------------------------------------------------------------------------
    fn new_cname(name: &str, value: &str) -> ScriptParameter {
        ScriptParameter {
            v_type: VarType::CName,
            v_name: create_varname_field(name),
            v_value: Box::new(
                input::TextField::new("##value", Some(value)).set_validators(vec![
                    validator::is_nonempty(),
                    validator::min_length(1),
                    validator::max_length(100),
                    validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
                ]),
            ),
        }
    }
    // ------------------------------------------------------------------------
    fn new_float(name: &str, value: f32) -> ScriptParameter {
        ScriptParameter {
            v_type: VarType::Float,
            v_name: create_varname_field(name),
            v_value: Box::new(input::FloatField::new("##value", Some(value))),
        }
    }
    // ------------------------------------------------------------------------
    fn new_int32(name: &str, value: i32) -> ScriptParameter {
        ScriptParameter {
            v_type: VarType::Int32,
            v_name: create_varname_field(name),
            v_value: Box::new(input::IntField::new("##value", Some(value))),
        }
    }
    // ------------------------------------------------------------------------
    fn new_bool(name: &str, value: bool) -> ScriptParameter {
        ScriptParameter {
            v_type: VarType::Bool,
            v_name: create_varname_field(name),
            v_value: Box::new(input::BoolField::new("##value", value)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for ScriptParameterSet {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
        self.params.iter_mut().for_each(ScriptParameter::reset_changed);
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        let invalid = self.params
            .iter()
            .enumerate()
            .filter(|(_, p)| !p.valid())
            .map(|(i, _)| format!("{}", i + 1))
            .collect::<Vec<_>>();

        self.error = match invalid.len() {
            0 => Ok(()),
            1 => Err(ImString::from(format!(
                "parameter {} is invalid.",
                invalid.join("")
            ))),
            _ => Err(ImString::from(format!(
                "parameters {} are invalid.",
                invalid.join(", ")
            ))),
        };
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            ListItemAdd => {
                // trigger validate to indicate missing var name
                let mut param = ScriptParameter::new_string("", "");
                param.validate();
                self.params.push(param);
            }
            ListItemDel(index) if index < self.params.len() => {
                self.params.remove(index);
            }
            ScriptParamSetType(index, ref new_type) => {
                set_paramtype(index, new_type, &mut self.params)?;
            }
            ScriptParamUpdateName(index, ref value) => {
                if let Some(param) = self.params.get_mut(index) {
                    param.v_name.validate().set_value(value.into())?
                }
            }
            ListItemUpdate(index, ref value) => {
                if let Some(param) = self.params.get_mut(index) {
                    param.v_value.validate().set_value(value.into())?
                }
            }
            _ => unreachable!(
                "scriptparameter property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// actions
// ----------------------------------------------------------------------------
fn set_paramtype(
    index: usize,
    new_type: &VarType,
    param_list: &mut Vec<ScriptParameter>,
) -> Result<(), String> {
    if let Some(param) = param_list.get_mut(index) {
        let name = param.v_name.value().as_str()?;
        if param.v_type != *new_type {
            *param = match new_type {
                VarType::String => ScriptParameter::new_string(name, ""),
                VarType::CName => ScriptParameter::new_cname(name, ""),
                VarType::Float => ScriptParameter::new_float(name, 0.0),
                VarType::Int32 => ScriptParameter::new_int32(name, 0),
                VarType::Bool => ScriptParameter::new_bool(name, false),
            };
            param.validate();
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
impl<'model> From<&'model ModelScriptParam> for ScriptParameter {
    // ------------------------------------------------------------------------
    fn from(param: &ModelScriptParam) -> ScriptParameter {
        use self::ModelScriptParam::*;

        let mut param = match param {
            String(ref name, ref value) => {
                ScriptParameter::new_string(name.as_str(), value.as_str())
            }
            CName(ref name, ref value) => ScriptParameter::new_cname(name.as_str(), value.as_str()),
            Float(ref name, value) => ScriptParameter::new_float(name.as_str(), *value),
            Int32(ref name, value) => ScriptParameter::new_int32(name.as_str(), *value),
            Bool(ref name, value) => ScriptParameter::new_bool(name.as_str(), *value),
        };
        param.validate();
        param
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
use std::convert::TryFrom;

impl<'ui> TryFrom<&'ui ScriptParameter> for ModelScriptParam {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(param: &ScriptParameter) -> Result<Self, String> {
        let name = param.v_name.value().as_str()?.to_owned();

        let param = match param.v_type {
            VarType::String => {
                ModelScriptParam::String(name, param.v_value.value().as_str()?.to_owned())
            }
            VarType::CName => {
                ModelScriptParam::CName(name, param.v_value.value().as_str()?.to_owned())
            }
            VarType::Float => ModelScriptParam::Float(name, param.v_value.value().as_f32()?),
            VarType::Int32 => ModelScriptParam::Int32(name, param.v_value.value().as_i32()?),
            VarType::Bool => ModelScriptParam::Bool(name, param.v_value.value().as_bool()?),
        };
        Ok(param)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
