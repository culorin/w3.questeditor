//
// questgraph::blocks::properties
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
pub(in gui) struct PropertyId(&'static str);
// ----------------------------------------------------------------------------
pub(in gui) enum Property {
    Field(PropertyId, Box<dyn input::Field>),
    Control(PropertyId, Box<dyn PropertyControl>),
}
// ----------------------------------------------------------------------------
// Property controls
// ----------------------------------------------------------------------------
pub(super) use self::conditions::{
    AreaCondition, FactCondition, InteractionCondition, LoadScreenCondition, LogicConditionSet,
    NamedConditionSet, TimeCondition,
};
pub(super) use self::misc::TagList;
pub(super) use self::references::{
    ChangeLayerControl, CommunityOptions, CommunityReferenceType, CommunitySet,
    InteractionReference, JournalMapPinReference, JournalObjectiveReference,
    JournalPhaseObjectivesReference, JournalQuestOutcomeReference, JournalReference,
    LabeledReference, RewardReference, RewardReferenceType, SceneReference, SegmentReference,
    TagReference,
};
pub(super) use self::scriptparameter::{ScriptParameter, ScriptParameterSet, VarType};
// ----------------------------------------------------------------------------
// traits
// ----------------------------------------------------------------------------
pub(in gui) trait PropertyControl: view::PropertyView {
    fn reset_changed(&mut self);
    fn validate(&mut self) -> &mut dyn PropertyControl;
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String>;
    fn as_any(&self) -> &dyn Any;
}
// ----------------------------------------------------------------------------
// actions
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub(in gui) enum PropertyAction {
    /// generic (simple properties) value update
    Update(FieldValue),
    /// update for one of multiple properties wrapped in a container that are
    /// generic but reference a specific property
    SubfieldUpdate(u8, FieldValue),
    /// update for one of mutliple properties wrapped in a container that are
    /// more complex than simple value updates but reference a specific property
    SubfieldAction(u8, Box<PropertyAction>),
    // generic list
    ListItemAdd,
    ListItemDel(usize),
    ListItemUpdate(usize, FieldValue),
    ListItemAction(usize, Box<PropertyAction>),
    // scriptparameter actions
    ScriptParamSetType(usize, VarType),
    ScriptParamUpdateName(usize, FieldValue),
    // *references
    ReferenceSetType(u8),
    ReferenceSetTag(WorldId, TagId),
    ReferenceSetInteractionEntityType(InteractionEntityType),
    ReferenceSetInteractionEntity(WorldId, InteractionEntityId),
    ReferenceSetSubsegment(::model::questgraph::SegmentId),
    // *conditions
    ConditionSetCompareType(::model::shared::CompareFunction),
    ConditionSetType(ConditionType),
    // *conditionset
    ConditionListSetLogicType(::model::shared::LogicOperation),
    // *layer
    LayersSetLayer(LayerId),
    LayersSetWorld(WorldId),
    // *community
    CommunitySetType(CommunityReferenceType),
    CommunitySetCommunity(CommunityId),
    CommunitySetPhase(PhaseId),
    CommunitySetPhaseAndCommunity(PhaseId, CommunityId),
    // items
    RewardSetType(RewardReferenceType),
    RewardSetReward(RewardId),
    // *jourmals
    JournalSetEntry(JournalGroupId, JournalEntryId),
    JournalSetObjective(JournalQuestId, JournalQuestPhaseId, JournalQuestObjectiveId),
    JournalSetPhase(JournalQuestId, JournalQuestPhaseId),
    JournalSetQuest(JournalQuestId),
    JournalSetMapPin(
        JournalQuestId,
        JournalQuestPhaseId,
        JournalQuestObjectiveId,
        JournalQuestMapPinId,
    ),
}
// ----------------------------------------------------------------------------
// view
// ----------------------------------------------------------------------------
// macro expose
#[macro_use]
pub(super) mod view;
// ----------------------------------------------------------------------------
// action processing
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn handle_action(
    id: &PropertyId,
    action: PropertyAction,
    state: &mut PropertyEditorState,
) -> Result<Option<()>, String> {
    update::handle_action(id, action, state).map(|_| None)
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;

use imgui_controls::input;
use imgui_controls::input::FieldAction;

// state
use super::{
    AreaConditionType, ConditionType, InteractionConditionType, LoadScreenConditionType,
    PropertyEditorState, TimeConditionType,
};

use self::references::InteractionEntityType;

// actions
use super::BlockEditorAction;

// misc
use gui::registry::{
    CommunityId, InteractionEntityId, JournalEntryId, JournalGroupId, JournalQuestId,
    JournalQuestMapPinId, JournalQuestObjectiveId, JournalQuestPhaseId, LayerId, PhaseId, RewardId,
    TagId, WorldId,
};
use gui::types::FieldValue;
// ----------------------------------------------------------------------------
mod update;

mod conditions;
mod misc;
mod references;
mod scriptparameter;
// ----------------------------------------------------------------------------
impl PropertyId {
    pub fn as_str(&self) -> &'static str {
        self.0
    }
}
// ----------------------------------------------------------------------------
impl Property {
    // ------------------------------------------------------------------------
    pub fn new_field<I: Into<PropertyId>>(id: I, mut field: Box<dyn input::Field>) -> Property {
        // trigger validation on creation (e.g. empty fields with no_empty validators)
        field.validate();
        Property::Field(id.into(), field)
    }
    // ------------------------------------------------------------------------
    pub fn new_control<I: Into<PropertyId>>(id: I, mut control: Box<dyn PropertyControl>) -> Property {
        // trigger validation on creation (e.g. default empty child fields with no_empty validators)
        control.validate();
        Property::Control(id.into(), control)
    }
    // ------------------------------------------------------------------------
    pub fn id(&self) -> &str {
        use self::Property::*;

        match self {
            Field(ref id, _) | Control(ref id, _) => id.0,
        }
    }
    // ------------------------------------------------------------------------
    #[inline]
    pub fn changed(&self) -> bool {
        match self {
            Property::Field(_, ref field) => field.changed(),
            Property::Control(_, ref control) => control.changed(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn reset_changed(&mut self) {
        match self {
            Property::Field(_, ref mut field) => field.reset_changed(),
            Property::Control(_, ref mut control) => control.reset_changed(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Action converter
// ----------------------------------------------------------------------------
impl<'a> From<PropertyAction> for FieldAction<'a, PropertyAction> {
    fn from(action: PropertyAction) -> FieldAction<'a, PropertyAction> {
        FieldAction::Custom(action)
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<input::FieldValue<'a>> for PropertyAction {
    // ------------------------------------------------------------------------
    fn from(value: input::FieldValue<'a>) -> PropertyAction {
        PropertyAction::Update(value.into())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<(PropertyId, input::FieldValue<'a>)> for BlockEditorAction {
    // ------------------------------------------------------------------------
    fn from(data: (PropertyId, input::FieldValue)) -> BlockEditorAction {
        let (id, value) = data;
        BlockEditorAction::UpdateField(id, value.into())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// type converter
// ----------------------------------------------------------------------------
impl From<&'static str> for PropertyId {
    fn from(id: &'static str) -> PropertyId {
        PropertyId(id)
    }
}
// ----------------------------------------------------------------------------
