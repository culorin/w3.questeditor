//
// blocks::properties::conditions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct FactCondition {
    pub(super) factid: input::TextField,
    pub(super) compareop: CompareFunction,
    pub(super) value: input::IntField,
    pub(super) changed: bool,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct AreaCondition {
    _type: AreaConditionType,
    pub(super) actor: input::TextField,
    pub(super) area: LabeledReference<TagReference>,
    pub(super) changed: bool,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct TimeCondition {
    _type: TimeConditionType,
    pub(super) time1: input::TextField,
    pub(super) time2: Option<input::TextField>,
    pub(super) changed: bool,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct InteractionCondition {
    _type: InteractionConditionType,
    pub(super) entity: LabeledReference<InteractionReference>,
    pub(super) interaction: Option<input::TextField>,
    pub(super) changed: bool,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct LoadScreenCondition {
    pub(super) _type: LoadScreenConditionType,
    pub(super) changed: bool,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct Condition {
    pub(super) _type: ConditionType,
    pub(super) control: Box<dyn PropertyControl>,
    pub(super) changed: bool,
    areas: Rc<TagList>,
    interactive_entities: Rc<InteractionList>,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct LogicConditionSet {
    pub(super) logicop: LogicOperation,
    pub(super) conditions: Vec<Condition>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
    areas: Rc<TagList>,
    interactive_entities: Rc<InteractionList>,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct NamedCondition {
    socket: String,
    pub(super) caption: ImString,
    pub(super) cond: FactCondition,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct NamedConditionSet {
    pub(super) conditions: Vec<NamedCondition>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(super) const MAX_CONDITIONS: usize = 5;
pub(super) use super::ConditionType;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;
use std::rc::Rc;

use imgui::ImString;
use imgui_controls::input;
use imgui_controls::input::validator;
use imgui_controls::input::Field;

// actions
use super::PropertyAction;

// misc
use gui::registry::TagList;

use self::model::{CompareFunction, LogicOperation};
use super::{
    AreaConditionType, InteractionConditionType, LoadScreenConditionType, TimeConditionType,
};
use super::{InteractionReference, LabeledReference, TagReference};

use super::view::{PropertyView, FIELD_WIDTH_TIME_HHMM, FIELD_WIDTH_TIME_HHMMSS, LABEL_WIDTH};
use super::PropertyControl;
// ----------------------------------------------------------------------------
type AreaList = ::gui::registry::TagList;
type InteractionList = ::gui::registry::InteractionList;
// ----------------------------------------------------------------------------
/// local reexport under a common module prefix-name
mod model {
    pub use model::primitives::references::{InteractionReference, LayerTagReference};
    pub use model::primitives::{HiResTime, Time};
    pub use model::questgraph::conditions::{
        Condition, EnteredLeftAreaCondition, InsideOutsideAreaCondition, InteractionCondition,
        InteractionType, LoadingScreenCondition, LogicCondition, TimeCondition,
    };
    pub use model::questgraph::primitives::OutSocketId;
    pub use model::shared::{CompareFunction, FactsDbCondition, LogicOperation};
}
// ----------------------------------------------------------------------------
impl FactCondition {
    // ------------------------------------------------------------------------
    pub fn new(cond: Option<&model::FactsDbCondition>) -> FactCondition {
        FactCondition {
            factid: input::TextField::new_with_label(
                "##factid",
                "fact",
                cond.map(|c| c.fact().as_str()),
            )
            .set_validators(vec![
                validator::is_nonempty(),
                validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
                validator::min_length(2),
                validator::max_length(250),
            ])
            .set_label_width(LABEL_WIDTH),
            compareop: cond
                .map(|c| c.comparefunction().clone())
                .unwrap_or(CompareFunction::EQ),
            value: input::IntField::new("##factvalue", cond.map(|c| *c.value())),
            changed: false,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl AreaCondition {
    // ------------------------------------------------------------------------
    fn new(
        _type: AreaConditionType,
        areas: Rc<AreaList>,
        actor: Option<&str>,
        area_label: &str,
        area: Option<&model::LayerTagReference>,
    ) -> AreaCondition {
        AreaCondition {
            _type,
            actor: input::TextField::new_with_label(
                "##actor",
                "actor",
                Some(actor.unwrap_or("PLAYER")),
            )
            .set_validators(vec![
                validator::is_nonempty(),
                validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
                validator::min_length(2),
                validator::max_length(250),
            ])
            .set_label_width(LABEL_WIDTH),
            area: LabeledReference::new(LABEL_WIDTH, area_label, areas, true, area),
            changed: false,
        }
    }
    // ------------------------------------------------------------------------
    fn new_with(interaction: &AreaConditionType, areas: Rc<AreaList>) -> AreaCondition {
        use self::AreaConditionType::*;

        let interaction = interaction.clone();

        match interaction {
            Entered => Self::new(interaction, areas, None, "entered", None),
            Left => Self::new(interaction, areas, None, "left", None),
            Inside => Self::new(interaction, areas, None, "inside", None),
            Outside => Self::new(interaction, areas, None, "outside", None),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TimeCondition {
    // ------------------------------------------------------------------------
    fn new<T: Into<String>>(
        _type: TimeConditionType,
        label: &str,
        time1: T,
        time2: Option<T>,
    ) -> TimeCondition {
        let time1 = if let TimeConditionType::Elapsed = _type {
            input::TextField::new("##time1", Some(time1))
                .set_validators(vec![validator::is_nonempty(), validator::is_hhmmss()])
                .set_width(FIELD_WIDTH_TIME_HHMMSS)
        } else {
            input::TextField::new("##time1", Some(time1))
                .set_validators(vec![validator::is_nonempty(), validator::is_hhmm()])
                .set_width(FIELD_WIDTH_TIME_HHMM)
        };

        TimeCondition {
            _type,
            time1: time1.set_label(label).set_label_width(LABEL_WIDTH),
            time2: time2.map(|time| {
                input::TextField::new("##time2", Some(time))
                    .set_width(FIELD_WIDTH_TIME_HHMM)
                    .set_validators(vec![validator::is_hhmm()])
            }),
            changed: false,
        }
    }
    // ------------------------------------------------------------------------
    fn new_with(cond: &TimeConditionType) -> TimeCondition {
        use self::TimeConditionType::*;

        let cond = cond.clone();

        match cond {
            Range => Self::new(cond, "between", "12:00", Some("")),
            After => Self::new(cond, "after", "12:00", None),
            Before => Self::new(cond, "before", "12:00", None),
            Elapsed => Self::new(cond, "elapsed", "00:00:15", None),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl InteractionCondition {
    // ------------------------------------------------------------------------
    fn new(
        _type: InteractionConditionType,
        entities: Rc<InteractionList>,
        entity_label: &str,
        entity: Option<&model::InteractionReference>,
        custom_interaction: Option<&str>,
    ) -> InteractionCondition {
        InteractionCondition {
            _type,
            interaction: custom_interaction.map(|interaction| {
                input::TextField::new_with_label(
                    "##interaction",
                    // TODO shortened caption only if embedded in logic condition
                    "interact.",
                    Some(interaction),
                )
                .set_validators(vec![
                    validator::is_nonempty(),
                    validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
                    validator::min_length(2),
                    validator::max_length(250),
                ])
                .set_label_width(LABEL_WIDTH)
            }),
            entity: LabeledReference::new(LABEL_WIDTH, entity_label, entities, true, entity),
            changed: false,
        }
    }
    // ------------------------------------------------------------------------
    fn new_with(
        interaction: &InteractionConditionType,
        entities: Rc<InteractionList>,
    ) -> InteractionCondition {
        use self::InteractionConditionType::*;

        let interaction = interaction.clone();

        match interaction {
            Custom => Self::new(interaction, entities, "entity", None, Some("")),
            Examined => Self::new(interaction, entities, "examined", None, None),
            Talked => Self::new(interaction, entities, "talked to", None, None),
            Looted => Self::new(interaction, entities, "looted", None, None),
            Used => Self::new(interaction, entities, "used", None, None),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Condition {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
        self.control.reset_changed();
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut Self {
        self.control.validate();
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::ConditionType::*;
        use self::PropertyAction::*;

        self.changed = true;

        match action {
            ConditionSetType(cond_type) => {
                self.control = match cond_type {
                    Fact => Box::new(FactCondition::new(None)),
                    Area(ref area_interaction) => Box::new(AreaCondition::new_with(
                        area_interaction,
                        self.areas.clone(),
                    )),
                    Time(ref time_cond) => Box::new(TimeCondition::new_with(time_cond)),
                    Interaction(ref entity_interaction) => {
                        Box::new(InteractionCondition::new_with(
                            entity_interaction,
                            self.interactive_entities.clone(),
                        ))
                    }
                    LoadScreen(_) => {
                        return Err(String::from("loadscreen conditions not supported"))
                    }
                    Logic(_) => {
                        return Err(String::from("recursive logic conditions not supported"))
                    }
                    MultiOutFact => {
                        return Err(String::from("multiout fact conditions not supported"))
                    }
                };
                self.control.validate();
                self._type = cond_type;
                Ok(())
            }
            _ => self.control.process_action(action),
        }
    }
    // ------------------------------------------------------------------------
    fn new_fact(
        areas: Rc<TagList>,
        interactive_entities: Rc<InteractionList>,
        cond: Option<&model::FactsDbCondition>,
    ) -> Condition {
        Condition {
            _type: ConditionType::Fact,
            control: Box::new(FactCondition::new(cond)),
            changed: false,
            areas,
            interactive_entities,
        }
    }
    // ------------------------------------------------------------------------
    fn new_entered_area(
        areas: Rc<TagList>,
        interactive_entities: Rc<InteractionList>,
        cond: &model::EnteredLeftAreaCondition,
    ) -> Condition {
        use self::AreaConditionType::*;

        let area_interaction = if *cond.has_entered() { Entered } else { Left };

        Condition {
            _type: ConditionType::Area(area_interaction),
            control: Box::new(AreaCondition::from((areas.clone(), cond))),
            changed: false,
            areas,
            interactive_entities,
        }
    }
    // ------------------------------------------------------------------------
    fn new_inside_area(
        areas: Rc<TagList>,
        interactive_entities: Rc<InteractionList>,
        cond: &model::InsideOutsideAreaCondition,
    ) -> Condition {
        use self::AreaConditionType::*;

        let area_interaction = if *cond.is_inside() { Inside } else { Outside };

        Condition {
            _type: ConditionType::Area(area_interaction),
            control: Box::new(AreaCondition::from((areas.clone(), cond))),
            changed: false,
            areas,
            interactive_entities,
        }
    }
    // ------------------------------------------------------------------------
    fn new_time(
        areas: Rc<TagList>,
        interactive_entities: Rc<InteractionList>,
        cond: &model::TimeCondition,
    ) -> Condition {
        Condition {
            _type: ConditionType::Time(TimeConditionType::from(cond)),
            control: Box::new(TimeCondition::from(cond)),
            changed: false,
            areas,
            interactive_entities,
        }
    }
    // ------------------------------------------------------------------------
    fn new_interaction(
        areas: Rc<TagList>,
        interactive_entities: Rc<InteractionList>,
        cond: &model::InteractionCondition,
    ) -> Condition {
        Condition {
            _type: ConditionType::Interaction(InteractionConditionType::from(cond)),
            control: Box::new(InteractionCondition::from((
                interactive_entities.clone(),
                cond,
            ))),
            changed: false,
            areas,
            interactive_entities,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LogicConditionSet {
    // ------------------------------------------------------------------------
    pub fn new(
        areas: Rc<TagList>,
        interactive_entities: Rc<InteractionList>,
        conditionset: &model::LogicCondition,
    ) -> LogicConditionSet {
        LogicConditionSet {
            logicop: conditionset.operation().clone(),
            conditions: conditionset
                .conditions()
                .map(|c| Condition::from((areas.clone(), interactive_entities.clone(), c)))
                .collect::<Vec<_>>(),
            changed: false,
            error: Ok(()),
            areas,
            interactive_entities,
        }
    }
    // ------------------------------------------------------------------------
    pub fn logic_op(&self) -> LogicOperation {
        self.logicop.clone()
    }
    // ------------------------------------------------------------------------
    pub fn conditions(&self) -> &Vec<Condition> {
        &self.conditions
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl NamedCondition {
    // ------------------------------------------------------------------------
    pub fn socket(&self) -> &str {
        &self.socket
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl NamedConditionSet {
    // ------------------------------------------------------------------------
    pub fn new(conditionset: &[model::Condition]) -> NamedConditionSet {
        NamedConditionSet {
            conditions: conditionset
                .iter()
                .filter_map(|c| NamedCondition::try_from(c).ok())
                .collect::<Vec<_>>(),
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
    pub fn conditions(&self) -> &Vec<NamedCondition> {
        &self.conditions
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// PropertyControl impls
// ----------------------------------------------------------------------------
impl PropertyControl for FactCondition {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.factid.reset_changed();
        self.value.reset_changed();
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.factid.validate();
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            SubfieldUpdate(id, ref value) if id == 0 => {
                self.factid.validate().set_value(value.into())?
            }
            SubfieldUpdate(id, ref value) if id == 1 => {
                self.value.validate().set_value(value.into())?
            }
            ConditionSetCompareType(op) => {
                self.compareop = op;
            }
            _ => unreachable!(
                "condition property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for AreaCondition {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.actor.reset_changed();
        self.area.reset_changed();
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.actor.validate();
        self.area.validate();
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            SubfieldUpdate(id, ref value) if id == 0 => {
                self.actor.validate().set_value(value.into())?
            }
            action => self.area.process_action(action)?,
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for TimeCondition {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.time1.reset_changed();
        if let Some(t) = self.time2.as_mut() {
            t.reset_changed()
        }
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.time1.validate();
        self.time2.as_mut().map(Field::validate);
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            SubfieldUpdate(id, ref value) if id == 0 => {
                self.time1.validate().set_value(value.into())?
            }
            SubfieldUpdate(id, ref value) if id == 1 && self.time2.is_some() => {
                if let Some(ref mut time) = self.time2 {
                    time.validate().set_value(value.into())?
                }
            }
            _ => unreachable!(
                "condition property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for InteractionCondition {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.entity.reset_changed();
        if let Some(i) = self.interaction.as_mut() {
            i.reset_changed()
        }
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.entity.validate();
        self.interaction.as_mut().map(Field::validate);
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            SubfieldUpdate(id, ref value) if id == 0 => {
                if let Some(ref mut interaction) = self.interaction {
                    interaction.validate().set_value(value.into())?
                }
            }
            action => self.entity.process_action(action)?,
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for LoadScreenCondition {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, _action: PropertyAction) -> Result<(), String> {
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for NamedCondition {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.cond.reset_changed();
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.cond.validate();
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        self.cond.process_action(action)
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for LogicConditionSet {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
        self.conditions
            .iter_mut()
            .for_each(Condition::reset_changed);
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.conditions.iter_mut().for_each(|c| {
            c.validate();
        });

        let invalid = self
            .conditions
            .iter()
            .enumerate()
            .filter(|(_, c)| !c.valid())
            .map(|(i, _)| format!("{}", i + 1))
            .collect::<Vec<_>>();

        self.error = match invalid.len() {
            0 if self.conditions.len() < 2 => Err(ImString::new("at least 2 conditions required.")),
            0 => Ok(()),
            1 => Err(ImString::from(format!(
                "condition {} is invalid.",
                invalid.join("")
            ))),
            _ => Err(ImString::from(format!(
                "condition {} are invalid.",
                invalid.join(", ")
            ))),
        };
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            ListItemAdd => {
                let mut cond = Condition::new_fact(
                    self.areas.clone(),
                    self.interactive_entities.clone(),
                    None,
                );
                // trigger validate to check initial state
                cond.validate();
                self.conditions.push(cond);
            }
            ListItemDel(index) if index < self.conditions.len() => {
                self.conditions.remove(index);
            }
            ConditionListSetLogicType(op) => {
                self.logicop = op;
            }
            ListItemAction(index, action) => {
                if let Some(ref mut cond) = self.conditions.get_mut(index) {
                    cond.process_action(*action)?
                }
            }
            _ => unreachable!(
                "condition property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for NamedConditionSet {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
        self.conditions
            .iter_mut()
            .for_each(PropertyControl::reset_changed);
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.conditions.iter_mut().for_each(|c| {
            c.validate();
        });

        let invalid = self
            .conditions
            .iter()
            .enumerate()
            .filter(|(_, c)| !c.valid())
            .map(|(i, _)| format!("{}", i + 1))
            .collect::<Vec<_>>();

        self.error = match invalid.len() {
            0 => Ok(()),
            1 => Err(ImString::from(format!(
                "condition {} is invalid.",
                invalid.join("")
            ))),
            _ => Err(ImString::from(format!(
                "condition {} are invalid.",
                invalid.join(", ")
            ))),
        };
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            ListItemAction(index, action) => {
                if let Some(ref mut cond) = self.conditions.get_mut(index) {
                    cond.process_action(*action)?
                }
            }
            _ => unreachable!(
                "condition property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
impl<'cond> From<(Rc<AreaList>, &'cond model::EnteredLeftAreaCondition)> for AreaCondition {
    // ------------------------------------------------------------------------
    fn from(data: (Rc<AreaList>, &model::EnteredLeftAreaCondition)) -> AreaCondition {
        let (areas, cond) = data;
        let (_type, area_label) = if *cond.has_entered() {
            (AreaConditionType::Entered, "entered")
        } else {
            (AreaConditionType::Left, "left")
        };
        AreaCondition::new(
            _type,
            areas,
            cond.actor().map(String::as_str),
            area_label,
            cond.area(),
        )
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> From<(Rc<AreaList>, &'cond model::InsideOutsideAreaCondition)> for AreaCondition {
    // ------------------------------------------------------------------------
    fn from(data: (Rc<AreaList>, &model::InsideOutsideAreaCondition)) -> AreaCondition {
        let (areas, cond) = data;
        let (_type, area_label) = if *cond.is_inside() {
            (AreaConditionType::Inside, "inside")
        } else {
            (AreaConditionType::Outside, "outside")
        };
        AreaCondition::new(
            _type,
            areas,
            cond.actor().map(String::as_str),
            area_label,
            cond.area(),
        )
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> From<&'cond model::TimeCondition> for TimeCondition {
    // ------------------------------------------------------------------------
    fn from(cond: &model::TimeCondition) -> TimeCondition {
        use self::model::TimeCondition::*;

        let (label, time1, time2) = match cond {
            After(time) => ("after", format!("{}", time), None),
            Before(time) => ("before", format!("{}", time), None),
            Time(time) => ("between", format!("{}", time), Some(String::from(""))),
            TimePeriod(start, end) => ("between", format!("{}", start), Some(format!("{}", end))),
            Elapsed(time) => ("elapsed", format!("{}", time), None),
        };
        TimeCondition::new(TimeConditionType::from(cond), label, time1, time2)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> From<(Rc<InteractionList>, &'cond model::InteractionCondition)>
    for InteractionCondition
{
    // ------------------------------------------------------------------------
    fn from(data: (Rc<InteractionList>, &model::InteractionCondition)) -> InteractionCondition {
        use self::model::InteractionType::*;
        let (entities, cond) = data;

        let (label, custom) = match cond.interaction() {
            Examine => ("examined", None),
            Talk => ("talked to", None),
            Use => ("used", None),
            Loot => ("looted", None),
            Custom(custom) => ("entity", Some(custom.as_str())),
        };
        InteractionCondition::new(
            InteractionConditionType::from(cond),
            entities,
            label,
            cond.entity(),
            custom,
        )
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> From<&'cond model::LoadingScreenCondition> for LoadScreenCondition {
    // ------------------------------------------------------------------------
    fn from(cond: &model::LoadingScreenCondition) -> LoadScreenCondition {
        let _type = if *cond.is_shown() {
            LoadScreenConditionType::Shown
        } else {
            LoadScreenConditionType::Hidden
        };
        LoadScreenCondition {
            _type,
            changed: false,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'model> From<(Rc<AreaList>, Rc<InteractionList>, &'model model::Condition)> for Condition {
    // ------------------------------------------------------------------------
    fn from(data: (Rc<AreaList>, Rc<InteractionList>, &model::Condition)) -> Condition {
        use self::model::Condition::*;
        let (areas, entities, cond) = data;

        let mut control = match cond {
            FactsDb(ref cond) => Condition::new_fact(areas, entities, Some(cond)),
            Time(ref cond) => Condition::new_time(areas, entities, cond),
            EnteredLeftArea(ref cond) => Condition::new_entered_area(areas, entities, cond),
            InsideOutsideArea(ref cond) => Condition::new_inside_area(areas, entities, cond),
            Interaction(ref cond) => Condition::new_interaction(areas, entities, cond),
            LoadingScreen(_) => {
                unimplemented!("embedded loadscreen conditions in logic condition not supported")
            }
            Logic(_) => {
                unimplemented!("embedded logic conditions in logic condition not supported")
            }
        };
        control.validate();
        control
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> From<&'cond model::TimeCondition> for TimeConditionType {
    // ------------------------------------------------------------------------
    fn from(cond: &model::TimeCondition) -> TimeConditionType {
        use self::model::TimeCondition::*;

        match cond {
            After(_) => TimeConditionType::After,
            Before(_) => TimeConditionType::Before,
            Time(_) | TimePeriod(_, _) => TimeConditionType::Range,
            Elapsed(_) => TimeConditionType::Elapsed,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> From<&'cond model::InteractionCondition> for InteractionConditionType {
    // ------------------------------------------------------------------------
    fn from(cond: &model::InteractionCondition) -> InteractionConditionType {
        use self::model::InteractionType::*;

        match cond.interaction() {
            Examine => InteractionConditionType::Examined,
            Talk => InteractionConditionType::Talked,
            Use => InteractionConditionType::Used,
            Loot => InteractionConditionType::Looted,
            Custom(_) => InteractionConditionType::Custom,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> TryFrom<&'cond model::Condition> for NamedCondition {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(cond: &model::Condition) -> Result<Self, Self::Error> {
        use self::model::Condition::*;

        let name = cond.name();

        match cond {
            FactsDb(cond) => Ok(NamedCondition {
                socket: name.to_owned(),
                caption: ImString::new(format!("\"{}\"", name)),
                cond: FactCondition::new(Some(cond)),
            }),
            _ => Err(String::from(
                "unsupported condition type for named condition",
            )),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// to model converter (no semantic checks: converts if it *can* be converted)
// ----------------------------------------------------------------------------
use std::convert::{TryFrom, TryInto};

impl<'cond> From<&'cond FactCondition> for model::Condition {
    // ------------------------------------------------------------------------
    fn from(cond: &FactCondition) -> Self {
        model::Condition::FactsDb(model::FactsDbCondition::new(
            cond.factid.value().as_str().unwrap_or("").to_owned(),
            cond.compareop.clone(),
            cond.value.value().as_i32().unwrap_or(0),
        ))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> TryFrom<&'cond TimeCondition> for model::Condition {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(cond: &TimeCondition) -> Result<Self, Self::Error> {
        use self::TimeConditionType::*;

        let cond = if let Elapsed = cond._type {
            model::TimeCondition::Elapsed(model::HiResTime::try_from(cond.time1.value().as_str()?)?)
        } else {
            let time1 = model::Time::try_from(cond.time1.value().as_str()?)?;

            let time2 = if let Some(ref time) = cond.time2 {
                Some(model::Time::try_from(time.value().as_str()?)?)
            } else {
                None
            };

            match cond._type {
                After => model::TimeCondition::After(time1),
                Before => model::TimeCondition::Before(time1),
                Range if time2.is_some() => model::TimeCondition::TimePeriod(time1, time2.unwrap()),
                Range => model::TimeCondition::Time(time1),
                Elapsed => unreachable!(),
            }
        };

        Ok(model::Condition::Time(cond))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> From<&'cond InteractionCondition> for model::Condition {
    // ------------------------------------------------------------------------
    fn from(cond: &InteractionCondition) -> Self {
        use self::InteractionConditionType::*;

        let entity = model::InteractionReference::try_from(cond.entity.reference()).ok();

        let _type = match cond._type {
            Custom => model::InteractionType::Custom(
                cond.interaction
                    .as_ref()
                    .map(|field| field.value().as_str().unwrap_or(""))
                    .unwrap_or("")
                    .to_owned(),
            ),
            Examined => model::InteractionType::Examine,
            Talked => model::InteractionType::Talk,
            Used => model::InteractionType::Use,
            Looted => model::InteractionType::Loot,
        };

        model::Condition::Interaction(model::InteractionCondition::new(_type, entity))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> From<&'cond AreaCondition> for model::Condition {
    // ------------------------------------------------------------------------
    fn from(cond: &AreaCondition) -> Self {
        use self::model::Condition::*;
        use self::AreaConditionType::*;

        let actor = cond.actor.value().as_str().ok().map(ToOwned::to_owned);
        let area = model::LayerTagReference::try_from(cond.area.reference()).ok();

        match cond._type {
            Entered => EnteredLeftArea(model::EnteredLeftAreaCondition::new(actor, true, area)),
            Left => EnteredLeftArea(model::EnteredLeftAreaCondition::new(actor, false, area)),
            Inside => InsideOutsideArea(model::InsideOutsideAreaCondition::new(actor, true, area)),
            Outside => {
                InsideOutsideArea(model::InsideOutsideAreaCondition::new(actor, false, area))
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> From<&'cond LoadScreenCondition> for model::Condition {
    // ------------------------------------------------------------------------
    fn from(cond: &LoadScreenCondition) -> Self {
        use self::model::Condition::*;
        use self::LoadScreenConditionType::*;

        match cond._type {
            Shown => LoadingScreen(model::LoadingScreenCondition::new(true)),
            Hidden => LoadingScreen(model::LoadingScreenCondition::new(false)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> From<&'cond NamedCondition> for model::Condition {
    // ------------------------------------------------------------------------
    fn from(namedcond: &NamedCondition) -> Self {
        model::Condition::from(&namedcond.cond)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'cond> TryFrom<&'cond Condition> for model::Condition {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(cond: &Condition) -> Result<Self, Self::Error> {
        use self::ConditionType::*;

        match cond._type {
            Fact => {
                let cond = ctl_to_property!(cond.control, FactCondition, "##factcond");
                Ok(cond.into())
            }
            Area(_) => {
                let cond = ctl_to_property!(cond.control, AreaCondition, "##areacond");
                Ok(cond.into())
            }
            Time(_) => {
                let cond = ctl_to_property!(cond.control, TimeCondition, "##timecond");
                cond.try_into()
            }
            Interaction(_) => {
                let cond = ctl_to_property!(cond.control, InteractionCondition, "##interactcond");
                Ok(cond.into())
            }
            LoadScreen(_) => Err(String::from("embedded loadscreen conditions not supported")),
            Logic(_) => Err(String::from("embedded logic conditions not supported")),
            MultiOutFact => Err(String::from("multi out fact conditions not supported")),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
