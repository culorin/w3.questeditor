//
// properties::references::layer
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct LayerReference {
    pub(super) world: Rc<World>,
    pub(super) layer: Option<Layer>,
    pub(super) options: Rc<LayerList>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct LayerSet {
    pub(super) caption: ImString,
    pub(super) layers: Vec<LayerReference>,
    pub(super) world: Option<Rc<World>>,
    pub(super) options: Rc<LayerList>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct ChangeLayerControl {
    pub(super) label_width: f32,
    pub(super) caption: ImString,
    pub(super) world: Option<Rc<World>>,
    pub(super) show: Option<LayerSet>,
    pub(super) hide: Option<LayerSet>,
    pub(super) worlds: Vec<World>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(super) const MAX_LAYERS: usize = 20;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;
use std::rc::Rc;

use imgui::ImString;

// actions
use super::PropertyAction;

// misc
use super::PropertyControl;
use super::super::view;
use super::super::view::PropertyView;

use gui::registry::{Layer, LayerId, LayerList, World, WorldId};
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl ChangeLayerControl {
    // ------------------------------------------------------------------------
    pub fn new(
        layers: &Rc<LayerList>,
        world: Option<&String>,
        show: Option<&Vec<String>>,
        hide: Option<&Vec<String>>,
    ) -> ChangeLayerControl {
        let world = world.map(|world| Rc::new(World::new(WorldId::from(world))));

        ChangeLayerControl {
            label_width: view::LABEL_WIDTH,
            caption: ImString::new("world"),
            show: show.map(|list| LayerSet::new("layers to show", list, &world, Rc::clone(layers))),
            hide: hide.map(|list| LayerSet::new("layers to hide", list, &world, Rc::clone(layers))),
            world,
            worlds: layers.keys().cloned().collect::<Vec<_>>(),
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
    pub fn world(&self) -> Option<&WorldId> {
        self.world.as_ref().map(|world| world.id())
    }
    // ------------------------------------------------------------------------
    pub fn layers_to_show(&self) -> Vec<&LayerId> {
        self.show
            .as_ref()
            .map(LayerSet::layer_ids)
            .unwrap_or_default()
    }
    // ------------------------------------------------------------------------
    pub fn layers_to_hide(&self) -> Vec<&LayerId> {
        self.hide
            .as_ref()
            .map(LayerSet::layer_ids)
            .unwrap_or_default()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LayerReference {
    // ------------------------------------------------------------------------
    fn new(world: Rc<World>, layerid: Option<&String>, options: Rc<LayerList>) -> LayerReference {
        LayerReference {
            world,
            layer: layerid.map(Layer::new),
            options,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
    fn id(&self) -> Option<&LayerId> {
        use gui::lists;
        self.layer.as_ref().map(lists::UiOption::id)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LayerSet {
    // ------------------------------------------------------------------------
    fn new(
        label: &str,
        list: &[String],
        world: &Option<Rc<World>>,
        options: Rc<LayerList>,
    ) -> LayerSet {
        let layers = match world {
            Some(world) => list.iter()
                .map(|id| LayerReference::new(Rc::clone(world), Some(id), Rc::clone(&options)))
                .collect(),
            None => Vec::default(),
        };
        LayerSet {
            caption: ImString::new(label),
            layers,
            world: world.as_ref().map(Rc::clone),
            options,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
    fn set_world(&mut self, world: &Option<Rc<World>>) {
        self.world = world.as_ref().map(Rc::clone);
        self.changed = true;
        self.layers.clear();
    }
    // ------------------------------------------------------------------------
    fn layer_ids(&self) -> Vec<&LayerId> {
        self.layers
            .iter()
            .flat_map(LayerReference::id)
            .collect()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// PropertyControl impls
// ----------------------------------------------------------------------------
impl PropertyControl for ChangeLayerControl {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
        if let Some(layer_set) = self.show.as_mut() {
            layer_set.reset_changed();
        }
        if let Some(layer_set) = self.hide.as_mut() {
            layer_set.reset_changed();
        }
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        if let Some(layer_set) = self.show.as_mut() {
            layer_set.validate();
        }
        if let Some(layer_set) = self.hide.as_mut() {
            layer_set.validate();
        }
        self.error = match self.world {
            Some(_) => Ok(()),
            None => Err(ImString::new("world setting is missing")),
        };
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            SubfieldAction(0, action) => if let Some(layerset) = self.show.as_mut() {
                layerset.process_action(*action)?;
            },
            SubfieldAction(1, action) => if let Some(layerset) = self.hide.as_mut() {
                layerset.process_action(*action)?;
            },
            LayersSetWorld(id) => {
                self.world = Some(Rc::new(World::new(id)));
                if let Some(layerset) = self.show.as_mut() {
                    layerset.set_world(&self.world);
                }
                if let Some(layerset) = self.hide.as_mut() {
                    layerset.set_world(&self.world);
                }
            }
            _ => unreachable!("layer control received unsupported action {:?}", action),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for LayerSet {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
        self.layers
            .iter_mut()
            .for_each(PropertyControl::reset_changed);
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.layers.iter_mut().for_each(|l| {
            l.validate();
        });

        let invalid = self.layers
            .iter()
            .enumerate()
            .filter(|(_, l)| !l.valid())
            .map(|(i, _)| format!("{}", i + 1))
            .collect::<Vec<_>>();

        self.error = match invalid.len() {
            0 if self.layers.is_empty() => Err(ImString::new("must not be empty")),
            0 => Ok(()),
            1 => Err(ImString::from(format!(
                "layer {} is invalid.",
                invalid.join("")
            ))),
            _ => Err(ImString::from(format!(
                "layers {} are invalid.",
                invalid.join(", ")
            ))),
        };
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            ListItemAdd => {
                if let Some(world) = self.world.as_ref() {
                    let mut layer =
                        LayerReference::new(Rc::clone(world), None, Rc::clone(&self.options));
                    // trigger validate to check initial state
                    layer.validate();
                    self.layers.push(layer);
                }
            }
            ListItemDel(index) if index < self.layers.len() => {
                self.layers.remove(index);
            }
            ListItemAction(index, action) => {
                if let Some(ref mut layer) = self.layers.get_mut(index) {
                    layer.process_action(*action)?;
                    layer.validate();
                }
            }
            _ => unreachable!("layerset received unsupported action {:?}", action),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for LayerReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.error = if self.layer.is_none() {
            Err(ImString::new("must not be empty"))
        } else {
            Ok(())
        };

        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            LayersSetLayer(id) => self.layer = Some(Layer::new(id)),
            _ => unreachable!("layerreference received unsupported action {:?}", action),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
