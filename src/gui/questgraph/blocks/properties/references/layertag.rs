//
// properties::references::layertag
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
/// types of layer tag references
#[derive(Debug, PartialEq)]
pub(in gui) enum TagReferenceType {
    Unchecked,
    WorldAndTag,
    WorldAndId,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct TagReference {
    pub(super) r_type: TagReferenceType,
    pub(super) unchecked: input::TextField,
    pub(super) world: Option<World>,
    pub(super) tag: Option<Tag>,
    pub(super) options: Rc<TagList>,
    pub(super) required: bool,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;
use std::rc::Rc;

use imgui::ImString;
use imgui_controls::input;
use imgui_controls::input::validator;
use imgui_controls::input::Field;

// actions
use super::PropertyAction;

// misc
use super::model;
use super::{PropertyControl, Reference};

use gui::registry::{Tag, TagList, World};
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl TagReference {
    // ------------------------------------------------------------------------
    fn extract_info(
        reference: Option<&model::LayerTagReference>,
    ) -> (TagReferenceType, Option<World>, Option<Tag>, Option<&str>) {
        use self::model::LayerTagReference::*;

        if let Some(reference) = reference {
            match reference {
                UncheckedTag(tag) => (TagReferenceType::Unchecked, None, None, Some(tag)),
                WorldAndTag(world, tag) => (
                    TagReferenceType::WorldAndTag,
                    if world.is_empty() { None } else { Some(World::new(world)) },
                    if (*tag).is_empty() { None } else { Some(Tag::new(false, tag.to_string())) },
                    None,
                ),
                WorldAndId(world, tag) => (
                    TagReferenceType::WorldAndId,
                    if world.is_empty() { None } else { Some(World::new(world)) },
                    if tag.is_empty() { None } else { Some(Tag::new(true, tag))},
                    None,
                ),
            }
        } else {
            (TagReferenceType::WorldAndId, None, None, None)
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// PropertyControl impls
// ----------------------------------------------------------------------------
impl PropertyControl for TagReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.unchecked.reset_changed();
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        match self.r_type {
            TagReferenceType::Unchecked => {
                self.unchecked.validate();
            }
            _ if self.tag.is_none() && self.required => {
                self.error = Err(ImString::new("must not be empty"));
            }
            _ => {
                self.error = Ok(());
            }
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            ReferenceSetType(index) => {
                let r_type = TagReferenceType::from(index);
                if r_type != self.r_type {
                    self.world = None;
                    self.tag = None;
                    self.unchecked.reset();
                }
                self.r_type = r_type;
            }
            ReferenceSetTag(world, tag) => {
                let is_checked = match self.r_type {
                    TagReferenceType::WorldAndId => true,
                    TagReferenceType::WorldAndTag => false,
                    _ => {
                        warn!("received reference-set-tag action but type is unchecked");
                        // ignore situation
                        return Ok(());
                    }
                };
                self.world = Some(World::new(world));
                self.tag = Some(Tag::new(is_checked, tag));
            }
            Update(ref value) => {
                self.unchecked.validate().set_value(value.into())?;
            }
            _ => unreachable!(
                "reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// wrappable references
// ----------------------------------------------------------------------------
impl Reference for TagReference {
    // ------------------------------------------------------------------------
    fn error(&self) -> Result<&(), &ImString> {
        match self.r_type {
            TagReferenceType::Unchecked => self.unchecked.error(),
            _ => self.error.as_ref(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
impl<'model> From<(bool, Rc<TagList>, Option<&'model model::LayerTagReference>)> for TagReference {
    // ------------------------------------------------------------------------
    fn from(data: (bool, Rc<TagList>, Option<&'model model::LayerTagReference>)) -> TagReference {
        let (required, tagset, reference) = data;

        let (r_type, world, tag, unchecked) = Self::extract_info(reference);
        //TODO check if world and tag are valid (== available in tagset)

        let mut validators = vec![
            validator::chars("^[0-9_a-zA-Z]*$", "[a-zA-Z_0-9]"),
            validator::min_length(2),
            validator::max_length(250),
        ];
        if required {
            validators.push(validator::is_nonempty());
        }

        TagReference {
            r_type,
            unchecked: input::TextField::new("##unchecked", unchecked).set_validators(validators),
            world,
            tag,
            options: tagset,
            required,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
use std::convert::TryFrom;
// ----------------------------------------------------------------------------
impl<'ui> TryFrom<&'ui TagReference> for model::LayerTagReference {
    type Error = String;
    // ------------------------------------------------------------------------
    /// no semantic checks (min str len, etc.): converts if it *can* be converted
    fn try_from(reference: &TagReference) -> Result<Self, String> {
        use self::TagReferenceType::*;

        if let Unchecked = reference.r_type {
            let tag = reference.unchecked.value().as_str()?.to_owned();
            if tag.is_empty() {
                Err(String::from("empty unchecked tag"))
            } else {
                Ok(model::LayerTagReference::UncheckedTag(tag))
            }
        } else {
            let world = reference
                .world
                .as_ref()
                .map(|w| w.id().into())
                .ok_or_else(|| String::from("missing world"))?;
            let tag = reference
                .tag
                .as_ref()
                .map(|t| t.id().into())
                .ok_or_else(|| String::from("missing tag"))?;

            Ok(match reference.r_type {
                WorldAndId => model::LayerTagReference::WorldAndId(world, tag),
                WorldAndTag => {
                    model::LayerTagReference::WorldAndTag(world, model::CheckedTag::new(tag))
                }
                Unchecked => unreachable!(),
            })
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
