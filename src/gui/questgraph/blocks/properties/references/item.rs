//
// properties::references::item
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
/// types of layer tag references
#[derive(Debug, PartialEq)]
pub(in gui) enum RewardReferenceType {
    Id,
    Unchecked,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct RewardReference {
    pub(super) r_type: RewardReferenceType,
    pub(super) unchecked: input::TextField,
    pub(super) reward: Option<Reward>,
    pub(super) options: Rc<RewardList>,
    pub(super) required: bool,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;
use std::rc::Rc;

use imgui::ImString;
use imgui_controls::input;
use imgui_controls::input::validator;
use imgui_controls::input::Field;

// actions
use super::PropertyAction;

// misc
use super::model;
use super::{PropertyControl, Reference};

use gui::registry::{Reward, RewardList};
// ----------------------------------------------------------------------------
// PropertyControl impls
// ----------------------------------------------------------------------------
impl PropertyControl for RewardReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.unchecked.reset_changed();
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        match self.r_type {
            RewardReferenceType::Unchecked => {
                self.unchecked.validate();
            }
            _ if self.reward.is_none() && self.required => {
                self.error = Err(ImString::new("must not be empty"));
            }
            _ => {
                self.error = Ok(());
            }
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            RewardSetType(new_type) => {
                let r_type = new_type;
                if r_type != self.r_type {
                    self.reward = None;
                    self.unchecked.reset();
                }
                self.r_type = r_type;
            }
            RewardSetReward(reward) => {
                self.reward = Some(Reward::new(reward));
            }
            Update(ref value) => {
                self.unchecked.validate().set_value(value.into())?;
            }
            _ => unreachable!(
                "reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// wrappable references
// ----------------------------------------------------------------------------
impl Reference for RewardReference {
    // ------------------------------------------------------------------------
    fn error(&self) -> Result<&(), &ImString> {
        match self.r_type {
            RewardReferenceType::Unchecked => self.unchecked.error(),
            _ => self.error.as_ref(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
impl<'model> From<(bool, Rc<RewardList>, Option<&'model model::RewardReference>)>
    for RewardReference
{
    // ------------------------------------------------------------------------
    fn from(
        data: (bool, Rc<RewardList>, Option<&'model model::RewardReference>),
    ) -> RewardReference {
        use self::model::RewardReference::*;

        let (required, rewardlist, reference) = data;

        let (r_type, reward, unchecked) = match reference {
            Some(Unchecked(ref id)) => (RewardReferenceType::Unchecked, None, Some(id.to_owned())),
            Some(Id(ref id)) => (RewardReferenceType::Id, Some(Reward::new(id)), None),
            None => (RewardReferenceType::Id, None, None),
        };

        let mut validators = vec![
            validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
            validator::min_length(2),
            validator::max_length(250),
        ];
        if required {
            validators.push(validator::is_nonempty());
        }

        RewardReference {
            r_type,
            unchecked: input::TextField::new("##unchecked", unchecked).set_validators(validators),
            reward,
            options: rewardlist,
            required,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
use std::convert::TryFrom;
// ----------------------------------------------------------------------------
impl<'ui> TryFrom<&'ui RewardReference> for model::RewardReference {
    type Error = String;
    // ------------------------------------------------------------------------
    /// no semantic checks (min str len, etc.): converts if it *can* be converted
    fn try_from(reference: &RewardReference) -> Result<Self, String> {
        use self::RewardReferenceType::*;

        if let Unchecked = reference.r_type {
            let reward = reference.unchecked.value().as_str()?.to_owned();
            if reward.is_empty() {
                Err(String::from("empty unchecked reward"))
            } else {
                Ok(model::RewardReference::Unchecked(reward))
            }
        } else {
            let reward = reference
                .reward
                .as_ref()
                .map(|r| r.id().into())
                .ok_or_else(|| String::from("missing reward"))?;

            Ok(match reference.r_type {
                Id => model::RewardReference::Id(reward),
                Unchecked => unreachable!(),
            })
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
