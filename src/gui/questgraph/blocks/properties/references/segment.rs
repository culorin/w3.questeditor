//
// properties::references::segment
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct SegmentReference {
    pub(super) required: bool,
    pub(super) segment: Option<SegmentRef>,
    pub(super) options: Rc<SegmentList>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;
use std::rc::Rc;

use imgui::ImString;

// actions
use super::PropertyAction;

// misc
use super::model;
use super::{PropertyControl, Reference};

use gui::questgraph::{SegmentList, SegmentRef};
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// PropertyControl impls
// ----------------------------------------------------------------------------
impl PropertyControl for SegmentReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.error = if self.segment.is_none() && self.required {
            Err(ImString::new("must not be empty"))
        } else {
            Ok(())
        };
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            ReferenceSetSubsegment(id) => {
                self.segment = Some(SegmentRef::new(id));
            }
            _ => unreachable!(
                "reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// wrappable references
// ----------------------------------------------------------------------------
impl Reference for SegmentReference {
    // ------------------------------------------------------------------------
    fn error(&self) -> Result<&(), &ImString> {
        self.error.as_ref()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
impl<'model> From<(bool, Rc<SegmentList>, Option<&'model model::SegmentId>)> for SegmentReference {
    // ------------------------------------------------------------------------
    fn from(data: (bool, Rc<SegmentList>, Option<&'model model::SegmentId>)) -> SegmentReference {
        let (required, segments, segmentlink) = data;

        SegmentReference {
            segment: segmentlink
                .as_ref()
                .map(|id| SegmentRef::new((**id).clone())),
            required,
            options: segments,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
use std::convert::TryFrom;
// ----------------------------------------------------------------------------
impl<'ui> TryFrom<&'ui SegmentReference> for model::SegmentId {
    type Error = String;
    // ------------------------------------------------------------------------
    /// no semantic checks (min str len, etc.): converts if it *can* be converted
    fn try_from(reference: &SegmentReference) -> Result<Self, String> {
        match reference.segment {
            Some(ref selected) => Ok(selected.id().clone()),
            None => Err(String::from("missing segmentid")),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
