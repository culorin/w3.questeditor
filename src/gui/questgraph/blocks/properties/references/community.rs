//
// properties::references::community
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
/// types of community references
#[derive(Debug, PartialEq)]
pub(in gui) enum CommunityReferenceType {
    Id,
    Unchecked,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct CommunityReference {
    pub(super) r_type: CommunityReferenceType,
    pub(super) unchecked: input::TextField,
    pub(super) phase: Option<Rc<Phase>>,
    pub(super) spawnset: Option<Community>,
    pub(super) options: Rc<CommunityOptions>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) enum CommunityOptions {
    All(Rc<CommunityList>),
    ByPhase(Rc<CommunityList>, Rc<CommunityPhaseList>),
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct CommunitySet {
    pub(super) label_width: f32,
    pub(super) caption: ImString,
    pub(super) spawnsets: Vec<CommunityReference>,
    pub(super) phase: Option<Rc<Phase>>,
    pub(super) options: Rc<CommunityOptions>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(super) const MAX_SPAWNSETS: usize = 20;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;
use std::rc::Rc;

use imgui::ImString;
use imgui_controls::input;
use imgui_controls::input::validator;
use imgui_controls::input::Field;

// actions
use super::PropertyAction;

// misc
use super::super::view;
use super::super::view::PropertyView;
use super::PropertyControl;

use gui::questgraph::CommunityPhaseList;
use gui::registry::{Community, CommunityList, Phase, PhaseId};
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl CommunityReference {
    // ------------------------------------------------------------------------
    fn new(
        spawnsetid: Option<&str>,
        phase: Option<&Rc<Phase>>,
        options: &Rc<CommunityOptions>,
    ) -> CommunityReference {
        use self::CommunityReferenceType::*;

        let (r_type, spawnset, unchecked) = match spawnsetid {
            Some(id) if id.ends_with(".w2comm") => (Unchecked, None, Some(id)),
            Some(id) => (Id, Some(Community::new(id)), None),
            None => (Id, None, None),
        };

        CommunityReference {
            r_type,
            unchecked: input::TextField::new("##unchecked", unchecked).set_validators(vec![
                validator::is_nonempty(),
                validator::chars("^[0-9/\\\\._a-z]*$", "[a-z/._0-9]"),
                validator::min_length(2),
                validator::max_length(250),
            ]),
            options: Rc::clone(options),
            phase: phase.map(Rc::clone),
            spawnset,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
    fn set_phase(&mut self, phase: &Rc<Phase>) -> bool {
        use self::CommunityOptions::*;
        use self::CommunityReferenceType::*;

        match (&*self.options, &self.r_type) {
            (ByPhase(_, _), Unchecked) => true,
            (ByPhase(_, ref by_phase), Id) => match (&self.spawnset, by_phase.get(&**phase)) {
                (Some(spawnset), Some(valid_sets)) if valid_sets.contains(spawnset) => {
                    self.phase = Some(Rc::clone(phase));
                    true
                }
                _ => false,
            },
            _ => false,
        }
    }
    // ------------------------------------------------------------------------
    fn id(&self) -> Option<&str> {
        use self::CommunityReferenceType::*;

        match self.r_type {
            Id => self.spawnset.as_ref().map(|c| c.id().into()),
            Unchecked => {
                // TODO
                // unfortunately it's not a type wrapped reference (atm)
                // -> return only if it ends with w2comm to distinguish
                match self.unchecked.value().as_str() {
                    Ok(reference) if reference.ends_with(".w2comm") => Some(reference),
                    _ => None,
                }
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl CommunitySet {
    // ------------------------------------------------------------------------
    pub fn new<'list, T: Iterator<Item = &'list str>>(
        caption: &str,
        options: CommunityOptions,
        phase: Option<&String>,
        list: T,
    ) -> CommunitySet {
        let options = Rc::new(options);
        let phase = phase
            .and_then(|phase| if phase.is_empty() { None } else { Some(phase) })
            .map(|phase| Rc::new(Phase::new(PhaseId::from(phase))));

        CommunitySet {
            label_width: view::LABEL_WIDTH,
            caption: ImString::new(caption),
            spawnsets: list.map(|id| CommunityReference::new(Some(id), phase.as_ref(), &options))
                .collect(),
            phase,
            options,
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
    pub fn phase(&self) -> Option<&PhaseId> {
        self.phase.as_ref().map(|phase| phase.id())
    }
    // ------------------------------------------------------------------------
    pub(in gui) fn spawnsets(&self) -> impl Iterator<Item = &str> {
        self.spawnsets.iter().flat_map(CommunityReference::id)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// PropertyControl impls
// ----------------------------------------------------------------------------
impl PropertyControl for CommunitySet {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
        self.spawnsets
            .iter_mut()
            .for_each(PropertyControl::reset_changed);
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.spawnsets.iter_mut().for_each(|set| {
            set.validate();
        });

        let invalid = self.spawnsets
            .iter()
            .enumerate()
            .filter(|(_, set)| !set.valid())
            .map(|(i, _)| format!("{}", i + 1))
            .collect::<Vec<_>>();

        self.error = match invalid.len() {
            0 if self.spawnsets.is_empty() => Err(ImString::new("no spawnset defined")),
            0 => Ok(()),
            1 => Err(ImString::from(format!(
                "spawnset {} is invalid.",
                invalid.join("")
            ))),
            _ => Err(ImString::from(format!(
                "spawnsets {} are invalid.",
                invalid.join(", ")
            ))),
        };
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        use gui::lists::AsStr;
        self.changed = true;

        match action {
            ListItemAdd => {
                let mut spawnset =
                    CommunityReference::new(None, self.phase.as_ref(), &self.options);
                // trigger validate to check initial state
                spawnset.validate();
                self.spawnsets.push(spawnset);
            }
            ListItemDel(index) if index < self.spawnsets.len() => {
                self.spawnsets.remove(index);
            }
            ListItemAction(index, action) => {
                if let Some(ref mut spawnset) = self.spawnsets.get_mut(index) {
                    spawnset.process_action(*action)?;
                    spawnset.validate();
                }
            }
            CommunitySetPhase(phase) => {
                let phase = Rc::new(Phase::new(phase));

                let mut new_collection = vec![];
                for mut set in self.spawnsets.drain(..) {
                    if set.set_phase(&phase) {
                        new_collection.push(set);
                    }
                }
                self.spawnsets = new_collection;
                self.phase = Some(phase);
            }
            CommunitySetPhaseAndCommunity(phase, community) => {
                self.phase = Some(Rc::new(Phase::new(phase)));
                self.spawnsets = vec![CommunityReference::new(
                    Some(community.as_str()),
                    self.phase.as_ref(),
                    &self.options,
                )];
            }
            _ => unreachable!("communityset received unsupported action {:?}", action),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for CommunityReference {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.unchecked.reset_changed();
        self.changed = false;
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        match self.r_type {
            CommunityReferenceType::Unchecked => {
                if self.unchecked.validate().valid() {
                    // additional check if scene ref matches type constraints
                    let community = self.unchecked.value().as_str().unwrap();
                    if !community.ends_with(".w2comm") {
                        self.unchecked
                            .set_error_msg("w2comm filepath must end with .w2comm".to_owned());
                    }
                }
            }
            _ if self.spawnset.is_none() => {
                self.error = Err(ImString::new("community id missing"));
            }
            _ => {
                self.error = Ok(());
            }
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            CommunitySetType(new_type) => {
                let r_type = new_type;
                if r_type != self.r_type {
                    self.spawnset = None;
                    self.unchecked.reset();
                }
                self.r_type = r_type;
            }
            CommunitySetCommunity(id) => {
                self.spawnset = Some(Community::new(id));
            }
            Update(ref value) => {
                self.unchecked.validate().set_value(value.into())?;
            }
            _ => unreachable!(
                "community reference property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
