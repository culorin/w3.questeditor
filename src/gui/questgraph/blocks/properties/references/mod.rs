//
// blocks::properties::references
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui) trait Reference: PropertyView + PropertyControl + 'static {
    // ------------------------------------------------------------------------
    fn error(&self) -> Result<&(), &ImString>;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) struct LabeledReference<Ref> {
    pub(super) label_width: f32,
    pub(super) label: ImString,
    pub(super) reference: Ref,
}
// ----------------------------------------------------------------------------
pub(in gui::questgraph::blocks) use self::community::{
    CommunityOptions, CommunityReference, CommunityReferenceType, CommunitySet,
};
pub(in gui::questgraph::blocks) use self::item::{RewardReference, RewardReferenceType};
pub(in gui::questgraph::blocks) use self::journal::{
    JournalMapPinReference, JournalObjectiveReference, JournalPhaseObjectivesReference,
    JournalQuestOutcomeReference, JournalReference, JournalReferenceType,
};
pub(in gui::questgraph::blocks) use self::layer::{ChangeLayerControl, LayerReference, LayerSet};
pub(in gui::questgraph::blocks) use self::layertag::{TagReference, TagReferenceType};
pub(in gui::questgraph::blocks) use self::scene::{
    InteractionEntityType, InteractionReference, SceneReference, SceneReferenceType,
};
pub(in gui::questgraph::blocks) use self::segment::SegmentReference;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;
use std::rc::Rc;

use imgui::ImString;

// actions
use super::PropertyAction;

// misc
use super::view::PropertyView;
use super::PropertyControl;
// ----------------------------------------------------------------------------
mod model {
    pub use model::primitives::references::*;
    pub use model::questgraph::journals::JournalElementReference;
    pub use model::questgraph::misc::{Despawnset, Spawnset};
    pub use model::questgraph::SegmentId;
}
mod community;
mod item;
mod journal;
mod layer;
mod layertag;
mod scene;
mod segment;

mod view;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl<Ref> LabeledReference<Ref> {
    // ------------------------------------------------------------------------
    pub fn new<'src, Options, SrcRef>(
        label_width: f32,
        label: &str,
        options: Rc<Options>,
        required: bool,
        reference: Option<&'src SrcRef>,
    ) -> LabeledReference<Ref>
    where
        Ref: From<(bool, Rc<Options>, Option<&'src SrcRef>)>,
    {
        LabeledReference {
            label_width,
            label: ImString::new(label),
            reference: Ref::from((required, options, reference)),
        }
    }
    // ------------------------------------------------------------------------
    pub fn reference(&self) -> &Ref {
        &self.reference
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// PropertyControl impls
// ----------------------------------------------------------------------------
impl<Ref: Reference> PropertyControl for LabeledReference<Ref> {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.reference.reset_changed();
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        self.reference.validate();
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        self.reference.process_action(action)
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
/// internal helper trait
trait AsStr {
    fn as_str(&self) -> &str;
}
// ----------------------------------------------------------------------------
