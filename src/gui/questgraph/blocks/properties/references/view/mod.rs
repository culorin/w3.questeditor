//
// properties::references::view
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::{ImString, Ui};
use imgui_controls::input::Field;

use gui::lists::AsCaption;

use super::PropertyView;

// actions
use super::PropertyAction;

// state
use super::{
    ChangeLayerControl, CommunityOptions, CommunityReference, CommunityReferenceType, CommunitySet,
    InteractionEntityType, InteractionReference, JournalMapPinReference, JournalObjectiveReference,
    JournalPhaseObjectivesReference, JournalQuestOutcomeReference, JournalReference,
    JournalReferenceType, LabeledReference, LayerReference, LayerSet, Reference, RewardReference,
    RewardReferenceType, SceneReference, SceneReferenceType, SegmentReference, TagReference,
    TagReferenceType,
};

// misc
use super::super::view;
// ----------------------------------------------------------------------------
impl<Ref: Reference> PropertyView for LabeledReference<Ref> {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.reference.valid()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.reference.changed()
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        view::draw_colored_label(ui, &self.label, self.reference.error());

        let mut result = None;
        ui.same_line(self.label_width);

        ui.with_region_height(im_str!("##labeled_ref"), 1.0, || {
            result = self.reference.draw(ui);
        });
        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for TagReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        match self.r_type {
            TagReferenceType::Unchecked => self.unchecked.valid(),
            _ => !self.required || (self.world.is_some() && self.tag.is_some()),
        }
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        use gui::registry::{Tag, World};

        let mut result = None;

        let t_type_width = 22.0;
        let world_width = 75.0;

        if let Some(id) =
            view::draw_type_selection(ui, &self.r_type, im_str!("##rtype_popup"), None)
        {
            result = Some(PropertyAction::ReferenceSetType(id));
        }

        if let TagReferenceType::Unchecked = self.r_type {
            ui.same_line(t_type_width - 4.0);

            if let Some(action) = self.unchecked.draw(ui) {
                result = Some(PropertyAction::Update(action.into()));
            }
        } else {
            ui.same_line(t_type_width);

            view::draw_popup_selectable(
                ui,
                self.world.as_ref(),
                world_width,
                im_str!("##world_popup"),
            );

            ui.same_line(t_type_width + 4.0 + world_width);
            ui.text(im_str!("/"));
            ui.same_line(t_type_width + 4.0 + world_width + 10.0);

            if self.world.is_some() {
                view::draw_popup_selectable(ui, self.tag.as_ref(), 0.0, im_str!("##tag_popup"));
            } else {
                ui.new_line();
            }

            let checked = matches!(self.r_type, TagReferenceType::WorldAndId);

            let mut draw_tag_items = |world: &World, tags: &Vec<Tag>| {
                for tag in tags.iter().filter(|t| t.checked() == checked) {
                    if ui.menu_item(tag.caption()).build() {
                        result = Some(PropertyAction::ReferenceSetTag(
                            world.id().clone(),
                            tag.id().clone(),
                        ));
                    }
                }
            };

            ui.popup(im_str!("##world_popup"), || {
                let mut has_entries = false;
                for (world, tags) in self.options.iter() {
                    if tags.iter().any(|t| t.checked() == checked) {
                        ui.menu(world.caption())
                            .build(|| draw_tag_items(world, tags));
                        has_entries = true;
                    }
                }
                if !has_entries {
                    ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
                }
            });

            ui.popup(im_str!("##tag_popup"), || {
                if let Some(ref world) = self.world {
                    let mut has_entries = false;
                    if let Some(tags) = self.options.get(world) {
                        draw_tag_items(world, tags);
                        has_entries = tags.iter().any(|t| t.checked() == checked);
                    }
                    if !has_entries {
                        ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
                    }
                }
            });
        }
        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for InteractionReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        match self.r_type {
            InteractionEntityType::Unchecked => self.unchecked.valid(),
            _ => (self.world.is_some() && self.entity.is_some()),
        }
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        use gui::registry::{InteractionEntity, World};

        let mut result = None;

        let t_type_width = 22.0;
        let world_width = 75.0;

        if let Some(id) =
            view::draw_type_selection(ui, &self.r_type, im_str!("##rtype_popup"), None)
        {
            result = Some(PropertyAction::ReferenceSetInteractionEntityType(id));
        }

        if let InteractionEntityType::Unchecked = self.r_type {
            ui.same_line(t_type_width - 4.0);

            if let Some(action) = self.unchecked.draw(ui) {
                result = Some(PropertyAction::Update(action.into()));
            }
        } else {
            ui.same_line(t_type_width);

            view::draw_popup_selectable(
                ui,
                self.world.as_ref(),
                world_width,
                im_str!("##world_popup"),
            );

            ui.same_line(t_type_width + 4.0 + world_width);
            ui.text(im_str!("/"));
            ui.same_line(t_type_width + 4.0 + world_width + 10.0);

            if self.world.is_some() {
                view::draw_popup_selectable(
                    ui,
                    self.entity.as_ref(),
                    0.0,
                    im_str!("##entity_popup"),
                );
            } else {
                ui.new_line();
            }

            let mut draw_items = |world: &World, items: &Vec<InteractionEntity>| {
                for item in items {
                    if ui.menu_item(item.caption()).build() {
                        result = Some(PropertyAction::ReferenceSetInteractionEntity(
                            world.id().clone(),
                            item.id().clone(),
                        ));
                    }
                }
            };

            ui.popup(im_str!("##world_popup"), || {
                let mut has_entries = false;
                for (world, items) in self.options.iter() {
                    if !items.is_empty() {
                        ui.menu(world.caption()).build(|| draw_items(world, items));
                        has_entries = true;
                    }
                }
                if !has_entries {
                    ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
                }
            });

            ui.popup(im_str!("##entity_popup"), || {
                if let Some(ref world) = self.world {
                    if let Some(items) = self.options.get(world) {
                        draw_items(world, items);
                    } else {
                        ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
                    }
                }
            });
        }
        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for SceneReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.scene.valid()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed || self.scene.changed()
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;
        let s_type_width = 18.0;

        ui.columns(3, im_str!("scene_ref"), false);

        ui.set_column_width(0, self.label_width);
        ui.set_column_width(1, s_type_width);

        view::draw_colored_label(ui, im_str!("scene"), self.scene.error());

        ui.with_style_var(::imgui::StyleVar::ItemSpacing((0.0, 4.0).into()), || {
            ui.next_column();

            if let Some(id) =
                view::draw_type_selection(ui, &self.s_type, im_str!("##rtype_popup"), None)
            {
                result = Some(PropertyAction::ReferenceSetType(id));
            }

            ui.next_column();

            if let Some(action) = self.scene.draw(ui) {
                result = Some(PropertyAction::Update(action.into()));
            }
        });
        ui.columns(1, im_str!("scene_ref"), false);

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// journals
// ----------------------------------------------------------------------------
impl PropertyView for JournalReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.name.is_some() && self.entry.is_some()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        use self::JournalReferenceType::*;
        use gui::registry::{JournalEntry, JournalGroup};

        view::draw_colored_label(ui, &self.label, self.error.as_ref());
        ui.same_line(self.label_width);

        let mut result = None;

        let t_type_width = 22.0;

        if let Some(id) =
            view::draw_type_selection(ui, &self.r_type, im_str!("##rtype_popup"), None)
        {
            result = Some(PropertyAction::ReferenceSetType(id));
        }

        ui.same_line(self.label_width + t_type_width);

        view::draw_popup_selectable(
            ui,
            self.name.as_ref(),
            0.0,
            im_str!("##journal_group_popup"),
        );

        view::draw_colored_label(ui, im_str!("entry"), self.error.as_ref());
        ui.same_line(self.label_width + t_type_width);

        if self.name.is_some() {
            view::draw_popup_selectable(
                ui,
                self.entry.as_ref(),
                0.0,
                im_str!("##journal_entry_popup"),
            );
        } else {
            ui.new_line();
        }

        let list = match self.r_type {
            Character => self.options.characters(),
            Creature => self.options.creatures(),
            Quest => self.options.quest_descriptions(),
        };

        let mut draw_menu_items = |name: &JournalGroup, entries: &Vec<JournalEntry>| {
            for entry in entries.iter() {
                if ui.menu_item(entry.caption()).build() {
                    result = Some(PropertyAction::JournalSetEntry(
                        name.id().clone(),
                        entry.id().clone(),
                    ));
                }
            }
        };

        ui.popup(im_str!("##journal_group_popup"), || {
            let mut has_entries = false;
            for (name, entries) in list.iter() {
                if !entries.is_empty() {
                    ui.menu(name.caption())
                        .build(|| draw_menu_items(name, entries));
                    has_entries = true;
                }
            }
            if !has_entries {
                ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
            }
        });

        ui.popup(im_str!("##journal_entry_popup"), || {
            if let Some(ref name) = self.name {
                if let Some(entries) = list.get(name) {
                    draw_menu_items(name, entries);
                } else {
                    ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
                }
            }
        });

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for JournalQuestOutcomeReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.quest.is_some()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        view::draw_colored_label(ui, im_str!("quest"), self.error.as_ref());
        ui.same_line(self.label_width);
        view::draw_popup_selectable(ui, self.quest.as_ref(), 0.0, im_str!("##quest_popup"));

        let list = &self.options;

        ui.popup(im_str!("##quest_popup"), || {
            ui.text(im_str!("select quest..."));
            ui.separator();
            for (quest, _) in list.iter() {
                if ui.menu_item(quest.caption()).build() {
                    result = Some(PropertyAction::JournalSetQuest(quest.id().clone()));
                }
            }
            if list.is_empty() {
                ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
            }
        });

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for JournalPhaseObjectivesReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.quest.is_some() && self.phase.is_some()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        use gui::registry::{JournalQuest, JournalQuestPhaseList};

        let mut result = None;

        view::draw_colored_label(ui, im_str!("quest"), self.error.as_ref());
        ui.same_line(self.label_width);
        view::draw_popup_selectable(ui, self.quest.as_ref(), 0.0, im_str!("##quest_popup"));

        view::draw_colored_label(ui, im_str!("phase"), self.error.as_ref());
        ui.same_line(self.label_width);
        if self.quest.is_some() {
            view::draw_popup_selectable(ui, self.phase.as_ref(), 0.0, im_str!("##phase_popup"));
        } else {
            ui.new_line();
        }

        let list = &self.options;

        let mut draw_quest_phases = |quest: &JournalQuest, phases: &JournalQuestPhaseList| {
            ui.text_colored(view::COL_INACTIVE, im_str!("quest phases       "));
            ui.separator();
            for (phase, _) in phases.iter() {
                if ui.menu_item(phase.caption()).build() {
                    result = Some(PropertyAction::JournalSetPhase(
                        quest.id().clone(),
                        phase.id().clone(),
                    ));
                };
            }
        };

        ui.popup(im_str!("##quest_popup"), || {
            ui.text(im_str!("select quest phase..."));
            ui.separator();
            for (quest, phases) in list.iter() {
                ui.menu(quest.caption()).build(|| {
                    draw_quest_phases(quest, phases);
                });
            }
            if list.is_empty() {
                ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
            }
        });

        if let Some(ref quest) = self.quest {
            ui.popup(im_str!("##phase_popup"), || {
                if let Some(phases) = list.get(quest) {
                    draw_quest_phases(quest, phases);
                }
            });
        }

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for JournalObjectiveReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.quest.is_some() && self.phase.is_some() && self.objective.is_some()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        use gui::registry::{
            JournalPhase, JournalQuest, JournalQuestObjectiveList, JournalQuestPhaseList,
        };

        let mut result = None;

        view::draw_colored_label(ui, im_str!("quest"), self.error.as_ref());
        ui.same_line(self.label_width);
        view::draw_popup_selectable(ui, self.quest.as_ref(), 0.0, im_str!("##quest_popup"));

        view::draw_colored_label(ui, im_str!("phase"), self.error.as_ref());
        ui.same_line(self.label_width);
        if self.quest.is_some() {
            view::draw_popup_selectable(ui, self.phase.as_ref(), 0.0, im_str!("##phase_popup"));
        } else {
            ui.new_line();
        }

        view::draw_colored_label(ui, im_str!("objective"), self.error.as_ref());
        ui.same_line(self.label_width);
        if self.phase.is_some() {
            view::draw_popup_selectable(ui, self.objective.as_ref(), 0.0, im_str!("##obj_popup"));
        } else {
            ui.new_line();
        }

        let list = &self.options;

        let mut draw_quest_objectives =
            |quest: &JournalQuest, phase: &JournalPhase, objectives: &JournalQuestObjectiveList| {
                ui.text_colored(view::COL_INACTIVE, im_str!("phase objectives   "));
                ui.separator();
                for objective in objectives.keys() {
                    if ui.menu_item(objective.caption()).build() {
                        result = Some(PropertyAction::JournalSetObjective(
                            quest.id().clone(),
                            phase.id().clone(),
                            objective.id().clone(),
                        ));
                    };
                }
            };

        let mut draw_quest_phases = |quest: &JournalQuest, phases: &JournalQuestPhaseList| {
            ui.text_colored(view::COL_INACTIVE, im_str!("quest phases       "));
            ui.separator();
            for (phase, objectives) in phases.iter() {
                ui.menu(phase.caption()).build(|| {
                    draw_quest_objectives(quest, phase, objectives);
                });
            }
        };

        ui.popup(im_str!("##quest_popup"), || {
            ui.text(im_str!("select quest objective..."));
            ui.separator();
            for (quest, phases) in list.iter() {
                ui.menu(quest.caption()).build(|| {
                    draw_quest_phases(quest, phases);
                });
            }
            if list.is_empty() {
                ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
            }
        });

        if let Some(ref quest) = self.quest {
            ui.popup(im_str!("##phase_popup"), || {
                if let Some(phases) = list.get(quest) {
                    draw_quest_phases(quest, phases);
                }
            });
        }

        if let (Some(quest), Some(phase)) = (self.quest.as_ref(), self.phase.as_ref()) {
            ui.popup(im_str!("##obj_popup"), || {
                if let Some(objectives) = list.get(quest).and_then(|phases| phases.get(phase)) {
                    draw_quest_objectives(quest, phase, objectives);
                }
            });
        }

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for JournalMapPinReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.quest.is_some()
            && self.phase.is_some()
            && self.objective.is_some()
            && self.mappin.is_some()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        use gui::registry::{
            JournalObjective, JournalPhase, JournalQuest, JournalQuestMapPinList,
            JournalQuestObjectiveList, JournalQuestPhaseList,
        };

        let mut result = None;

        view::draw_colored_label(ui, im_str!("quest"), self.error.as_ref());
        ui.same_line(self.label_width);
        view::draw_popup_selectable(ui, self.quest.as_ref(), 0.0, im_str!("##quest_popup"));

        view::draw_colored_label(ui, im_str!("phase"), self.error.as_ref());
        ui.same_line(self.label_width);
        if self.quest.is_some() {
            view::draw_popup_selectable(ui, self.phase.as_ref(), 0.0, im_str!("##phase_popup"));
        } else {
            ui.new_line();
        }

        view::draw_colored_label(ui, im_str!("objective"), self.error.as_ref());
        ui.same_line(self.label_width);
        if self.phase.is_some() {
            view::draw_popup_selectable(ui, self.objective.as_ref(), 0.0, im_str!("##obj_popup"));
        } else {
            ui.new_line();
        }

        view::draw_colored_label(ui, im_str!("map-pin"), self.error.as_ref());
        ui.same_line(self.label_width);
        if self.objective.is_some() {
            view::draw_popup_selectable(ui, self.mappin.as_ref(), 0.0, im_str!("##mappin_popup"));
        } else {
            ui.new_line();
        }

        let list = &self.options;

        let mut draw_quest_mappins =
            |quest: &JournalQuest,
             phase: &JournalPhase,
             objective: &JournalObjective,
             mappins: &JournalQuestMapPinList| {
                ui.text_colored(view::COL_INACTIVE, im_str!("objective map-pins "));
                ui.separator();
                for mappin in mappins {
                    if ui.menu_item(mappin.caption()).build() {
                        result = Some(PropertyAction::JournalSetMapPin(
                            quest.id().clone(),
                            phase.id().clone(),
                            objective.id().clone(),
                            mappin.id().clone(),
                        ));
                    }
                }
                if mappins.is_empty() {
                    ui.text_colored(view::COL_INACTIVE, im_str!("none defined"));
                }
            };

        let mut draw_quest_objectives =
            |quest: &JournalQuest, phase: &JournalPhase, objectives: &JournalQuestObjectiveList| {
                ui.text_colored(view::COL_INACTIVE, im_str!("phase objectives   "));
                ui.separator();
                for (objective, mappins) in objectives.iter() {
                    ui.menu(objective.caption()).build(|| {
                        draw_quest_mappins(quest, phase, objective, mappins);
                    });
                }
            };

        let mut draw_quest_phases = |quest: &JournalQuest, phases: &JournalQuestPhaseList| {
            ui.text_colored(view::COL_INACTIVE, im_str!("quest phases       "));
            ui.separator();
            for (phase, objectives) in phases.iter() {
                ui.menu(phase.caption()).build(|| {
                    draw_quest_objectives(quest, phase, objectives);
                });
            }
        };

        ui.popup(im_str!("##quest_popup"), || {
            ui.text(im_str!("select quest map-pin..."));
            ui.separator();
            for (quest, phases) in list.iter() {
                ui.menu(quest.caption()).build(|| {
                    draw_quest_phases(quest, phases);
                });
            }
            if list.is_empty() {
                ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
            }
        });

        if let Some(ref quest) = self.quest {
            ui.popup(im_str!("##phase_popup"), || {
                if let Some(phases) = list.get(quest) {
                    draw_quest_phases(quest, phases);
                }
            });
        }

        if let (Some(quest), Some(phase)) = (self.quest.as_ref(), self.phase.as_ref()) {
            ui.popup(im_str!("##obj_popup"), || {
                if let Some(objectives) = list.get(quest).and_then(|phases| phases.get(phase)) {
                    draw_quest_objectives(quest, phase, objectives);
                }
            });
        }

        if let (Some(quest), Some(phase), Some(objective)) = (
            self.quest.as_ref(),
            self.phase.as_ref(),
            self.objective.as_ref(),
        ) {
            ui.popup(im_str!("##mappin_popup"), || {
                if let Some(mappins) = list
                    .get(quest)
                    .and_then(|phases| phases.get(phase))
                    .and_then(|objectives| objectives.get(objective))
                {
                    draw_quest_mappins(quest, phase, objective, mappins);
                }
            });
        }

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// subsegment
// ----------------------------------------------------------------------------
impl PropertyView for SegmentReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.segment.is_some()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        if let Some(id) = view::draw_popup_list(
            ui,
            self.segment.as_ref(),
            Some(im_str!("select subsegment...")),
            &*self.options,
            0.0,
            im_str!("##segment_popup"),
        ) {
            result = Some(PropertyAction::ReferenceSetSubsegment(id.to_owned()));
        }

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Layers
// ----------------------------------------------------------------------------
impl PropertyView for ChangeLayerControl {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.world.is_some()
            && self.show.as_ref().map(PropertyView::valid).unwrap_or(true)
            && self.hide.as_ref().map(PropertyView::valid).unwrap_or(true)
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
            || self.show.as_ref().map(PropertyView::changed).unwrap_or(false)
            || self.hide.as_ref().map(PropertyView::changed).unwrap_or(false)
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        view::draw_colored_label(ui, &self.caption, self.error.as_ref());

        ui.same_line(self.label_width);
        if let Some(id) = view::draw_popup_list(
            ui,
            self.world.as_deref(),
            None,
            &self.worlds,
            0.0,
            im_str!("##world_popup"),
        ) {
            result = Some(PropertyAction::LayersSetWorld(id.clone()));
        }
        ui.spacing();

        if let Some(layerset) = self.show.as_mut() {
            ui.with_id(0, || {
                if let Some(action) = layerset.draw(ui) {
                    result = Some(PropertyAction::SubfieldAction(0, Box::new(action)));
                }
            });
            ui.spacing();
        }

        if let Some(layerset) = self.hide.as_mut() {
            ui.with_id(1, || {
                if let Some(action) = layerset.draw(ui) {
                    result = Some(PropertyAction::SubfieldAction(1, Box::new(action)));
                }
            });
            ui.spacing();
        }

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for LayerSet {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.layers.iter().all(PropertyView::valid)
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed || self.layers.iter().any(PropertyView::changed)
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        let v_button_width = 15.0;
        let remaining_width = ui.get_contentregion_width() - 8.0;

        // -- label + add layer
        let colvars = if self.error.is_ok() {
            if self.world.is_some() {
                view::COL_DEFAULT
            } else {
                view::COL_INACTIVE
            }
        } else {
            view::COL_RED
        };

        ui.text_colored(colvars, &self.caption);

        if let Err(err) = self.error.as_ref() {
            if ui.is_item_hovered() {
                ui.tooltip(|| {
                    ui.text(err);
                });
            }
        }

        if self.layers.len() < super::layer::MAX_LAYERS {
            ui.same_line(remaining_width);
            if ui.small_enabled_button(im_str!("+"), self.world.is_some()) {
                result = Some(PropertyAction::ListItemAdd);
            }
        }

        if !self.layers.is_empty() {
            let row_height = 22.0;
            let height = f32::min(100.0, self.layers.len() as f32 * row_height);

            view::draw_colored_separator(ui, view::COL_SEPARATOR_LIGHT);
            ui.child_frame(im_str!("child frame"), (0.0, height))
                .show_borders(false)
                .always_show_vertical_scroll_bar(false)
                .build(|| {
                    let remaining_width = ui.get_contentregion_width() - v_button_width - 2.0;

                    // -- layer list
                    ui.columns(2, &self.caption, false);
                    ui.set_column_width(0, remaining_width);

                    for (i, layer) in self.layers.iter_mut().enumerate() {
                        ui.with_id(i as i32, || {
                            if let Some(action) = layer.draw(ui) {
                                result = Some(PropertyAction::ListItemAction(i, Box::new(action)))
                            }
                            ui.next_column();

                            if ui.button(im_str!("-"), (v_button_width, view::BUTTON_HEIGHT)) {
                                result = Some(PropertyAction::ListItemDel(i));
                            }
                            ui.next_column();
                        });
                    }

                    ui.columns(1, &self.caption, false);
                });
            view::draw_colored_separator(ui, view::COL_SEPARATOR_LIGHT);
        }

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for LayerReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.layer.is_some()
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        view::draw_colored_label(ui, self.world.caption(), self.error.as_ref());
        ui.same_line(0.0);
        ui.text(im_str!("/"));
        ui.same_line(0.0);

        if let Some(options) = self.options.get(&*self.world) {
            if let Some(id) = view::draw_popup_list(
                ui,
                self.layer.as_ref(),
                None,
                options,
                0.0,
                im_str!("##layer_popup"),
            ) {
                result = Some(PropertyAction::LayersSetLayer(id.clone()));
            }
        }

        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Communities
// ----------------------------------------------------------------------------
use gui::questgraph::CommunityPhaseList;
use gui::registry::{CommunityList, Phase};
use std::rc::Rc;
// ----------------------------------------------------------------------------
fn draw_community_phase_selection<'ui>(
    ui: &Ui<'ui>,
    label_width: f32,
    option_width: f32,
    phase: Option<&Rc<Phase>>,
    communities: &Rc<CommunityList>,
    by_phase: &Rc<CommunityPhaseList>,
    result: &mut Option<PropertyAction>,
) {
    let err = match phase {
        Some(_) => Ok(()),
        _ => Err(ImString::new("phase must be selected")),
    };
    view::draw_colored_label(ui, im_str!("  in phase"), err.as_ref());
    ui.same_line(label_width);

    view::draw_popup_selectable(
        ui,
        phase.map(|phase| &**phase),
        option_width,
        im_str!("##phase_popup"),
    );

    ui.popup(im_str!("##phase_popup"), || {
        ui.text(im_str!("select phase..."));
        ui.separator();
        ui.spacing();

        let mut has_entries = false;
        for (community, phases) in communities.iter() {
            ui.menu(community.caption()).build(|| {
                for phase in phases {
                    if ui.menu_item(phase.caption()).build() {
                        *result = Some(PropertyAction::CommunitySetPhaseAndCommunity(
                            phase.id().clone(),
                            community.id().clone(),
                        ));
                    }
                }
            });
            has_entries = has_entries || !phases.is_empty();
        }
        if has_entries {
            ui.separator();

            for phase in by_phase.keys() {
                if ui.menu_item(phase.caption()).build() {
                    *result = Some(PropertyAction::CommunitySetPhase(phase.id().clone()));
                }
            }
        } else {
            ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
        }
    });
}
// ----------------------------------------------------------------------------
impl PropertyView for CommunitySet {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        use self::CommunityOptions::*;

        let precondition = matches!(*self.options, ByPhase(_, _) if self.phase.is_some());
        precondition && self.spawnsets.iter().all(PropertyView::valid)
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed || self.spawnsets.iter().any(PropertyView::changed)
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        use self::CommunityOptions::*;

        let mut result = None;

        //TODO make this calculation sane...
        let padding = 4.0;
        let v_button_width = 15.0;
        let label_width = self.label_width + padding;
        let button_pos = padding + ui.get_contentregion_width() - v_button_width;
        let option_width = button_pos - label_width - 3.0 * padding;

        // -- label + add spawnset
        let enabled = match *self.options {
            All(_) => true,
            ByPhase(_, _) => self.phase.is_some(),
        };

        let colvars = if self.error.is_ok() {
            if enabled {
                view::COL_DEFAULT
            } else {
                view::COL_INACTIVE
            }
        } else {
            view::COL_RED
        };

        ui.text_colored(colvars, &self.caption);

        if let Err(err) = self.error.as_ref() {
            if ui.is_item_hovered() {
                ui.tooltip(|| {
                    ui.text(err);
                });
            }
        }

        if let ByPhase(ref communities, ref by_phase) = *self.options {
            draw_community_phase_selection(
                ui,
                label_width,
                option_width,
                self.phase.as_ref(),
                communities,
                by_phase,
                &mut result,
            );
        }

        if self.spawnsets.len() < super::community::MAX_SPAWNSETS {
            ui.same_line(button_pos + 2.0);
            if ui.small_enabled_button(im_str!("+"), enabled) {
                result = Some(PropertyAction::ListItemAdd);
            }
        }
        view::draw_colored_separator(ui, view::COL_SEPARATOR_LIGHT);

        if !self.spawnsets.is_empty() {
            let row_height = 22.0;
            let height = f32::min(100.0, self.spawnsets.len() as f32 * row_height);

            ui.child_frame(im_str!("child frame"), (0.0, height))
                .show_borders(false)
                .always_show_vertical_scroll_bar(false)
                .build(|| {
                    let spawnsetref_width = ui.get_contentregion_width() - v_button_width - 2.0;

                    // -- spawnset list
                    ui.columns(2, &self.caption, false);
                    ui.set_column_width(0, spawnsetref_width);

                    for (i, layer) in self.spawnsets.iter_mut().enumerate() {
                        ui.with_id(i as i32, || {
                            if let Some(action) = layer.draw(ui) {
                                result = Some(PropertyAction::ListItemAction(i, Box::new(action)))
                            }
                            ui.next_column();

                            if ui.button(im_str!("-"), (v_button_width, view::BUTTON_HEIGHT)) {
                                result = Some(PropertyAction::ListItemDel(i));
                            }
                            ui.next_column();
                        });
                    }

                    ui.columns(1, &self.caption, false);
                });
            view::draw_colored_separator(ui, view::COL_SEPARATOR_LIGHT);
        }
        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyView for CommunityReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        match self.r_type {
            CommunityReferenceType::Unchecked => self.unchecked.valid(),
            _ => self.spawnset.is_some(),
        }
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        use gui::registry::Community;

        let mut result = None;

        let r_type_width = 22.0;

        if let Some(id) =
            view::draw_type_selection(ui, &self.r_type, im_str!("##rtype_popup"), None)
        {
            result = Some(PropertyAction::CommunitySetType(id));
        }

        if let CommunityReferenceType::Unchecked = self.r_type {
            ui.same_line(r_type_width - 4.0);

            if let Some(action) = self.unchecked.draw(ui) {
                result = Some(PropertyAction::Update(action.into()));
            }
        } else {
            ui.same_line(r_type_width);

            let mut draw_items = |items: &[Community]| {
                for item in items {
                    if ui.menu_item(item.caption()).build() {
                        result = Some(PropertyAction::CommunitySetCommunity(item.id().clone()));
                    }
                }
            };

            match *self.options {
                CommunityOptions::ByPhase(_, ref options) => {
                    if let Some(ref phase) = self.phase {
                        view::draw_popup_selectable(
                            ui,
                            self.spawnset.as_ref(),
                            0.0,
                            im_str!("##spawnset_popup"),
                        );

                        ui.popup(im_str!("##spawnset_popup"), || {
                            if let Some(items) = options.get(&**phase) {
                                draw_items(items);
                            } else {
                                ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
                            }
                        });
                    }
                }
                CommunityOptions::All(ref options) => {
                    view::draw_popup_selectable(
                        ui,
                        self.spawnset.as_ref(),
                        0.0,
                        im_str!("##spawnset_popup"),
                    );

                    ui.popup(im_str!("##spawnset_popup"), || {
                        for item in options.keys() {
                            if ui.menu_item(item.caption()).build() {
                                result =
                                    Some(PropertyAction::CommunitySetCommunity(item.id().clone()));
                            }
                        }
                        if options.is_empty() {
                            ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
                        }
                    });
                }
            }
        }
        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Items
// ----------------------------------------------------------------------------
impl PropertyView for RewardReference {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        match self.r_type {
            RewardReferenceType::Unchecked => self.unchecked.valid(),
            _ => !self.required || self.reward.is_some(),
        }
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        let r_type_width = 22.0;

        if let Some(id) =
            view::draw_type_selection(ui, &self.r_type, im_str!("##rtype_popup"), None)
        {
            result = Some(PropertyAction::RewardSetType(id));
        }

        if let RewardReferenceType::Unchecked = self.r_type {
            ui.same_line(r_type_width - 4.0);

            if let Some(action) = self.unchecked.draw(ui) {
                result = Some(PropertyAction::Update(action.into()));
            }
        } else {
            ui.same_line(r_type_width);

            view::draw_popup_selectable(ui, self.reward.as_ref(), 0.0, im_str!("##reward_popup"));

            ui.popup(im_str!("##reward_popup"), || {
                for item in &*self.options {
                    if ui.menu_item(item.caption()).build() {
                        result = Some(PropertyAction::RewardSetReward(item.id().clone()));
                    }
                }

                if self.options.is_empty() {
                    ui.text_colored(view::COL_INACTIVE, im_str!("none found"));
                }
            });
        }
        result
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Helper
// ----------------------------------------------------------------------------
impl view::AsOptionList for SceneReferenceType {
    type Id = u8;
    // ------------------------------------------------------------------------
    fn as_short_caption(&self) -> (&str, Self::Id) {
        match self {
            SceneReferenceType::Definition => ("g", 0),
            SceneReferenceType::Unchecked => ("!", 1),
        }
    }
    // ------------------------------------------------------------------------
    fn options(&self) -> Vec<(&::imgui::ImStr, Self::Id)> {
        vec![
            (im_str!("generated scene"), 0),
            (im_str!("unchecked path"), 1),
        ]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<u8> for SceneReferenceType {
    fn from(id: u8) -> SceneReferenceType {
        match id {
            0 => SceneReferenceType::Definition,
            _ => SceneReferenceType::Unchecked,
        }
    }
}
// ----------------------------------------------------------------------------
impl view::AsOptionList for TagReferenceType {
    type Id = u8;
    // ------------------------------------------------------------------------
    fn as_short_caption(&self) -> (&str, Self::Id) {
        match self {
            TagReferenceType::WorldAndTag => ("t", 0),
            TagReferenceType::WorldAndId => ("i", 1),
            TagReferenceType::Unchecked => ("!", 2),
        }
    }
    // ------------------------------------------------------------------------
    fn options(&self) -> Vec<(&::imgui::ImStr, Self::Id)> {
        vec![
            (im_str!("tag"), 0),
            (im_str!("tag-id"), 1),
            (im_str!("unchecked tag"), 2),
        ]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<u8> for TagReferenceType {
    fn from(id: u8) -> TagReferenceType {
        match id {
            0 => TagReferenceType::WorldAndTag,
            1 => TagReferenceType::WorldAndId,
            _ => TagReferenceType::Unchecked,
        }
    }
}
// ----------------------------------------------------------------------------
impl view::AsOptionList for InteractionEntityType {
    type Id = InteractionEntityType;
    // ------------------------------------------------------------------------
    fn as_short_caption(&self) -> (&str, Self::Id) {
        use self::InteractionEntityType::*;

        match self {
            Unchecked => ("!", Unchecked),
            LayerEntity => ("e", LayerEntity),
        }
    }
    // ------------------------------------------------------------------------
    fn options(&self) -> Vec<(&::imgui::ImStr, Self::Id)> {
        use self::InteractionEntityType::*;

        vec![
            (im_str!("unchecked tag"), Unchecked),
            (im_str!("layer entity-id"), LayerEntity),
        ]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl view::AsOptionList for CommunityReferenceType {
    type Id = CommunityReferenceType;
    // ------------------------------------------------------------------------
    fn as_short_caption(&self) -> (&str, CommunityReferenceType) {
        use self::CommunityReferenceType::*;

        match self {
            Id => ("c", Id),
            Unchecked => ("!", Unchecked),
        }
    }
    // ------------------------------------------------------------------------
    fn options(&self) -> Vec<(&::imgui::ImStr, CommunityReferenceType)> {
        use self::CommunityReferenceType::*;

        vec![
            (im_str!("community id"), Id),
            (im_str!("unchecked path"), Unchecked),
        ]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl view::AsOptionList for RewardReferenceType {
    type Id = RewardReferenceType;
    // ------------------------------------------------------------------------
    fn as_short_caption(&self) -> (&str, RewardReferenceType) {
        use self::RewardReferenceType::*;

        match self {
            Id => ("r", Id),
            Unchecked => ("!", Unchecked),
        }
    }
    // ------------------------------------------------------------------------
    fn options(&self) -> Vec<(&::imgui::ImStr, RewardReferenceType)> {
        use self::RewardReferenceType::*;

        vec![
            (im_str!("reward id"), Id),
            (im_str!("unchecked tag"), Unchecked),
        ]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl view::AsOptionList for JournalReferenceType {
    type Id = u8;
    // ------------------------------------------------------------------------
    fn as_short_caption(&self) -> (&str, Self::Id) {
        match self {
            JournalReferenceType::Character => ("C", 0),
            JournalReferenceType::Creature => ("B", 1),
            JournalReferenceType::Quest => ("Q", 2),
        }
    }
    // ------------------------------------------------------------------------
    fn options(&self) -> Vec<(&::imgui::ImStr, Self::Id)> {
        vec![
            (im_str!("character"), 0),
            (im_str!("beast"), 1),
            (im_str!("quest"), 2),
        ]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<u8> for JournalReferenceType {
    fn from(id: u8) -> JournalReferenceType {
        match id {
            0 => JournalReferenceType::Character,
            1 => JournalReferenceType::Creature,
            _ => JournalReferenceType::Quest,
        }
    }
}
// ----------------------------------------------------------------------------
