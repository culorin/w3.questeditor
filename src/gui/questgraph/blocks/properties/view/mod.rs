//
// blocks::properties::view
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
// traits
// ----------------------------------------------------------------------------
pub(in gui) trait PropertyView {
    fn valid(&self) -> bool;
    fn changed(&self) -> bool;
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction>;
}
// ----------------------------------------------------------------------------
// draw helper
// ----------------------------------------------------------------------------
/// `AsOptionList` helper trait
pub(super) trait AsOptionList {
    type Id;
    fn as_short_caption(&self) -> (&str, Self::Id);
    fn options(&self) -> Vec<(&ImStr, Self::Id)>;
}
// ----------------------------------------------------------------------------
pub(super) fn draw_colored_label<'ui>(ui: &Ui<'ui>, label: &ImStr, valid: Result<&(), &ImString>) {
    let colvars = if valid.is_ok() { COL_DEFAULT } else { COL_RED };

    ui.text_colored(colvars, label);

    if let Err(err) = valid {
        if ui.is_item_hovered() {
            ui.tooltip(|| {
                ui.text(err);
            });
        }
    }
}
// ----------------------------------------------------------------------------
pub(super) fn draw_popup_selectable<'ui, T: AsCaption>(
    ui: &Ui<'ui>,
    option: Option<&T>,
    width: f32,
    popupid: &ImStr,
) {
    let label = option.map(AsCaption::caption).unwrap_or_else(|| im_str!(""));

    if ui.selectable(
        label,
        false,
        ::imgui::ImGuiSelectableFlags::empty(),
        (width, 0.0),
    ) {
        ui.open_popup(popupid);
    }
}
// ----------------------------------------------------------------------------
pub(super) fn draw_type_selection<'ui, O, I>(
    ui: &Ui<'ui>,
    c_type: &O,
    popupid: &ImStr,
    button_size: Option<(f32, f32)>,
) -> Option<I>
where
    O: AsOptionList<Id = I>,
    I: PartialEq,
{
    let mut clicked_id = None;

    let (caption, selected_id) = c_type.as_short_caption();

    if ui.button(
        im_str!("{}", caption),
        button_size.unwrap_or((14.0, BUTTON_HEIGHT)),
    ) {
        ui.open_popup(popupid);
    }

    ui.popup(popupid, || {
        ui.text(im_str!("select type..."));
        ui.separator();
        ui.spacing();

        for (option, id) in c_type.options() {
            if ui.selectable(
                option,
                id == selected_id,
                ::imgui::ImGuiSelectableFlags::empty(),
                (0.0, 0.0),
            ) && id != selected_id
            {
                clicked_id = Some(id);
            }
        }
    });
    clicked_id
}
// ----------------------------------------------------------------------------
// TODO unify with optionlist ?
pub(super) fn draw_popup_selection<'ui, I>(
    ui: &Ui<'ui>,
    selected: &I,
    width: f32,
    popupid: &ImStr,
) -> Option<I>
where
    I: AsCaption + AsOptionList<Id = I> + PartialEq,
{
    let mut clicked_id = None;

    draw_popup_selectable(ui, Some(selected), width, popupid);

    ui.popup(popupid, || {
        for (option, id) in selected.options() {
            if ui.selectable(
                option,
                &id == selected,
                ::imgui::ImGuiSelectableFlags::empty(),
                (0.0, 0.0),
            ) && &id != selected
            {
                clicked_id = Some(id);
            }
        }
    });
    clicked_id
}
// ----------------------------------------------------------------------------
pub(super) fn draw_popup_list<'ui, 'options, I>(
    ui: &Ui<'ui>,
    selected: Option<&UiOption<I>>,
    popup_title: Option<&ImStr>,
    options: &'options [UiOption<I>],
    width: f32,
    popupid: &ImStr,
) -> Option<&'options I>
where
    I: PartialEq,
{
    let mut clicked_id = None;

    draw_popup_selectable(ui, selected, width, popupid);

    ui.popup(popupid, || {
        if let Some(title) = popup_title {
            ui.text(title);
            ui.separator();
            ui.spacing();
        }
        for option in options {
            let selected = selected.map(|s| s.id() == option.id()).unwrap_or(false);
            if ui.selectable(
                option.caption(),
                selected,
                ::imgui::ImGuiSelectableFlags::empty(),
                (0.0, 0.0),
            ) && !selected
            {
                clicked_id = Some(option.id());
            }
        }
        if options.is_empty() {
            ui.text_colored(COL_INACTIVE, im_str!("none found"))
        }
    });
    clicked_id
}
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn draw_colored_separator(ui: &Ui, col: (f32, f32, f32, f32)) {
    ui.with_color_var(::imgui::ImGuiCol::Separator, col, || {
        ui.separator();
    });
}
// ----------------------------------------------------------------------------
// macros
// ----------------------------------------------------------------------------
macro_rules! render_property {
    ($ui: ident, $property: ident, $result: expr) => ({
        use ::gui::questgraph::blocks::properties::Property;
        if let Some(action) = match $property {
            Property::Field(id, field) => {
                field.draw($ui).map(|action| BlockEditorAction::Property(id.to_owned(), action.into()))
            }
            Property::Control(id, control) => {
                control.draw($ui).map(|action| BlockEditorAction::Property(id.to_owned(), action))
            },
        } {
            $result = Some(action.into());
        }
    });
}
// ----------------------------------------------------------------------------
macro_rules! new_property_field_bool {
    ($id: expr, $label: expr, $value: expr) => ({
        Property::new_field(
            $id,
            Box::new(input::BoolField::new($id, $value).set_label($label).set_label_width(LABEL_WIDTH))
        )
    });
    ($id: expr, $label: expr, $value: expr, $label_width: expr) => ({
        Property::new_field(
            $id,
            Box::new(input::BoolField::new($id, $value).set_label($label).set_label_width($label_width))
        )
    });
}
// ----------------------------------------------------------------------------
macro_rules! new_property_field {
    ($id: expr, $label: expr, $fieldtype: path, $validators: expr) => ({
        use ::gui::questgraph::blocks::properties::view::LABEL_WIDTH;

        Property::new_field(
            $id,
            Box::new(
                $fieldtype($id, None).set_label($label).set_label_width(LABEL_WIDTH).set_validators($validators)
            )
        )
    });
    ($id: expr, $label: expr, $value: expr, $fieldtype: path, $validators: expr) => ({
        use ::gui::questgraph::blocks::properties::view::LABEL_WIDTH;

        Property::new_field(
            $id,
            Box::new(
                $fieldtype($id, Some($value)).set_label($label).set_label_width(LABEL_WIDTH).set_validators($validators)
            )
        )
    });
    ($id: expr, $label: expr, $value: expr, $fieldtype: path, $validators: expr, $width: expr) => ({
        use ::gui::questgraph::blocks::properties::view::LABEL_WIDTH;

        Property::new_field(
            $id,
            Box::new(
                $fieldtype($id, Some($value))
                    .set_label($label).set_label_width(LABEL_WIDTH)
                    .set_validators($validators)
                    .set_width($width)
            )
        )
    });
}
// ----------------------------------------------------------------------------
macro_rules! ctl_to_property {
    ($control: expr, $type: path, $id: expr) => ({
        $control
            .as_any()
            .downcast_ref::<$type>()
            .expect(&format!("invalid type for {}", $id))
    })
}
// ----------------------------------------------------------------------------
// misc
// ----------------------------------------------------------------------------
pub(in gui) const LABEL_WIDTH: f32 = 90.0;
pub(in gui) const LABEL_WIDTH_EXTENDED: f32 = LABEL_WIDTH + 30.0;
pub(in gui) const FIELD_WIDTH_TIME_HHMM: f32 = 45.0;
pub(in gui) const FIELD_WIDTH_TIME_HHMMSS: f32 = 65.0;
pub(in gui) const BUTTON_HEIGHT: f32 = 19.0;

pub(in gui) const COL_RED: (f32, f32, f32, f32) = (1.0, 0.0, 0.0, 1.0);
pub(in gui) const COL_DEFAULT: (f32, f32, f32, f32) = (255.0, 255.0, 255.0, 1.0);
pub(in gui) const COL_INACTIVE: (f32, f32, f32, f32) = (255.0, 255.0, 255.0, 0.4);
pub(in gui) const COL_SEPARATOR_LIGHT: (f32, f32, f32, f32) = (0.3, 0.3, 0.3, 0.95);
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::{ImStr, ImString, Ui};

use gui::lists::{AsCaption, UiOption};

// actions
use super::PropertyAction;

// state
use super::conditions as conds;
use super::{
    AreaConditionType, ConditionType, InteractionConditionType, LoadScreenConditionType,
    TimeConditionType,
};

use super::{ScriptParameter, ScriptParameterSet, TagList, VarType};

// misc
use super::conditions::MAX_CONDITIONS;
use super::misc::MAX_TAGS;
use super::scriptparameter::MAX_SCRIPTPARAMS;
// ----------------------------------------------------------------------------
mod conditions;
mod misc;
mod scriptparameter;
// ----------------------------------------------------------------------------
