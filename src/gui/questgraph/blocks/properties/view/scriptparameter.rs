//
// properties::view::scriptparameter
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::Ui;
use imgui_controls::input::Field;

use super::PropertyView;

// actions
use super::PropertyAction;

// state
use super::{ScriptParameter, ScriptParameterSet, VarType};

// misc
use super::AsOptionList;
// ----------------------------------------------------------------------------
impl PropertyView for ScriptParameterSet {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        self.params.iter().all(ScriptParameter::valid)
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed || self.params.iter().any(ScriptParameter::changed)
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        super::draw_colored_label(ui, im_str!("parameter list"), self.error.as_ref());

        ui.with_style_var(::imgui::StyleVar::ItemSpacing((0.0, 4.0).into()), || {
            let v_type_width = 18.0;
            let v_button_width = 15.0;
            let remaining_width = ui.get_contentregion_width() - v_type_width - v_button_width;

            ui.columns(4, im_str!("scriptparam_table"), false);

            ui.set_column_width(0, v_type_width);
            ui.set_column_width(1, remaining_width * 0.4);
            ui.set_column_width(2, remaining_width * 0.6);
            ui.set_column_width(3, v_button_width);

            // paramn list header
            ui.next_column();

            if self.params.is_empty() {
                ui.text_colored(super::COL_INACTIVE, im_str!("empty"));
                ui.next_column();
            } else {
                ui.text(im_str!("name"));
                ui.next_column();
                ui.text(im_str!("value"));
            };
            ui.next_column();
            if self.params.len() < super::MAX_SCRIPTPARAMS && ui.small_button(im_str!("+")) {
                result = Some(PropertyAction::ListItemAdd);
            }
            ui.next_column();
            if !self.params.is_empty() {
                ui.separator();
            }

            for (i, param) in &mut self.params.iter_mut().enumerate() {
                ui.with_id(i as i32, || {
                    if let Some(id) =
                        super::draw_type_selection(ui, &param.v_type, im_str!("##type_popup"), None)
                    {
                        result = Some(PropertyAction::ScriptParamSetType(i, (id).into()));
                    }

                    ui.next_column();
                    if let Some(action) = param.v_name.draw(ui) {
                        result = Some(PropertyAction::ScriptParamUpdateName(i, action.into()));
                    }

                    ui.next_column();
                    if let Some(action) = param.v_value.draw(ui) {
                        result = Some(PropertyAction::ListItemUpdate(i, action.into()));
                    }

                    ui.next_column();
                    if ui.button(im_str!("-"), (0.0, super::BUTTON_HEIGHT)) {
                        result = Some(PropertyAction::ListItemDel(i));
                    }
                    ui.next_column();
                });
            }
        });
        ui.columns(1, im_str!("scriptparam_table"), false);

        result
    }
}
// ----------------------------------------------------------------------------
impl AsOptionList for VarType {
    type Id = u8;
    // ------------------------------------------------------------------------
    fn as_short_caption(&self) -> (&str, Self::Id) {
        match self {
            VarType::String => ("S", 0),
            VarType::CName => ("C", 1),
            VarType::Bool => ("B", 2),
            VarType::Float => ("F", 3),
            VarType::Int32 => ("I", 4),
        }
    }
    // ------------------------------------------------------------------------
    fn options(&self) -> Vec<(&::imgui::ImStr, Self::Id)> {
        vec![
            (im_str!("String"), 0),
            (im_str!("CName"), 1),
            (im_str!("Bool"), 2),
            (im_str!("Float"), 3),
            (im_str!("Int"), 4),
        ]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<u8> for VarType {
    fn from(id: u8) -> VarType {
        match id {
            0 => VarType::String,
            1 => VarType::CName,
            2 => VarType::Bool,
            3 => VarType::Float,
            _ => VarType::Int32,
        }
    }
}
// ----------------------------------------------------------------------------
