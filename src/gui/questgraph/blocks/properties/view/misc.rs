//
// properties::view::misc
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::Ui;
use imgui_controls::input::Field;

use super::PropertyView;

// actions
use super::PropertyAction;

// state
use super::TagList;

// misc
// ----------------------------------------------------------------------------
impl PropertyView for TagList {
    // ------------------------------------------------------------------------
    fn valid(&self) -> bool {
        (!self.required || self.tags.is_empty()) && self.tags.iter().all(Field::valid)
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        self.changed || self.tags.iter().any(Field::changed)
    }
    // ------------------------------------------------------------------------
    fn draw<'ui>(&mut self, ui: &Ui<'ui>) -> Option<PropertyAction> {
        let mut result = None;

        // spacing_adjustment
        let label_width = self.label_width - 8.0;

        ui.with_style_var(::imgui::StyleVar::ItemSpacing((0.0, 4.0).into()), || {
            let v_button_width = 15.0;
            let remaining_width = ui.get_contentregion_width() - v_button_width - label_width;

            ui.columns(3, im_str!("tags_table"), false);

            ui.set_column_width(0, label_width);
            ui.set_column_width(1, remaining_width);
            ui.set_column_width(2, v_button_width);

            // list header
            super::draw_colored_label(ui, &self.label, self.error.as_ref());
            ui.next_column();

            if self.tags.is_empty() {
                ui.text_colored(super::COL_INACTIVE, im_str!("empty"));
            } else {
                for (i, tag) in &mut self.tags.iter_mut().enumerate() {
                    ui.with_id(i as i32, || {
                        if let Some(action) = tag.draw(ui) {
                            result = Some(PropertyAction::ListItemUpdate(i, action.into()));
                        }

                        ui.next_column();
                        if ui.button(im_str!("-"), (0.0, super::BUTTON_HEIGHT)) {
                            result = Some(PropertyAction::ListItemDel(i));
                        }
                        ui.next_column();
                        ui.next_column();
                    });
                }
            };
            ui.next_column();
            if self.tags.len() < super::MAX_TAGS
                && ui.button(im_str!("+"), (0.0, super::BUTTON_HEIGHT))
            {
                result = Some(PropertyAction::ListItemAdd);
            }
            ui.next_column();
        });
        ui.columns(1, im_str!("tags_table"), false);

        result
    }
}
// ----------------------------------------------------------------------------
