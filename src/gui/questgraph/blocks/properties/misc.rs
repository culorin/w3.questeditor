//
// blocks::properties::misc
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
/// list with multiple tags
pub(in gui::questgraph::blocks) struct TagList {
    pub(super) label_width: f32,
    pub(super) label: ImString,
    pub(super) required: bool,
    pub(super) tags: Vec<input::TextField>,
    pub(super) changed: bool,
    pub(super) error: Result<(), ImString>,
}
// ----------------------------------------------------------------------------
pub(super) const MAX_TAGS: usize = 10;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::any::Any;

use imgui::ImString;
use imgui_controls::input;
use imgui_controls::input::validator;
use imgui_controls::input::Field;

// actions
use super::PropertyAction;

// misc

use super::PropertyControl;
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl TagList {
    // ------------------------------------------------------------------------
    pub fn new(width: f32, label: &str, tags: &[String], required: bool) -> TagList {
        TagList {
            label_width: width,
            label: ImString::new(label),
            required,
            tags: tags.iter()
                .map(|tag| Self::create_tagfield(tag))
                .collect(),
            changed: false,
            error: Ok(()),
        }
    }
    // ------------------------------------------------------------------------
    pub fn tags(&self) -> Vec<String> {
        self.tags
            .iter()
            .filter_map(|field| field.value().as_str().ok())
            .filter(|v| !v.is_empty())
            .map(ToOwned::to_owned)
            .collect()
    }
    // ------------------------------------------------------------------------
    fn create_tagfield(tag: &str) -> input::TextField {
        let mut field = input::TextField::new("##tag", Some(tag)).set_validators(vec![
            validator::is_nonempty(),
            validator::chars("^[0-9_a-zA-Z]*$", "[a-z_0-9]"),
            validator::min_length(2),
            validator::max_length(250),
        ]);
        field.validate();
        field
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl PropertyControl for TagList {
    // ------------------------------------------------------------------------
    fn reset_changed(&mut self) {
        self.changed = false;
        self.tags.iter_mut().for_each(Field::reset_changed);
    }
    // ------------------------------------------------------------------------
    fn validate(&mut self) -> &mut dyn PropertyControl {
        let invalid = self.tags
            .iter()
            .enumerate()
            .filter(|(_, t)| !t.valid())
            .map(|(i, _)| format!("{}", i + 1))
            .collect::<Vec<_>>();

        self.error = match invalid.len() {
            0 => Ok(()),
            1 => Err(ImString::from(format!(
                "tag {} is invalid.",
                invalid.join("")
            ))),
            _ => Err(ImString::from(format!(
                "tags {} are invalid.",
                invalid.join(", ")
            ))),
        };
        if self.required && self.tags.is_empty() {
            self.error = Err(ImString::new("tag list must not be empty"))
        }
        self
    }
    // ------------------------------------------------------------------------
    fn process_action(&mut self, action: PropertyAction) -> Result<(), String> {
        use self::PropertyAction::*;
        self.changed = true;

        match action {
            ListItemAdd => {
                // trigger validate to indicate any error
                let mut tagfield = Self::create_tagfield("");
                tagfield.validate();
                self.tags.push(tagfield);
            }
            ListItemDel(index) if index < self.tags.len() => {
                self.tags.remove(index);
            }
            ListItemUpdate(index, ref value) => {
                if let Some(tagfield) = self.tags.get_mut(index) {
                    tagfield.validate().set_value(value.into())?
                }
            }
            _ => unreachable!(
                "actor tags property received unsupported action {:?}",
                action
            ),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn as_any(&self) -> &dyn Any {
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
