//
// gui/auxiliary windows/helpers
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui) fn show_windows<'a>(
    ui: &Ui<'a>,
    windows: &mut WindowState,
    help: &HelpSystem,
    settings: &mut Settings,
) -> Option<SettingsAction> {
    let mut result = None;

    if windows.show_help {
        render_help_window(ui, &mut windows.show_help, help);
    }

    // -- modals
    if windows.show_about {
        render_about_window(ui, &mut windows.show_about);
    }

    if windows.show_settings {
        if let Some(action) = render_settings_window(ui, &mut windows.show_settings, settings) {
            result = Some(action);
        }
    }

    if windows.error.show() {
        windows::error(ui, &mut windows.error);
    }

    if windows.info.show() {
        windows::info(ui, &mut windows.info);
    }
    result
}
// ----------------------------------------------------------------------------
pub(in gui) fn set_error(error: &mut windows::ErrorWindow, msg: &str) {
    error.set_msg(msg);
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui;
use imgui::Ui;
use imgui_support::windows;

use super::{HelpSystem, HelpTopic, Settings, SettingsAction, WindowState};
// ----------------------------------------------------------------------------
const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
const GIT_HASH: &str = env!("GIT_HASH");
const BUILD_TIME: &str = env!("BUILD_TIME");
// ----------------------------------------------------------------------------
fn render_about_window<'a>(ui: &Ui<'a>, opened: &mut bool) {
    ui.window(im_str!("About"))
        .size((450.0, 300.0), imgui::ImGuiCond::Always)
        .resizable(false)
        .opened(opened)
        .build_modal(|| {
            ui.text(im_str!("Witcher 3 Quest Editor"));
            ui.same_line(0.0);
            ui.text(format!(
                "v{} ({}/{})",
                VERSION.unwrap_or("unknown"),
                BUILD_TIME,
                GIT_HASH,
            ));

            ui.separator();
            ui.text_wrapped(im_str!(
                "\nWitcher 3 Questgraph Editor for \"The Witcher 3: Wild Hunt\" \
                game by CD Projekt Red is part of radish modding tools.\n\n\
                radish modding tools are a collection of community created \
                modding tools aimed to enable the creation of new quests for \
                \"The Witcher 3: Wild Hunt\" game by CD Projekt Red.\n\n\
                The full package can be downloaded from nexusmods: \n\
                https://www.nexusmods.com/witcher3/mods/3620"
            ));
            ui.new_line();
            ui.text_wrapped(im_str!(
                "Questgraph editor sourcecode repository:\n\
                https://codeberg.org/rmemr/w3.questeditor\n\n\
                See Cargo.toml for all used rust crates and libraries. \
                Used UI Framework: \"Dear ImGui\" by Omar Cornut and others \
                (https://github.com/ocornut/imgui)"
            ));
        });
}
// ----------------------------------------------------------------------------
fn render_help_window<'a>(ui: &Ui<'a>, opened: &mut bool, help: &HelpSystem) {
    ui.window(im_str!("Documentation"))
        .size((400.0, 500.0), imgui::ImGuiCond::FirstUseEver)
        .opened(opened)
        .build(|| {
            let mut is_missing = true;
            for topic in help.topics() {
                if let HelpTopic::General(header) = topic {
                    is_missing = false;
                    // if ui.collapsing_header(header).build() {
                    ui.tree_node(header).build(|| {
                        if let Some(helptxt) = help.get(topic) {
                            ui.text_wrapped(helptxt);
                        }
                        ui.new_line();
                        ui.separator();
                    });
                }
            }

            if is_missing {
                ui.text(im_str!("Documentation missing!"));
            }
        });
}
// ----------------------------------------------------------------------------
fn render_settings_window<'a>(
    ui: &Ui<'a>,
    opened: &mut bool,
    settings: &mut Settings,
) -> Option<SettingsAction> {
    let mut result = None;

    ui.window(im_str!("Settings"))
        .size((320.0, 220.0), imgui::ImGuiCond::Always)
        .resizable(false)
        .movable(false)
        .opened(opened)
        .build_modal(|| {
            ui.spacing();
            ui.text_wrapped(im_str!(
                "All settings will be saved automatically on program exit and \
                restored on next start."
            ));
            ui.spacing();
            ui.separator();

            ui.spacing();
            ui.spacing();
            ui.text(im_str!("right panel"));
            ui.spacing();
            if ui
                .slider_float(
                    im_str!("panel width"),
                    &mut settings.rpanel_width,
                    320.0,
                    500.0,
                )
                .display_format(im_str!("%.0f"))
                .build()
            {
                result = Some(SettingsAction::PanelWidthChanged);
            }

            ui.new_line();
            ui.text(im_str!("plane grid spacing"));
            ui.spacing();
            if ui
                .slider_int(
                    im_str!("horizontal##gridxwidth"),
                    &mut settings.grid_width.0,
                    1,
                    150,
                )
                .build()
            {
                result = Some(SettingsAction::GridSpacingChanged(
                    settings.grid_width.0,
                    settings.grid_width.1,
                ));
            }
            if ui
                .slider_int(
                    im_str!("vertical##gridywidth"),
                    &mut settings.grid_width.1,
                    1,
                    150,
                )
                .build()
            {
                result = Some(SettingsAction::GridSpacingChanged(
                    settings.grid_width.0,
                    settings.grid_width.1,
                ));
            }

            settings.changed = true;
        });

    result
}
// ----------------------------------------------------------------------------
