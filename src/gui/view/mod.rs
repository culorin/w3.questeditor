//
// gui::view
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub mod auxiliary;
pub mod info;
pub mod menu;
// ----------------------------------------------------------------------------
#[inline]
pub(super) fn show_editor<'ui>(
    ui: &Ui<'ui>,
    screenspace: &ScreenSpaceManager,
    state: &mut State,
) -> Option<::gui::Action> {
    if state.data.is_available() {
        let mut result = None;
        ui.with_style_var(StyleVar::WindowRounding(0.0), || {
            result = match state.mode {
                EditMode::QuestGraph => {
                    show_questgraph_editor(ui, screenspace, &mut state.data, &state.help)
                }
                EditMode::None => None,
            };
            // show global mode independent info, asset registry, etc
            self::info::render(ui, screenspace.left(), &state.data);
        });
        result
    } else {
        None
    }
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::{StyleVar, Ui};

// actions
use super::MenuSelection;
use super::settings::SettingsAction;

// state
use super::help::{HelpSystem, HelpTopic};
use super::settings::Settings;
use super::{EditMode, QuestData, State, WindowState};

// different subviews
use super::questgraph;

// util
use super::{ScreenSpaceManager, UiArea};
// ----------------------------------------------------------------------------
#[inline]
fn show_questgraph_editor<'ui>(
    ui: &Ui<'ui>,
    screenspace: &ScreenSpaceManager,
    data: &mut QuestData,
    help: &HelpSystem,
) -> Option<::gui::Action> {
    if let Some(ref mut state) = data.graph {
        let structure = data
            .definition
            .as_ref()
            .map(|def| def.structure())
            .unwrap();
        questgraph::view::show(ui, screenspace, state, structure, help)
    } else {
        None
    }
}
// ----------------------------------------------------------------------------
