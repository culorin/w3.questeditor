//
// gui::view::menu
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(in gui) fn render<'a>(
    ui: &Ui<'a>,
    mode: EditMode,
    data: &QuestData,
    settings: &mut Settings,
    help: &HelpSystem,
) -> Option<MenuSelection> {
    let mut result = None;
    ui.main_menu_bar(|| {
        ui.menu(im_str!("Project")).build(|| {
            if ui.menu_item(im_str!("New")).build() {
                result = Some(MenuSelection::NewProject);
            }
            let mut auto_reload = data.auto_reload;
            if ui
                .menu_item(im_str!("Auto reload on change"))
                .selected(&mut auto_reload)
                .build()
            {
                result = Some(MenuSelection::SetAutoReload(auto_reload));
            }
            ui.separator();
            if ui.menu_item(im_str!("Load")).build() {
                result = Some(MenuSelection::LoadProject);
            }
            if ui
                .menu_item(im_str!("Reload"))
                .enabled(!data.is_new())
                .build()
            {
                result = Some(MenuSelection::ReloadProject);
            }
            if ui
                .menu_item(im_str!("Save"))
                .enabled(!data.is_new() && data.is_available())
                .build()
            {
                result = Some(MenuSelection::SaveProject);
            }
            if ui
                .menu_item(im_str!("Save in..."))
                .enabled(data.is_available())
                .build()
            {
                result = Some(MenuSelection::SaveProjectAtLocation);
            }
            ui.separator();
            if ui
                .menu_item(im_str!("Close"))
                .enabled(data.is_available())
                .build()
            {
                result = Some(MenuSelection::CloseProject);
            }
            ui.separator();
            if ui.menu_item(im_str!("Quit editor")).build() {
                result = Some(MenuSelection::Quit);
            }
        });

        // mode specific main menus
        if let Some(ref def) = data.definition {
            if let EditMode::QuestGraph = mode {
                questgraph_main_menu(ui, data.graph.as_ref(), settings, def, &mut result);
            }
        }

        ui.menu(im_str!("|")).enabled(false).build(|| {});
        if ui.menu_item(im_str!("Settings")).build() {
            result = Some(MenuSelection::ShowSettings);
        }
        ui.menu(im_str!("|")).enabled(false).build(|| {});

        ui.menu(im_str!("Help")).build(|| {
            if ui.menu_item(im_str!("Documentation")).build() {
                result = Some(MenuSelection::ShowHelp);
            }
            let mut context_help = help.context_help();
            if ui
                .menu_item(im_str!("Context Help"))
                .enabled(!help.is_empty())
                .selected(&mut context_help)
                .build()
            {
                result = Some(MenuSelection::ShowContextHelp(context_help));
            }
            ui.separator();
            if ui.menu_item(im_str!("About")).build() {
                result = Some(MenuSelection::ShowAbout);
            }
        });
        // #[cfg(debug_assertions)]
        // debug::show_debug_menu(ui, state);
    });
    result
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui::Ui;

use super::MenuSelection;

use super::{EditMode, HelpSystem, QuestData, Settings};

use super::questgraph;
// ----------------------------------------------------------------------------
#[inline]
fn questgraph_main_menu<'ui>(
    ui: &Ui<'ui>,
    graph: Option<&questgraph::GraphState>,
    settings: &mut Settings,
    def: &::model::QuestDefinition,
    result: &mut Option<MenuSelection>,
) {
    if let (Some(graph), structure) = (graph, def.structure()) {
        if let Some(action) =
            questgraph::view::menu::show_main_menus(ui, graph, settings, structure)
        {
            *result = Some(MenuSelection::QuestGraph(questgraph::Action::Menu(action)));
        }
    }
}
// ----------------------------------------------------------------------------
