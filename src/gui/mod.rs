//
// gui for interactive quest editor
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
mod support;
// mod hotkeys;
#[macro_use]
mod lists;
mod registry;

// -- application modules
mod questgraph;
// mod settings;
mod cmds;
mod types;
mod update;
mod utils;
mod view;

mod settings;

mod help;

// #[cfg(debug_assertions)]
// mod debug;

use std::path::{Path, PathBuf};
use std::sync::mpsc;
use std::time;

use imgui::{ImString, Ui};

use imgui_controls::filebrowser;
use imgui_controls::popup;
use imgui_support::windows;

use imgui_support::actions;
use imgui_support::actions::{ActionState, Sequence};

use notify::{RecommendedWatcher, RecursiveMode, Watcher};

use definitions;
use model;

use self::types::{FieldId, FieldValue};
use self::utils::{ScreenSpaceManager, UiArea};

const CLEAR_COLOR: [f32; 4] = [114.0 / 255.0, 144.0 / 255.0, 154.0 / 255.0, 1.0];
// ----------------------------------------------------------------------------
#[derive(Copy, Clone)]
enum EditMode {
    QuestGraph,
    None,
}
// ----------------------------------------------------------------------------
struct State {
    windows: WindowState,

    mode: EditMode,
    data: QuestData,

    filebrowser: FileBrowser,
    popup: Option<popup::Popup<PopupAction, Action>>,

    settings: settings::Settings,
    help: help::HelpSystem,
}
// ----------------------------------------------------------------------------
#[derive(Default)]
struct WindowState {
    show_help: bool,
    show_about: bool,
    show_settings: bool,

    show_filebrowser: bool,
    info: windows::InfoWindow,
    error: windows::ErrorWindow,
}
// ----------------------------------------------------------------------------
#[derive(Debug)]
enum Action {
    GuardModifiedData,
    Confirm(String, Vec<Action>, Vec<Action>),
    SaveCurrent,
    SaveProject(PathBuf),
    ReloadCurrent,
    LoadProject(PathBuf),
    QuestGraph(questgraph::Action),
    PopupRequest(Box<dyn popup::PopupContent<PopupAction, Action>>),
    Popup(PopupAction),
    Menu(MenuSelection),
    OpenFileBrowser(FileBrowserIntent),
    FileBrowser(filebrowser::Selection),
    SettingsChanged(settings::SettingsAction),
    ResetEditor,
    CreateNewProject,
    ExternalDefinitionChanged(time::Instant),
    IgnoreExternalDefinitionChange,
    Quit,
}
// ----------------------------------------------------------------------------
type ActionSequence = Sequence<Action>;
// ----------------------------------------------------------------------------
#[derive(Debug)]
enum MenuSelection {
    ShowHelp,
    ShowContextHelp(bool),
    ShowAbout,
    ShowSettings,
    NewProject,
    LoadProject,
    ReloadProject,
    SetAutoReload(bool),
    SaveProject,
    SaveProjectAtLocation,
    CloseProject,
    QuestGraph(questgraph::Action),
    Quit,
}
// ----------------------------------------------------------------------------
#[derive(Debug)]
enum PopupAction {
    UpdateField(FieldId, FieldValue),
}
// ----------------------------------------------------------------------------
impl State {
    // ------------------------------------------------------------------------
    fn new() -> State {
        State {
            windows: WindowState::default(),

            mode: EditMode::None,
            data: QuestData::default(),

            filebrowser: FileBrowser::default(),
            popup: None,

            settings: settings::Settings::default(),
            help: help::HelpSystem::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
pub fn run(
    app_name: String,
    helpfile: &Path,
    settings: &Path,
    repo_dir: &Path,
    startdir: Option<PathBuf>,
) -> Result<(), String> {
    let mut actions = ActionState::default();
    let mut state = State::new();
    let mut dont_quit = true;

    let mut def_watcher = DefinitionWatcher::new()?;
    let repo = definitions::quest::load_repo(repo_dir)?;

    if let Some(startdir) = startdir {
        // save current dir
        state.filebrowser.set_path(&startdir);
        actions.push(Action::LoadProject(startdir));
    }

    // ignore missing help file (help won't be available but everthing else works)
    state.help.load(helpfile).ok();
    state.settings.load(settings).map_err(|e| error!("{}", e)).ok();

    support::run(app_name, CLEAR_COLOR, |ui, win_close| {
        if win_close {
            actions.include(ima_prio_seq![Action::GuardModifiedData, Action::Quit]);
        }

        let screenspace = ScreenSpaceManager::new(ui.imgui().display_size(), &state.settings);

        actions.filter_push(view::menu::render(
            ui,
            state.mode,
            &state.data,
            &mut state.settings,
            &state.help,
        ));

        // -- main content
        // unfortunately imgui requires a mutable buffer to accept string input.
        // passing immutetable state for rendering (preferable!) would require
        // to clone *all* displayed strings on every frame. to prevent this
        // a mutable reference is passed. nevertheless string input is handled
        // by actions (key input) like remaining render -> action -> update pattern
        actions.filter_push(view::show_editor(ui, &screenspace, &mut state));

        // -- additional global interaction windows
        actions.filter_push(show_filebrowser(ui, &mut state));

        if let Some(ref mut popup) = state.popup {
            actions.filter_push(popup.draw(ui));
            if !popup.opened() {
                state.popup = None;
            }
        }

        // must be called AFTER view drawing and BEFORE action perform since it
        // cleans some one-time state flags (e.g. context menu opening)
        actions.filter_push(running_actions_tick(&mut state));

        // perform "blocks" on interactive action (e.g. confirm dialog)
        // "blocks" meaning: redraws associated ui until some user interaction
        // takes place and continues with next action in specified queue afterwards
        while let Some(action) = actions::perform(ui, &mut actions) {
            match handle_action(action, &mut actions, &mut state, &mut def_watcher, &repo) {
                Ok(do_quit) => dont_quit = !do_quit,
                Err(error_msg) => {
                    actions.clear();
                    view::auxiliary::set_error(&mut state.windows.error, &error_msg);
                }
            }
        }

        // detect file changes
        if def_watcher.has_changed() {
            actions.push(Action::ExternalDefinitionChanged(time::Instant::now()));
        }

        // auxiliary windows contain error and info popups -> draw after cmds to
        // show update/cmd execution errors
        actions.filter_push(view::auxiliary::show_windows(
            ui,
            &mut state.windows,
            &state.help,
            &mut state.settings,
        ));

        dont_quit
    })
}
// ----------------------------------------------------------------------------
// main loop functions for better readablity
// ----------------------------------------------------------------------------
#[inline]
fn running_actions_tick(state: &mut State) -> Option<Action> {
    match state.mode {
        EditMode::QuestGraph => match state.data.graph {
            Some(ref mut graph) => questgraph::running_actions_tick(graph),
            None => None,
        },
        EditMode::None => None,
    }
}
// ----------------------------------------------------------------------------
#[inline]
fn handle_action(
    action: Action,
    actions: &mut ActionState<Action>,
    state: &mut State,
    def_watcher: &mut DefinitionWatcher,
    repo: &definitions::Repository,
) -> Result<bool, String> {
    match action {
        Action::QuestGraph(action) => match state.data.graph {
            Some(ref mut graph) => {
                if let Some(ref mut def) = state.data.definition {
                    actions.filter_include(questgraph::handle_action(
                        action,
                        graph,
                        def.structure_mut(),
                        &mut state.data.changed,
                    )?);
                }
                // *VERY* coarse dirty marking
                // state.data.changed = true;
                Ok(())
            }
            None => Ok(()),
        },

        Action::GuardModifiedData => {
            if let Some(editmode_dataguard) = match state.mode {
                EditMode::QuestGraph => questgraph::guard_modified_data(&state.data.graph),
                EditMode::None => None,
            } {
                actions.include(ima_prio_seq![editmode_dataguard, Action::GuardModifiedData]);
            } else if state.data.changed() {
                actions.include(ima_prio_seq![Action::confirm_save_modified_data()]);
            }
            // *VERY* coarse dirty marking
            // state.data.changed = true;
            Ok(())
        }

        Action::Confirm(text, yes_actions, no_actions) => {
            actions.set_interactive(text, yes_actions, no_actions);
            Ok(())
        }

        Action::SaveCurrent => {
            verify_definition(&state.mode, &mut state.data)?;

            def_watcher.pause();
            cmds::save_questdefinition(
                state.filebrowser.current_path().to_path_buf(),
                state.data.definition.as_mut(),
            )?;
            def_watcher.resume();

            // *VERY* coarse dirty marking
            state.data.changed = false;
            state.data.is_new = false;
            state.data.last_sync = time::Instant::now();

            Ok(())
        }

        Action::SaveProject(ref path) => {
            verify_definition(&state.mode, &mut state.data)?;

            state.filebrowser.set_path(path);

            def_watcher.stop_watching();
            cmds::save_questdefinition(path.to_path_buf(), state.data.definition.as_mut())?;
            def_watcher.start_watching(path);

            // *VERY* coarse dirty marking
            state.data.changed = false;
            state.data.is_new = false;
            state.data.last_sync = time::Instant::now();

            Ok(())
        }

        #[allow(clippy::bind_instead_of_map)]
        Action::LoadProject(dir) => {
            // stop watching previous directory
            def_watcher.stop_watching();
            cmds::load_questdefinition(&dir, repo, state).and_then(|_| {
                def_watcher.start_watching(&dir);
                Ok(())
            })
        }

        Action::ReloadCurrent => {
            cmds::load_questdefinition(&state.filebrowser.current_path().clone(), repo, state)?;
            Ok(())
        }

        // -- filebrowser
        Action::OpenFileBrowser(intent) => {
            state.filebrowser.intent = Some(intent);
            state.windows.show_filebrowser = true;
            Ok(())
        }

        Action::FileBrowser(ref selection) => {
            if let Some(intent) = state.filebrowser.intent.take() {
                actions.include(update::handle_filebrowser_selection(selection, &intent));
            }
            Ok(())
        }

        // -- definition watcher
        Action::ExternalDefinitionChanged(time) => {
            if state.data.last_sync < time {
                let action = if state.data.changed() {
                    Action::confirm_reload_definition_loose_changes()
                } else if state.data.auto_reload {
                    Action::ReloadCurrent
                } else {
                    Action::confirm_reload_definition()
                };
                actions.include(ima_prio_seq![action]);
            }
            Ok(())
        }
        Action::IgnoreExternalDefinitionChange => state.data.ignore_external_change(),

        // -- settings change
        Action::SettingsChanged(action) => {
            use self::settings::SettingsAction::*;
            match action {
                GridSpacingChanged(x, y) => actions.push(questgraph::Action::UpdateGridSpacing(x, y)),
                // ignore as the settings value is used directly
                PanelWidthChanged => {}
            }
            Ok(())
        }

        // -- generic popup
        Action::PopupRequest(control) => {
            state.popup = Some(popup::Popup::new(control));
            Ok(())
        }
        Action::Popup(action) => {
            if let Some(ref mut popup) = state.popup {
                actions.filter_push(popup.handle_action(action));
            }
            Ok(())
        }

        // -- menu
        Action::Menu(selection) => {
            actions.filter_include(update::handle_menu_selection(selection, state));
            Ok(())
        }

        Action::ResetEditor => {
            def_watcher.stop_watching();
            cmds::close_project(state)
        }
        Action::CreateNewProject => cmds::create_new_project(state),

        Action::Quit => {
            if state.settings.changed {
                state.settings.save().map_err(|e| error!("{}", e)).ok();
            }
            return Ok(true);
        }
    }
    // default do not quit
    .map(|_| false)
}
// ----------------------------------------------------------------------------
fn verify_definition(mode: &EditMode, questdata: &mut QuestData) -> Result<(), String> {
    match *mode {
        EditMode::QuestGraph => {
            questgraph::sync_data(
                questdata.graph.as_ref().expect("missing graphstate"),
                questdata
                    .definition
                    .as_mut()
                    .expect("missing data")
                    .structure_mut(),
            )?;
        }
        EditMode::None => {}
    }
    // validity check to prevent saving of non readable definition
    cmds::verify_questdefinition(questdata.definition.as_ref()).map_err(|err| {
        format!(
            "Quest definition contains one or more errors:\
             \n\n{}\n\nData was NOT saved.",
            err
        )
    })
}
// ----------------------------------------------------------------------------
#[inline]
fn show_filebrowser<'ui>(ui: &Ui<'ui>, state: &mut State) -> Option<filebrowser::Selection> {
    if state.windows.show_filebrowser {
        filebrowser::show(
            ui,
            &mut state.filebrowser.browser,
            &mut state.windows.show_filebrowser,
        )
    } else {
        None
    }
}
// ----------------------------------------------------------------------------
// helper for browser
// ----------------------------------------------------------------------------
#[derive(Debug)]
enum FileBrowserIntent {
    LoadProject,
    SaveProject,
}
// ----------------------------------------------------------------------------
struct FileBrowser {
    current_path: PathBuf,
    intent: Option<FileBrowserIntent>,
    browser: filebrowser::FileChooserState,
}
// ----------------------------------------------------------------------------
impl Default for FileBrowser {
    // ------------------------------------------------------------------------
    fn default() -> FileBrowser {
        let default_path = PathBuf::from(".");
        FileBrowser {
            browser: filebrowser::FileChooserState::new(&default_path),
            current_path: default_path,
            intent: None,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl FileBrowser {
    // ------------------------------------------------------------------------
    fn set_path<P: Into<PathBuf>>(&mut self, path: P) {
        self.current_path = path.into();
        self.browser = filebrowser::FileChooserState::new(&self.current_path);
    }
    // ------------------------------------------------------------------------
    fn current_path(&self) -> &PathBuf {
        &self.current_path
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// helper struct for external change detection of definitions
// ----------------------------------------------------------------------------
struct DefinitionWatcher {
    watcher: RecommendedWatcher,
    receiver: mpsc::Receiver<notify::DebouncedEvent>,
    is_watching: bool,
    watched_path: Option<PathBuf>,
}
// ----------------------------------------------------------------------------
impl DefinitionWatcher {
    // ------------------------------------------------------------------------
    fn new() -> Result<Self, String> {
        // channel to receive notifications from file watcher
        let (watcher_channel, receiver) = mpsc::channel();
        // debounced file watcher auto-selected based on the platform
        let watcher = notify::watcher(watcher_channel, time::Duration::from_secs(1))
            .map_err(|e| format!("failed to start definition files watcher: {}", e))?;

        Ok(DefinitionWatcher {
            watcher,
            receiver,
            watched_path: None,
            is_watching: false,
        })
    }
    // ------------------------------------------------------------------------
    fn start_watching(&mut self, path: &Path) {
        if self.watched_path.is_some() {
            self.stop_watching();
        }
        match path.canonicalize() {
            Ok(path) => {
                if let Err(err) = self.watcher.watch(&path, RecursiveMode::Recursive) {
                    error!("failed to watch path [{}]: {}", path.display(), err);
                } else {
                    info!("definition watcher: started watching {}", path.display());
                    self.watched_path = Some(path);
                    self.is_watching = true;
                }
            }
            Err(msg) => {
                error!(
                    "definition watcher: failed to normalize path [{}]: {}",
                    path.display(),
                    msg
                );
            }
        }
    }
    // ------------------------------------------------------------------------
    fn stop_watching(&mut self) {
        if let Some(path) = self.watched_path.take() {
            match self.watcher.unwatch(&path) {
                Ok(_) => info!("definition watcher: stopped watching {}", path.display()),
                Err(msg) => error!(
                    "definition watcher: failed to stop watching path [{}]: {}",
                    path.display(),
                    msg
                ),
            }
        }
        self.watched_path = None;
        self.is_watching = false;
    }
    // ------------------------------------------------------------------------
    fn pause(&mut self) {
        if self.is_watching {
            if let Some(path) = &self.watched_path {
                if let Err(msg) = self.watcher.unwatch(&path) {
                    error!(
                        "definition watcher: failed to stop watching path [{}]: {}",
                        path.display(),
                        msg
                    );
                } else {
                    self.is_watching = false;
                }
            }
        }
    }
    // ------------------------------------------------------------------------
    fn resume(&mut self) {
        if let Some(path) = &self.watched_path {
            if !self.is_watching {
                if let Err(err) = self.watcher.watch(&path, RecursiveMode::NonRecursive) {
                    error!("failed to watch path [{}]: {}", path.display(), err);
                } else {
                    self.is_watching = true;
                }
            }
        }
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn has_changed(&self) -> bool {
        use notify::DebouncedEvent::*;
        use std::ops::Deref;

        match self.receiver.try_recv() {
            Ok(Write(ref file))
            | Ok(Remove(ref file))
            | Ok(Create(ref file))
            | Ok(Rename(ref file, _))
            | Ok(NoticeRemove(ref file))
            | Ok(NoticeWrite(ref file))
                if file.to_string_lossy().deref().ends_with(".yml") =>
            {
                info!("detected definition change: {}", file.display());
                true
            }
            _ => false,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// top level editable data container
// ----------------------------------------------------------------------------
struct QuestData {
    name: ImString,
    definition: Option<model::QuestDefinition>,
    graph: Option<questgraph::GraphState>,
    is_new: bool,
    changed: bool,
    auto_reload: bool,
    last_sync: time::Instant,
}
// ----------------------------------------------------------------------------
impl Default for QuestData {
    fn default() -> QuestData {
        QuestData {
            name: ImString::new(""),
            definition: None,
            graph: None,
            is_new: true,
            changed: false,
            auto_reload: false,
            last_sync: time::Instant::now(),
        }
    }
}
// ----------------------------------------------------------------------------
impl QuestData {
    // ------------------------------------------------------------------------
    fn set(&mut self, definition: model::QuestDefinition, is_new: bool) -> Result<(), String> {
        if definition.settings().is_some() {
            self.definition = Some(definition);

            if let Some(ref q) = self.definition {
                if let Some(settings) = q.settings() {
                    self.name = ImString::new(settings.dlcid().as_str());
                }
            }
            self.is_new = is_new;
            self.changed = false;
            self.last_sync = time::Instant::now();
            Ok(())
        } else {
            Err("quest definition does not contain any settings".into())
        }
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn is_available(&self) -> bool {
        self.definition.is_some()
    }
    // ------------------------------------------------------------------------
    #[inline]
    fn is_new(&self) -> bool {
        self.is_new
    }
    // ------------------------------------------------------------------------
    fn changed(&self) -> bool {
        // TODO the "correct" way would be to compare a loaded with current definition
        // however that would mean deriving *everywhere* Clone and Eq
        // for now KISS: it's always assumed dirty
        self.definition.is_some() && self.changed
    }
    // ------------------------------------------------------------------------
    #[allow(clippy::unnecessary_wraps)]
    fn ignore_external_change(&mut self) -> Result<(), String> {
        self.last_sync = time::Instant::now();
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn enable_auto_reload(&mut self, enable: bool) {
        self.auto_reload = enable;
    }
    // ------------------------------------------------------------------------
    fn reset(&mut self) {
        self.name = ImString::new("");
        self.definition = None;
        self.graph = None;
        self.is_new = true;
        self.changed = false;
        self.last_sync = time::Instant::now();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Action {
    // ------------------------------------------------------------------------
    fn confirm_save_modified_data() -> Action {
        Action::Confirm(
            String::from("Definition may have changed. Save data?"),
            vec![Action::SaveCurrent],
            vec![],
        )
    }
    // ------------------------------------------------------------------------
    fn confirm_reload_definition() -> Action {
        Action::Confirm(
            String::from(
                "Definition files were updated outside of the editor.\n\n\n\
                 Do you want to RELOAD the definition?",
            ),
            vec![Action::ReloadCurrent],
            vec![Action::IgnoreExternalDefinitionChange],
        )
    }
    // ------------------------------------------------------------------------
    fn confirm_reload_definition_loose_changes() -> Action {
        Action::Confirm(
            String::from(
                "There are UNSAVED changes in the editor and definition \
                 files were updated outside of the editor.\n\n\
                 Do you want to DISCARD changes and RELOAD the definition?",
            ),
            vec![Action::ReloadCurrent],
            vec![Action::IgnoreExternalDefinitionChange],
        )
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
impl From<MenuSelection> for Action {
    fn from(selection: MenuSelection) -> Action {
        Action::Menu(selection)
    }
}
// ----------------------------------------------------------------------------
impl From<PopupAction> for Action {
    fn from(action: PopupAction) -> Action {
        Action::Popup(action)
    }
}
// ----------------------------------------------------------------------------
impl From<filebrowser::Selection> for Action {
    fn from(selection: filebrowser::Selection) -> Action {
        Action::FileBrowser(selection)
    }
}
// ----------------------------------------------------------------------------
impl From<settings::SettingsAction> for Action {
    fn from(action: settings::SettingsAction) -> Action {
        Action::SettingsChanged(action)
    }
}
// ----------------------------------------------------------------------------
impl From<questgraph::Action> for Action {
    fn from(action: questgraph::Action) -> Action {
        Action::QuestGraph(action)
    }
}
// ----------------------------------------------------------------------------
