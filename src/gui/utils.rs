//
// gui::utils
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub(super) struct ScreenSpaceManager {
    left: UiArea,
    main: UiArea,
    right: UiArea,
    help: UiArea,
}
// ----------------------------------------------------------------------------
pub(super) struct UiArea {
    pub pos: (f32, f32),
    pub size: (f32, f32),
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use super::settings::Settings;
// ----------------------------------------------------------------------------
impl ScreenSpaceManager {
    // ------------------------------------------------------------------------
    pub fn new(winsize: (f32, f32), settings: &Settings) -> ScreenSpaceManager {
        let (width, height) = winsize;
        let menu_height = 19.0;
        let help_height = 220.0;
        let main_area = ((0.0, menu_height), (width, height - menu_height));
        let left_area = ((0.0, menu_height), (300.0, height - menu_height));
        let right_area = ((width - settings.rpanel_width, menu_height), (settings.rpanel_width, height - menu_height));
        let context_help = ((width - 320.0, height - help_height), (320.0, help_height));

        ScreenSpaceManager {
            left: left_area.into(),
            main: main_area.into(),
            right: right_area.into(),
            help: context_help.into(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn main_plane(&self) -> &UiArea {
        &self.main
    }
    // ------------------------------------------------------------------------
    pub fn left(&self) -> &UiArea {
        &self.left
    }
    // ------------------------------------------------------------------------
    pub fn right(&self) -> &UiArea {
        &self.right
    }
    // ------------------------------------------------------------------------
    pub fn help(&self) -> &UiArea {
        &self.help
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<((f32, f32), (f32, f32))> for UiArea {
    fn from(v: ((f32, f32), (f32, f32))) -> UiArea {
        UiArea {
            pos: v.0,
            size: v.1,
        }
    }
}
// ----------------------------------------------------------------------------
