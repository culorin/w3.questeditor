//
// ::gui::registry
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct WorldId(String);
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct TagId(String);
// ----------------------------------------------------------------------------
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct JournalGroupId(String);
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct JournalEntryId(String);
// ----------------------------------------------------------------------------
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct JournalQuestId(String);
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct JournalQuestPhaseId(String);
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct JournalQuestObjectiveId(String);
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct JournalQuestMapPinId(String);
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
pub(in gui) struct InteractionEntityId(String);
// ----------------------------------------------------------------------------
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone)]
pub(in gui) struct LayerId(String);
// ----------------------------------------------------------------------------
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct PhaseId(String);
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct CommunityId(String);
// ----------------------------------------------------------------------------
#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Debug, Clone)]
pub(in gui) struct RewardId(String);
// ----------------------------------------------------------------------------
pub(in gui) struct Journals {
    characters: JournalList,
    creatures: JournalList,
    quest_descriptions: JournalList,
    quests: Rc<JournalQuestList>,
}
// ----------------------------------------------------------------------------
pub(in gui) type World = UiOption<WorldId>;
// ----------------------------------------------------------------------------
pub(in gui) type Phase = UiOption<PhaseId>;
pub(in gui) type Community = UiOption<CommunityId>;
// ----------------------------------------------------------------------------
pub(in gui) type Reward = UiOption<RewardId>;
// ----------------------------------------------------------------------------
pub(in gui) struct Tag {
    checked: bool,
    id: TagId,
    caption: ImString,
}
// ----------------------------------------------------------------------------
pub(in gui) type InteractionEntity = UiOption<InteractionEntityId>;
// ----------------------------------------------------------------------------
pub(in gui) type JournalGroup = UiOption<JournalGroupId>;
pub(in gui) type JournalEntry = UiOption<JournalEntryId>;
pub(in gui) type JournalQuest = UiOption<JournalQuestId>;
pub(in gui) type JournalPhase = UiOption<JournalQuestPhaseId>;
pub(in gui) type JournalObjective = UiOption<JournalQuestObjectiveId>;
pub(in gui) type JournalMapPin = UiOption<JournalQuestMapPinId>;
// ----------------------------------------------------------------------------
pub(in gui) type Layer = UiOption<LayerId>;
// ----------------------------------------------------------------------------
pub(in gui) type TagList = IndexMap<World, Vec<Tag>>;
pub(in gui) type JournalList = IndexMap<JournalGroup, Vec<JournalEntry>>;
pub(in gui) type LayerList = IndexMap<World, Vec<Layer>>;
pub(in gui) type CommunityList = IndexMap<Community, Vec<Phase>>;
pub(in gui) type RewardList = Vec<Reward>;
// ----------------------------------------------------------------------------
pub(in gui) type JournalQuestList = IndexMap<JournalQuest, JournalQuestPhaseList>;
pub(in gui) type JournalQuestPhaseList = IndexMap<JournalPhase, JournalQuestObjectiveList>;
pub(in gui) type JournalQuestObjectiveList = IndexMap<JournalObjective, JournalQuestMapPinList>;
pub(in gui) type JournalQuestMapPinList = Vec<JournalMapPin>;
// ----------------------------------------------------------------------------
pub(in gui) type InteractionList = IndexMap<World, Vec<InteractionEntity>>;
// ----------------------------------------------------------------------------
pub struct AssetRegistry {
    tags: LayerTags,
    journals: Rc<Journals>,
    interactions: Rc<InteractionList>,
    layers: Rc<LayerList>,
    communities: Rc<CommunityList>,
    rewards: Rc<RewardList>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::collections::HashSet;
use std::iter::FromIterator;
use std::rc::Rc;

use imgui::{ImStr, ImString};

use indexmap::IndexMap;

use gui::lists::{AsCaption, AsStr, UiOption};

mod model {
    pub use model::community::QuestCommunities;
    pub use model::item::QuestItems;
    pub use model::journal::QuestJournals;
    pub use model::layer::entities::LayerContentEntity;
    pub use model::layer::{QuestLayer, QuestLayers};
    pub use model::primitives::references::{
        InteractionReference, LayerTagReference, RewardReference,
    };
    pub use model::QuestDefinition;
}
// ----------------------------------------------------------------------------
struct LayerTags {
    waypoints: Rc<TagList>,
    scenepoints: Rc<TagList>,
    // actionpoints: Rc<TagList>,
    areas: Rc<TagList>,
    // mappins: Rc<TagList>,
}
// ----------------------------------------------------------------------------
impl AssetRegistry {
    // ------------------------------------------------------------------------
    pub fn new(quest: &model::QuestDefinition) -> AssetRegistry {
        let (tags, interactions) = Self::extract_layercontent_infos(quest.layers());
        AssetRegistry {
            tags,
            journals: Rc::new(Self::extract_journals(quest.journals())),
            interactions: Rc::new(interactions),
            layers: Rc::new(Self::extract_layers(quest.layers())),
            communities: Rc::new(Self::extract_communities(quest.communities())),
            rewards: Rc::new(Self::extract_rewards(quest.items())),
        }
    }
    // ------------------------------------------------------------------------
    fn group_identical_tags(list: &mut TagList) {
        // group identical points (= tag set) and merge into a special named entry
        for taglist in list.values_mut() {
            let mut deduped = IndexMap::new();

            for tag in taglist.drain(..) {
                *deduped.entry(tag).or_insert(0) += 1;
            }

            *taglist = deduped
                .drain(..)
                .map(|(mut tag, count)| {
                    if count > 1 {
                        tag.set_caption(&format!("{} (#{})", tag.id().as_str(), count))
                    }
                    tag
                })
                .collect();
        }
    }
    // ------------------------------------------------------------------------
    fn extract_layercontent_infos(layerset: &model::QuestLayers) -> (LayerTags, InteractionList) {
        use self::model::LayerContentEntity::*;
        use self::model::QuestLayer::*;

        let mut waypoints = TagList::new();
        let mut scenepoints = TagList::new();
        let mut areas = TagList::new();
        // let mut mappins = TagList::new();
        let mut interactive_entities = InteractionList::new();

        let add_tag = |list: &mut TagList, worldid: &str, entityid: &str| {
            let entry = list.entry(World::new(worldid)).or_insert(Vec::new());
            entry.push(Tag::from(model::LayerTagReference::new(worldid, entityid)));
        };

        for (world, layer) in layerset.layers().flat_map(|layer| match layer {
            Data(layer) if layer.world().is_some() => Some((layer.world().unwrap(), layer)),
            _ => None,
        }) {
            for layerentity in layer.content() {
                match layerentity {
                    // MapPin(ref entity) => add_tag(&mut mappins, world, entity.id()),
                    Area(ref entity) => add_tag(&mut areas, world, entity.id()),
                    Waypoint(ref entity) => add_tag(&mut waypoints, world, entity.id()),
                    ScenePoint(ref entity) => add_tag(&mut scenepoints, world, entity.id()),

                    Interactive(ref entity) => {
                        let entry = interactive_entities
                            .entry(World::new(world))
                            .or_insert(Vec::new());

                        // TODO extract and add information about valid interactions?
                        let interaction = InteractionEntity::from(
                            model::InteractionReference::new_entity(world, entity.id()),
                        );
                        entry.push(interaction)
                    }
                    _ => {}
                }
            }
        }

        // sort tags in every world
        waypoints.values_mut().for_each(|tags| tags.sort());
        scenepoints.values_mut().for_each(|tags| tags.sort());
        areas.values_mut().for_each(|tags| tags.sort());
        // mappins.values_mut().for_each(|tags| tags.sort());

        Self::group_identical_tags(&mut waypoints);
        Self::group_identical_tags(&mut scenepoints);

        // sort worlds
        waypoints.sort_keys();
        scenepoints.sort_keys();
        areas.sort_keys();
        // mappins.sort_keys();
        interactive_entities.sort_keys();

        (
            LayerTags {
                waypoints: Rc::new(waypoints),
                scenepoints: Rc::new(scenepoints),
                areas: Rc::new(areas),
                // mappins: Rc::new(mappins),
            },
            interactive_entities,
        )
    }
    // ------------------------------------------------------------------------
    fn extract_layers(layerset: &model::QuestLayers) -> LayerList {
        use self::model::QuestLayer::*;

        let mut list = IndexMap::new();

        for layerid in layerset.layer_ids() {
            if let Some(layer) = layerset.layer(layerid) {
                let (world, option) = match layer {
                    Data(layer) => {
                        let world = layer
                            .world()
                            .map(|w| w.as_str())
                            .unwrap_or("-missing world-");
                        match layer.context() {
                            Some(context) => (
                                world,
                                Layer::new_with_caption(
                                    layer.id(),
                                    &format!("{}/{}", context, layer.id()),
                                ),
                            ),
                            None => (world, Layer::new(layer.id())),
                        }
                    }
                    FilePath(world, _) => (world.as_str(), Layer::new(layerid)),
                };
                let entry = list.entry(World::new(world)).or_insert(Vec::new());
                entry.push(option);
            }
        }

        // sort layerlist in every world
        list.values_mut()
            .for_each(|layers| layers.sort_by(|a, b| a.caption().cmp(b.caption())));

        // sort worlds
        list.sort_keys();

        list
    }
    // ------------------------------------------------------------------------
    #[allow(clippy::from_iter_instead_of_collect)]
    fn extract_journals(journals: &model::QuestJournals) -> Journals {
        let mut characters = JournalList::new();
        let mut creatures = JournalList::new();
        let mut quest_descriptions = JournalList::new();
        let mut quests = JournalQuestList::new();

        for character in journals.characters() {
            let mut entries = HashSet::<_>::from_iter(
                character
                    .entries()
                    .map(|entry| JournalEntry::new(entry.id())),
            )
            .drain()
            .collect::<Vec<_>>();

            entries.sort();
            characters.insert(JournalGroup::new(character.id()), entries);
        }

        for creature in journals.creatures() {
            let mut entries = HashSet::<_>::from_iter(
                creature
                    .description()
                    .entries()
                    .map(|entry| JournalEntry::new(entry.id())),
            )
            .drain()
            .collect::<Vec<_>>();

            entries.sort();
            creatures.insert(JournalGroup::new(creature.id()), entries);
        }

        for quest in journals.quests() {
            let mut entries = HashSet::<_>::from_iter(
                quest
                    .description()
                    .entries()
                    .map(|entry| JournalEntry::new(entry.id())),
            )
            .drain()
            .collect::<Vec<_>>();

            entries.sort();
            quest_descriptions.insert(JournalGroup::new(quest.id()), entries);

            let mut phases = JournalQuestPhaseList::new();
            for phase in quest.instructions() {
                let mut objectives = JournalQuestObjectiveList::new();

                for objective in phase.objectives() {
                    objectives.insert(
                        JournalObjective::new(objective.id()),
                        objective
                            .mappins()
                            .map(|pin| {
                                JournalMapPin::new_with_caption(
                                    pin.id(),
                                    &pin.id().replace('~', "(~) "),
                                )
                            })
                            .collect(),
                    );
                }

                phases.insert(JournalPhase::new(phase.id()), objectives);
            }
            quests.insert(JournalQuest::new(quest.id()), phases);
        }

        characters.sort_keys();
        creatures.sort_keys();
        quest_descriptions.sort_keys();
        quests.sort_keys();

        Journals {
            characters,
            creatures,
            quest_descriptions,
            quests: Rc::new(quests),
        }
    }
    // ------------------------------------------------------------------------
    #[allow(clippy::from_iter_instead_of_collect)]
    fn extract_communities(communities: &model::QuestCommunities) -> CommunityList {
        let mut list = IndexMap::new();

        for community in communities.communities() {
            let mut phases = HashSet::<_>::from_iter(community.phase_ids().map(Phase::new))
                .drain()
                .collect::<Vec<_>>();
            phases.sort();

            list.insert(Community::new(community.id()), phases);
        }
        list.sort_keys();

        list
    }
    // ------------------------------------------------------------------------
    fn extract_rewards(items: &model::QuestItems) -> RewardList {
        items
            .rewards()
            .map(|reward| Reward::new(reward.id()))
            .collect()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl AssetRegistry {
    // ------------------------------------------------------------------------
    pub(in gui) fn scenepoints(&self) -> Rc<TagList> {
        self.tags.scenepoints.clone()
    }
    // ------------------------------------------------------------------------
    pub(in gui) fn waypoints(&self) -> Rc<TagList> {
        self.tags.waypoints.clone()
    }
    // ------------------------------------------------------------------------
    pub(in gui) fn areas(&self) -> Rc<TagList> {
        self.tags.areas.clone()
    }
    // ------------------------------------------------------------------------
    pub(in gui) fn journals(&self) -> Rc<Journals> {
        self.journals.clone()
    }
    // ------------------------------------------------------------------------
    pub(in gui) fn interactions(&self) -> Rc<InteractionList> {
        self.interactions.clone()
    }
    // ------------------------------------------------------------------------
    pub(in gui) fn layers(&self) -> Rc<LayerList> {
        self.layers.clone()
    }
    // ------------------------------------------------------------------------
    pub(in gui) fn communities(&self) -> Rc<CommunityList> {
        self.communities.clone()
    }
    // ------------------------------------------------------------------------
    pub(in gui) fn rewards(&self) -> Rc<RewardList> {
        self.rewards.clone()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Journals {
    // ------------------------------------------------------------------------
    pub fn characters(&self) -> &JournalList {
        &self.characters
    }
    // ------------------------------------------------------------------------
    pub fn creatures(&self) -> &JournalList {
        &self.creatures
    }
    // ------------------------------------------------------------------------
    pub fn quest_descriptions(&self) -> &JournalList {
        &self.quest_descriptions
    }
    // ------------------------------------------------------------------------
    pub fn quests(&self) -> Rc<JournalQuestList> {
        self.quests.clone()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// type converter
// ----------------------------------------------------------------------------
as_option_id!(WorldId);
as_option_id!(TagId);
as_option_id!(InteractionEntityId);
as_option_id!(JournalGroupId);
as_option_id!(JournalEntryId);
as_option_id!(JournalQuestId);
as_option_id!(JournalQuestObjectiveId);
as_option_id!(JournalQuestPhaseId);
as_option_id!(JournalQuestMapPinId);
as_option_id!(LayerId);
as_option_id!(PhaseId);
as_option_id!(CommunityId);
as_option_id!(RewardId);
// ----------------------------------------------------------------------------
impl<'a> From<&'a String> for JournalGroup {
    fn from(id: &String) -> JournalGroup {
        JournalGroup::new(id.as_str())
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a String> for JournalEntry {
    fn from(id: &String) -> JournalEntry {
        JournalEntry::new(id.as_str())
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a String> for JournalQuest {
    fn from(id: &String) -> JournalQuest {
        JournalQuest::new(id.as_str())
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a String> for JournalObjective {
    fn from(id: &String) -> JournalObjective {
        JournalObjective::new(id.as_str())
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a String> for JournalPhase {
    fn from(id: &String) -> JournalPhase {
        JournalPhase::new(id.as_str())
    }
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a String> for JournalMapPin {
    fn from(id: &String) -> JournalMapPin {
        JournalMapPin::new(id.as_str())
    }
}
// ----------------------------------------------------------------------------
impl From<model::LayerTagReference> for Tag {
    // ------------------------------------------------------------------------
    fn from(reference: model::LayerTagReference) -> Tag {
        use self::model::LayerTagReference::*;

        match reference {
            WorldAndId(_, id) => Tag::new(true, id),
            WorldAndTag(_, tag) => Tag::new(false, &*tag),
            UncheckedTag(id) => Tag::new(false, id),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<model::InteractionReference> for UiOption<InteractionEntityId> {
    // ------------------------------------------------------------------------
    fn from(reference: model::InteractionReference) -> UiOption<InteractionEntityId> {
        use self::model::InteractionReference::*;

        match reference {
            LayerEntity(_, id) | Unchecked(id) => UiOption::new(id),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Helper
// ----------------------------------------------------------------------------
impl AsCaption for Tag {
    // ------------------------------------------------------------------------
    fn caption(&self) -> &ImStr {
        &self.caption
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Tag {
    // ------------------------------------------------------------------------
    pub fn new<I: Into<TagId>>(checked: bool, id: I) -> Tag {
        let id = id.into();
        let caption = ImString::new(id.0.replace('~', "").replace('_', " "));
        Tag {
            checked,
            id,
            caption,
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_caption(&mut self, caption: &str) {
        self.caption = ImString::new(caption.replace('~', "").replace('_', " "));
    }
    // ------------------------------------------------------------------------
    pub fn id(&self) -> &TagId {
        &self.id
    }
    // ------------------------------------------------------------------------
    pub fn checked(&self) -> bool {
        self.checked
    }
    // ------------------------------------------------------------------------
    pub fn caption(&self) -> &ImStr {
        &self.caption
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
use std::cmp::{Ord, Ordering};
use std::hash::{Hash, Hasher};
// ----------------------------------------------------------------------------
impl Hash for Tag {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}
// ----------------------------------------------------------------------------
impl PartialEq for Tag {
    fn eq(&self, other: &Tag) -> bool {
        self.id == other.id
    }
}
// ----------------------------------------------------------------------------
impl Eq for Tag {}
// ----------------------------------------------------------------------------
impl Ord for Tag {
    fn cmp(&self, other: &Tag) -> Ordering {
        self.id.cmp(&other.id)
    }
}
// ----------------------------------------------------------------------------
impl PartialOrd for Tag {
    fn partial_cmp(&self, other: &Tag) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
// ----------------------------------------------------------------------------
