//
// gui::cmds - more complex actions with (possible) side effects
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
/// creates a "questart -> script.HelloWorld -> questend" questdefinition
pub(super) fn create_new_project(state: &mut State) -> Result<(), String> {
    use model::questgraph::primitives::{BlockId, LinkTarget};
    use model::questgraph::scripting::Script;
    use model::questgraph::QuestBlock;
    use model::shared::{ScriptCall, ScriptParameter};

    // -- create definition with quest start and quest end block
    let mut new_def = QuestDefinition::new("NewQuestProject", "New Quest Project")?;

    // -- link blocks in root segment
    let structure = new_def.structure_mut();

    let root = structure.root_mut()?;
    if let Some(start_block) = root.start_block_mut(&BlockId::QuestStart("In".into())) {
        start_block.add_link_to(None, LinkTarget::Script("HelloWorld".into()), None)
    }

    // -- create scriptblock
    let mut script_block = Script::new("HelloWorld");
    let mut script_call = ScriptCall::new("radLog");
    script_call.add_param(ScriptParameter::CName(
        "logChannel".to_string(),
        "new_quest".to_string(),
    ));
    script_call.add_param(ScriptParameter::String(
        "msg".to_string(),
        "Hello World!".to_string(),
    ));
    script_block.set_function_call(script_call);
    script_block.add_link_to(None, LinkTarget::QuestEnd("Out".into()), None);

    root.add_block(script_block)?;

    // -- set some default editor settings for better visibility
    let data = root.editordata_or_default();
    data.zoom = 1.0;
    data.pos = (0.0, -100.0);

    state
        .data
        .set(new_def, true)
        // questgraph is default mode
        .and_then(|_| questgraph::init(&mut state.data, None, &state.settings))
        .map(|_| state.mode = EditMode::QuestGraph)
}
// ----------------------------------------------------------------------------
#[allow(clippy::unnecessary_wraps)]
pub(super) fn close_project(state: &mut State) -> Result<(), String> {
    state.mode = EditMode::None;
    state.data.reset();
    Ok(())
}
// ----------------------------------------------------------------------------
pub(super) fn load_questdefinition(
    new_dir: &Path,
    repo: &definitions::Repository,
    state: &mut State,
) -> Result<(), String> {
    state.mode = EditMode::None;
    state.data.reset();
    definitions::quest::load_def(new_dir, repo)
        .and_then(|data| state.data.set(data, false))
        // questgraph is default mode
        .and_then(|_| questgraph::init(&mut state.data, Some(new_dir), &state.settings))
        .map(|_| state.mode = EditMode::QuestGraph)
}
// ----------------------------------------------------------------------------
pub(super) fn save_questdefinition(
    path: PathBuf,
    data: Option<&mut QuestDefinition>,
) -> Result<(), String> {
    if let Some(definition) = data {
        // backup and delete current files
        backup_definitions(&path)?;

        // increase version before saving
        if let Some(ref mut settings) = definition.settings_mut() {
            settings.set_version(settings.version() + 1);
        }

        definitions::quest::save_def(path, definition)
    } else {
        Err(String::from("no quest definition available. save failed."))
    }
}
// ----------------------------------------------------------------------------
pub(super) fn verify_questdefinition(data: Option<&QuestDefinition>) -> Result<(), String> {
    use model::ValidatableElement;

    if let Some(definition) = data {
        definition.validate("quest")
    } else {
        Ok(())
    }
}
// ----------------------------------------------------------------------------
//
// ----------------------------------------------------------------------------
use std::fs::File;
use std::io::{Seek, Write};
use std::path::{Path, PathBuf};

use walkdir::{DirEntry, WalkDir};
use zip;

use definitions;

use model::QuestDefinition;

use super::questgraph;

use super::{EditMode, State};
// ----------------------------------------------------------------------------
fn backup_definitions(path: &Path) -> Result<(), String> {
    use std::fs;

    debug!("> creating backup of current quest definition files...");

    // find all yml files
    let def_filelist = WalkDir::new(path)
        .contents_first(true)
        .into_iter()
        .filter_entry(|e| e.path().is_dir() || is_definition_file(e))
        .collect::<Result<Vec<_>, _>>()
        .map_err(|e| format!("backup creation: read error: {}", e))?;

    if !def_filelist.is_empty() {
        let (filename, zipfile) = create_backupfile(path)?;

        // zip files as backup-Y-m-d_HMS.zip
        let files = zip_files(path, &def_filelist, zipfile)
            .map_err(|e| format!("backup creation: error creating backup zip: {}", e))?;

        // and delete all zipped files
        for file in files {
            fs::remove_file(file).map_err(|e| {
                format!(
                    "backup creation: failed to delete {}: {}",
                    file.display(),
                    e
                )
            })?;
        }
        info!("definition backup stored as {}", filename.display());
    } else {
        info!("found no definition files for backup.");
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn create_backupfile(path: &Path) -> Result<(PathBuf, File), String> {
    use chrono::prelude::Local;
    use std::fs;

    if !path.is_dir() {
        return Err(format!(
            "backup creation: definition files path is not a directory: {}",
            path.display()
        ));
    }

    let mut backupfile = path.to_owned();
    backupfile.push("backups");

    fs::create_dir_all(backupfile.as_path()).map_err(|why| {
        format!(
            "could not create backup directory {}: {}",
            backupfile.display(),
            why
        )
    })?;

    backupfile.push(format!(
        "backup-{}.zip",
        Local::now().format("%Y-%m-%d_%H%M%S")
    ));

    let file = File::create(&backupfile)
        .map_err(|e| format!("backup creation: could not create backup file: {}", e))?;

    Ok((backupfile, file))
}
// ----------------------------------------------------------------------------
fn is_definition_file(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.ends_with(".yml") || s.ends_with(".apx"))
        .unwrap_or(false)
}
// ----------------------------------------------------------------------------
fn zip_files<'entries, W>(
    src_dir: &Path,
    filelist: &'entries [DirEntry],
    writer: W,
) -> Result<Vec<&'entries Path>, String>
where
    W: Write + Seek,
{
    use std::io::Read;
    use zip::write::FileOptions;

    let mut zipped_files = Vec::with_capacity(filelist.len());

    let mut zip = zip::ZipWriter::new(writer);
    let options = FileOptions::default().compression_method(zip::CompressionMethod::Deflated);

    let mut buffer = Vec::new();
    for entry in filelist {
        let path = entry.path();

        if path.is_file() {
            let name = path
                .strip_prefix(src_dir)
                .map_err(|e| e.to_string())?
                .to_str()
                .ok_or_else(|| String::from("could not strip src dir from filename"))?;

            trace!(">> adding {}...", path.display());
            zip.start_file(name, options).map_err(|e| e.to_string())?;
            let mut f = File::open(path).map_err(|e| e.to_string())?;

            f.read_to_end(&mut buffer).map_err(|e| e.to_string())?;
            zip.write_all(&*buffer).map_err(|e| e.to_string())?;

            buffer.clear();
            zipped_files.push(path);
        }
    }
    zip.finish()
        .map(|_| zipped_files)
        .map_err(|e| e.to_string())
}
// ----------------------------------------------------------------------------
