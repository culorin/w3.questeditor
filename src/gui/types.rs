//
// gui::types
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
pub(in gui) struct FieldId(&'static str);
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub(in gui) enum FieldValue {
    String(String),
    Float(f32),
    Int(i32),
    Bool(bool),
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use imgui_controls::input;

// ----------------------------------------------------------------------------
impl FieldId {
    pub fn as_str(&self) -> &'static str {
        self.0
    }
}
// ----------------------------------------------------------------------------
impl From<&'static str> for FieldId {
    // ------------------------------------------------------------------------
    fn from(id: &'static str) -> FieldId {
        FieldId(id)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl FieldValue {
    // ------------------------------------------------------------------------
    pub fn as_str(&self) -> Result<&str, String> {
        match *self {
            FieldValue::String(ref value) => Ok(value),
            _ => Err(String::from("value not a string")),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<input::FieldValue<'a>> for FieldValue {
    // ------------------------------------------------------------------------
    fn from(value: input::FieldValue) -> FieldValue {
        match value {
            input::FieldValue::Str(value) => FieldValue::String(value.to_owned()),
            input::FieldValue::Float(value) => FieldValue::Float(value),
            input::FieldValue::Int(value) => FieldValue::Int(value),
            input::FieldValue::Bool(value) => FieldValue::Bool(value),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a, 'b: 'a> From<&'b FieldValue> for input::FieldValue<'a> {
    // ------------------------------------------------------------------------
    fn from(value: &'b FieldValue) -> input::FieldValue<'a> {
        match value {
            FieldValue::String(ref value) => input::FieldValue::Str(value),
            FieldValue::Float(value) => input::FieldValue::Float(*value),
            FieldValue::Int(value) => input::FieldValue::Int(*value),
            FieldValue::Bool(value) => input::FieldValue::Bool(*value),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// display
// ----------------------------------------------------------------------------
use std::fmt;

impl fmt::Display for FieldValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            FieldValue::String(ref value) => value.fmt(f),
            FieldValue::Float(value) => value.fmt(f),
            FieldValue::Int(value) => value.fmt(f),
            FieldValue::Bool(value) => value.fmt(f),
        }
    }
}
// ----------------------------------------------------------------------------
