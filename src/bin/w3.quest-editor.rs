extern crate logger;
#[macro_use]
extern crate log;

extern crate getopts;

extern crate w3_questeditor as w3questeditor;

use getopts::{Matches, Options};
use std::env;
use std::path::{Path, PathBuf};

use logger::LevelFilter;

use w3questeditor::gui;
// ----------------------------------------------------------------------------
const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
const NAME: &str = "w3 quest editor";
const HELPFILE: &str = "help.txt";
const SETTINGSFILE: &str = "settings.ini";
const GIT_HASH: &str = env!("GIT_HASH");
const BUILD_TIME: &str = env!("BUILD_TIME");

struct CliArgs {
    data: Option<PathBuf>,
    repodir: PathBuf,
}
// ----------------------------------------------------------------------------
fn setup_option() -> Options {
    let mut opts = Options::new();

    // misc
    opts.optflag("h", "help", "print this help menu");

    // dquest defintion directory
    opts.optopt("d", "data-dir", "defines directory of quest definitions to edit",
        "DIRECTORY");

    // repo directory
    opts.optopt("r", "repo-dir", "defines directory of repository for required quest \
        independent definitions. default is repo.quests/", "DIRECTORY");

    // misc
    opts.optflag("v", "verbose", "show debug messages in console");
    opts.optflag("", "very-verbose", "show more debug messages in console");

    opts
}
// ----------------------------------------------------------------------------
fn print_usage(program: &str, opts: &Options) {
    let brief = format!(
        "{} v{} (git:{} build:{}) \nUsage: {} [options]",
        NAME,
        VERSION.unwrap_or("unknown"),
        GIT_HASH,
        BUILD_TIME,
        program
    );
    print!("{}", opts.usage(&brief));
}
// ----------------------------------------------------------------------------
fn check_dir<T: Into<PathBuf>>(dir: T, name: &str) -> Result<PathBuf, String> {
    let dir = dir.into();
    if !dir.exists() || !dir.is_dir() {
        Err(format!("{} [{}] does not exist", name, dir.display()))
    } else {
        Ok(dir)
    }
}
// ----------------------------------------------------------------------------
fn parse_arguments(found: &Matches) -> Result<CliArgs, String> {
    let loglevel = if found.opt_present("very-verbose") {
        LevelFilter::Trace
    } else if found.opt_present("v") {
        LevelFilter::Debug
    } else {
        LevelFilter::Info
    };
    let _ = logger::init(loglevel);

    // dirs
    let param_data_dir = found.opt_str("d");
    let param_repo_dir = found.opt_str("r");

    let repodir = check_dir(
        param_repo_dir.unwrap_or_else(|| {
            debug!("using default repository dir: repo.quests/");
            String::from("repo.quests/")
        }),
        "repository directory",
    )?;

    let datadir = match param_data_dir {
        Some(dir) => Some(check_dir(&dir, "data directory")?),
        None => None,
    };

    Ok(CliArgs {
        data: datadir,
        repodir,
    })
}
// ----------------------------------------------------------------------------
fn interactive_mode(repodir: &Path, inputdir: Option<PathBuf>) -> Result<(), String> {
    let app_name = format!("{} v{}", NAME, VERSION.unwrap_or("unknown"));

    // start main gui loop
    let path = env::current_exe()
        .map(|mut p| {
            p.pop();
            p
        })
        .unwrap_or_else(|_| PathBuf::from("./"));

    let helpfile = path.join(HELPFILE);
    let settingsfile = path.join(SETTINGSFILE);
    gui::run(app_name, &helpfile, &settingsfile, repodir, inputdir)
}
// ----------------------------------------------------------------------------
fn start_main() -> Result<(), i32> {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let opts = setup_option();

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            logger::pre_init_fatal(f.to_string());
            print_usage(&program, &opts);
            return Err(1);
        }
    };

    // no free args
    if matches.opt_present("h") || !matches.free.is_empty() {
        print_usage(&program, &opts);
        return Ok(());
    }

    match parse_arguments(&matches) {
        Ok(args) => {
            println!(
                "{} v{} (git:{} build:{})\n",
                NAME,
                VERSION.unwrap_or("unknown"),
                GIT_HASH,
                BUILD_TIME
            );

            interactive_mode(&args.repodir, args.data)
                .map_err(|errmsg| {
                    error!("{}", errmsg);
                    1
                })
        },
        Err(msg) => {
            error!("{}", msg);
            print_usage(&program, &opts);
            Err(1)
        }
    }
}
// ----------------------------------------------------------------------------
use std::process;
fn main() {
    let resultcode = match start_main() {
        Ok(_) => 0,
        Err(err) => err,
    };

    process::exit(resultcode);
}
// ----------------------------------------------------------------------------
